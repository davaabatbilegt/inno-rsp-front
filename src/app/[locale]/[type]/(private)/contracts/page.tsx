'use client';

import ContractsContainer from '@/containers/contracts';
import { type NextPage } from 'next';

const ContractsPage: NextPage = () => <ContractsContainer />;

export default ContractsPage;

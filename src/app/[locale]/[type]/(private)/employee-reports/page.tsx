'use client';

import EmployeesReportContainer from '@/containers/employee-report';
import { type NextPage } from 'next';

const EmployeesReportPage: NextPage = () => <EmployeesReportContainer />;

export default EmployeesReportPage;

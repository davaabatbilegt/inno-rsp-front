'use client';

import EmployeesManageContainer from '@/containers/employees-manage';
import { type NextPage } from 'next';

const EmployeesPage: NextPage = () => <EmployeesManageContainer />;

export default EmployeesPage;

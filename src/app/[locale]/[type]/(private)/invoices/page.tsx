'use client';

import { UserTypesType } from '@/constants/common.constants';
import InvoiceContainer from '@/containers/invoices';
import { type NextPage } from 'next';

type Props = {
  params: { type: UserTypesType };
};

const InvoicesPage: NextPage<Props> = (props: Props) => (
  <InvoiceContainer type={props.params.type} />
);

export default InvoicesPage;

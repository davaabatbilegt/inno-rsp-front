'use client';

import FullLoader from '@/components/full-loader';
import MainLayout from '@/components/layouts/main-layout';
import {
  COOKIE_KEYS,
  USER_TYPES_LIST,
  UserTypesType,
} from '@/constants/common.constants';
import { useAuth } from '@/contexts/auth';
import { usePathname } from '@/lib/navigation';
import { getCookie } from 'cookies-next';
import { notFound } from 'next/navigation';
import { useEffect } from 'react';

export default function PrivateLayout({
  children,
  params,
}: {
  children: React.ReactNode;
  params: { type: UserTypesType; locale: string };
}): React.ReactNode {
  const { userData, triggerFullLoading, refetchUserInfo } = useAuth();
  const pathname = usePathname();
  const userTypesList: string[] = USER_TYPES_LIST;

  useEffect(() => {
    if (!userTypesList.includes(pathname.split('/')[1])) {
      return notFound();
    }
    if (params.type !== getCookie(COOKIE_KEYS.TYPE)) {
      return notFound();
    }
    if (!userData?.name) {
      refetchUserInfo();
    }
  }, []);

  return (
    <MainLayout type={params.type} locale={params.locale}>
      <FullLoader loading={triggerFullLoading} />
      {children}
    </MainLayout>
  );
}

'use client';

import { USER_TYPES, UserTypesType } from '@/constants/common.constants';
import ContractsMenuListContainer from '@/containers/contracts-menu';
import MenusContainer from '@/containers/menus';
import MenusManageContainer from '@/containers/menus-manage';
import { type NextPage } from 'next';

type Props = { params: { id: string; type: UserTypesType } };

const MenusPage: NextPage<Props> = (props: Props) => {
  if (props.params.type === USER_TYPES.RESTAURANT) {
    return <MenusManageContainer />;
  } else if (props.params.type === USER_TYPES.EMPLOYEE) {
    return <ContractsMenuListContainer />;
  } else {
    return <MenusContainer />;
  }
};

export default MenusPage;

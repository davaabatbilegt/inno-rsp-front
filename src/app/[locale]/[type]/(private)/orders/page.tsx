'use client';

import OrderingContainer from '@/containers/ordering';
import { type NextPage } from 'next';

const OrdersPage: NextPage = () => <OrderingContainer />;

export default OrdersPage;

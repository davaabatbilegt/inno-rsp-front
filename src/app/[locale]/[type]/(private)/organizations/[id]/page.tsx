'use client';

import OrganizationDetailContainer from '@/containers/organization-detail';
import { type NextPage } from 'next';

type Props = { params: { id: string } };

const RestaurantOrganizationDetailPage: NextPage<Props> = (props: Props) => (
  <OrganizationDetailContainer id={props.params.id} />
);

export default RestaurantOrganizationDetailPage;

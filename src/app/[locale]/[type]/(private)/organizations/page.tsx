'use client';

import OrganizationsContainer from '@/containers/organizations';
import { type NextPage } from 'next';

const OrganizationsPage: NextPage = () => <OrganizationsContainer />;

export default OrganizationsPage;

'use client';

import { UserTypesType } from '@/constants/common.constants';
import ProfileContainer from '@/containers/profile';
import { type NextPage } from 'next';

type Props = {
  params: { type: UserTypesType };
};

const ProfilePage: NextPage<Props> = (props: Props) => (
  <ProfileContainer type={props.params.type} />
);

export default ProfilePage;

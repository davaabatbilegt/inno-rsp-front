'use client';

import { UserTypesType } from '@/constants/common.constants';
import ReportContainer from '@/containers/reports';
import { type NextPage } from 'next';

type Props = {
  params: { type: UserTypesType };
};

const ReportsPage: NextPage<Props> = (props: Props) => (
  <ReportContainer type={props.params.type} />
);

export default ReportsPage;

'use client';

import { UserTypesType } from '@/constants/common.constants';
import RestaurantDetailContainer from '@/containers/restaurant-detail';
import { type NextPage } from 'next';

type Props = { params: { id: string; type: UserTypesType } };

const EmployeeRestaurantDetailPage: NextPage<Props> = (props: Props) => (
  <RestaurantDetailContainer id={props.params.id} type={props.params.type} />
);

export default EmployeeRestaurantDetailPage;

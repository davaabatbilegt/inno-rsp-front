'use client';

import { UserTypesType } from '@/constants/common.constants';
import RestaurantsContainer from '@/containers/restaurants';
import { type NextPage } from 'next';

type Props = { params: { type: UserTypesType } };

const RestaurantsPage: NextPage<Props> = (props: Props) => (
  <RestaurantsContainer type={props.params.type} />
);

export default RestaurantsPage;

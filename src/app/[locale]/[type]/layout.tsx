'use client';

import { USER_TYPES_LIST } from '@/constants/common.constants';
import { usePathname } from '@/lib/navigation';
import { notFound } from 'next/navigation';

export default function TypeLayout({
  children,
}: {
  children: React.ReactNode;
}): React.ReactNode {
  const pathname = usePathname();
  const userTypesList: string[] = USER_TYPES_LIST;

  if (!userTypesList.includes(pathname.split('/')[1])) return notFound();

  return children;
}

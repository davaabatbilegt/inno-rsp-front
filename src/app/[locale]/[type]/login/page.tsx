'use client';

import { UserTypesType } from '@/constants/common.constants';
import { type NextPage } from 'next';

import LoginContainer from '@containers/login';

type Props = {
  params: { type: UserTypesType };
};

const LoginPage: NextPage<Props> = (props: Props) => (
  <LoginContainer type={props.params.type} />
);

export default LoginPage;

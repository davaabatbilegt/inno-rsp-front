import NotFoundContainer from '@/containers/not-found';

export default function NotFound() {
  return <NotFoundContainer />;
}

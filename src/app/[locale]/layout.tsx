import { ReactQueryClientProvider } from '@/components/providers/ReactQueryClientProvider';
import { Toaster } from '@/components/ui/sonner';
import { TooltipProvider } from '@/components/ui/tooltip';
import { AuthContextProvider } from '@/contexts/auth';
import dayjs from 'dayjs';
import type { Metadata } from 'next';
import { NextIntlClientProvider, useMessages } from 'next-intl';
import { Montserrat } from 'next/font/google';

import '../globals.css';

const montserrat = Montserrat({ subsets: ['latin'] });

export const metadata: Metadata = {
  title: 'RSP',
  description: `Restaurant service platform | Project for UMS innovator ${dayjs().year()}`,
};

export default function LocaleLayout({
  children,
  params: { locale },
}: Readonly<{
  children: React.ReactNode;
  params: { locale: string };
}>) {
  const messages = useMessages();

  return (
    <ReactQueryClientProvider>
      <html lang={locale}>
        <body className={montserrat.className}>
          <NextIntlClientProvider locale={locale} messages={messages}>
            <AuthContextProvider>
              <TooltipProvider>{children}</TooltipProvider>
              <Toaster position="top-right" duration={2000} />
            </AuthContextProvider>
          </NextIntlClientProvider>
        </body>
      </html>
    </ReactQueryClientProvider>
  );
}

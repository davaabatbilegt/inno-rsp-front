import { getImage } from '@/lib/utils';
import _ from 'lodash';
import Image from 'next/image';

type Props = {
  username: string;
  size: number;
};

const AvatarImage: React.FC<Props> = ({ username, size }: Props) => {
  const imageInfo = getImage(username);
  return (
    <div
      className={`flex h-${size} w-${size} items-center justify-center rounded-full bg-gray-300 text-${size > 10 ? 'xl' : 'md'} text-black`}
    >
      {!_.isEmpty(imageInfo) ? (
        <Image src={imageInfo} alt={username} className="rounded-full" />
      ) : (
        <div className="rounded-full">{username[0].toUpperCase()}</div>
      )}
    </div>
  );
};

export default AvatarImage;

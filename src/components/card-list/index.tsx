'use client';

import CustomPagination from '@/components/custom-pagination';
import { MetaType } from '@/constants/types.constants';

import EmptyData from '../empty-data';
import { Skeleton } from '../ui/skeleton';

type Props = {
  children: React.ReactNode;
  meta: MetaType | undefined;
  pageSize?: number;
  loading?: boolean;
};

const CardList: React.FC<Props> = (props: Props) => {
  const { meta, loading } = props;

  return (
    <div className="flex-col">
      {loading ? (
        <div className="mt-8 grid grid-cols-1 flex-row gap-4 p-4 sm:grid-cols-2 sm:gap-6 md:grid-cols-3 md:gap-8 lg:grid-cols-5 lg:gap-8">
          {Array.from({ length: 10 }, (_, index) => (
            <Skeleton key={index} className="h-[250px] w-full rounded-xl" />
          ))}
        </div>
      ) : meta?.totalItems === 0 ? (
        <EmptyData size="large" />
      ) : (
        props.children
      )}
      {meta?.totalItems !== 0 ? (
        <div className="mt-8 flex-col">
          <CustomPagination
            page={meta?.currentPage}
            totalPage={meta?.totalPages}
            loading={loading}
          />
        </div>
      ) : null}
    </div>
  );
};

export default CardList;

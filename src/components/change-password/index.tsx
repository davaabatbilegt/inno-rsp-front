import CustomButton from '@/components/custom-button';
import { Button } from '@/components/ui/button';
import {
  Dialog,
  DialogContent,
  DialogHeader,
  DialogTitle,
} from '@/components/ui/dialog';
import {
  Form,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from '@/components/ui/form';
import { Colors } from '@/constants/color.constants';
import { useAuth } from '@/contexts/auth';
import { ProfileParamType, editProfile } from '@/services/auth.service';
import { useMutation } from '@tanstack/react-query';
import { useTranslations } from 'next-intl';
import React, { useEffect } from 'react';
import { toast } from 'sonner';
import { z } from 'zod';

import { Input } from '../ui/input';
import { usePasswordValidation } from './validation';

type Props = {
  open: boolean;
  onClose: () => void;
};

const ChangePasswordForm: React.FC<Props> = (props: Props) => {
  const t = useTranslations();
  const { form, schema } = usePasswordValidation();
  const { open, onClose } = props;
  const { userData, refetchUserInfo } = useAuth();

  const { mutateAsync, isPending } = useMutation({
    mutationFn: (param: ProfileParamType) => editProfile(param),
    onSuccess: () => {
      toast.success(t('common.requests.success.change_password'));
      refetchUserInfo();
      onClose();
    },
    onError: () => {
      toast.error(t('common.requests.error.system'));
    },
  });

  const onSubmit = (values: z.infer<typeof schema>) => {
    if (userData?.id) {
      mutateAsync({
        id: userData?.id,
        type: `${userData?.type}`,
        params: {
          password: values.password,
        },
      });
    } else {
      toast.error(t('common.requests.error.system'));
    }
  };

  useEffect(() => {
    form.reset();
  }, []);

  return (
    <Dialog
      open={open}
      onOpenChange={() => {
        onClose();
      }}
    >
      <DialogContent className="sm:max-w-[425px]">
        <DialogHeader>
          <DialogTitle>{t('profile.change_password')}</DialogTitle>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-4">
            <FormField
              control={form.control}
              name="password"
              render={({ field }) => (
                <FormItem className="w-full">
                  <FormLabel className="font-bold">
                    {t('profile.new_password')}
                  </FormLabel>
                  <Input placeholder={t('profile.new_password')} {...field} />
                  <FormMessage />
                </FormItem>
              )}
            />
            <div className="flex w-full justify-end">
              <Button
                className="mr-5 w-full"
                variant={'outline'}
                onClick={() => onClose()}
              >
                {t('profile.cancel')}
              </Button>
              <CustomButton
                text={t('profile.submit')}
                type="submit"
                className={`w-full ${Colors.BLUE} ${Colors.HOVER_BLUE}`}
                loading={isPending}
              />
            </div>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};

export default ChangePasswordForm;

'use client';

import { zodResolver } from '@hookform/resolvers/zod';
import { useTranslations } from 'next-intl';
import { useForm } from 'react-hook-form';
import { z } from 'zod';

const initialValues = {
  password: '',
};

export const usePasswordValidation = () => {
  const t = useTranslations();

  const schema = z.object({
    password: z
      .string()
      .min(1, { message: t('profile.validation.password_required') }),
  });

  const form = useForm<z.infer<typeof schema>>({
    resolver: zodResolver(schema),
    defaultValues: initialValues,
  });

  return { schema, form };
};

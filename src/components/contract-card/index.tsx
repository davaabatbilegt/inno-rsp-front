'use client';

import { Colors } from '@/constants/color.constants';
import { USER_TYPES } from '@/constants/common.constants';
import { PRIVATE_ROUTES } from '@/constants/routes.constants';
import { ContractListType } from '@/constants/types.constants';
import { OpenDialogType } from '@/containers/contracts/list';
import { useAuth } from '@/contexts/auth';
import { useRouter } from '@/lib/navigation';
import { formatMoney, orderableByDate } from '@/lib/utils';
import { ContractType } from '@/services/contract.service';
import {
  CheckCheck,
  CircleSlash,
  MoreVertical,
  Pencil,
  Plus,
} from 'lucide-react';
import { useTranslations } from 'next-intl';
import Image from 'next/image';

import AvatarImage from '../avatar-image';
import CustomButton from '../custom-button';
import CustomTooltip from '../custom-tooltip';
import { Badge } from '../ui/badge';
import { Button } from '../ui/button';
import {
  HoverCard,
  HoverCardContent,
  HoverCardTrigger,
} from '../ui/hover-card';
import { Popover, PopoverContent, PopoverTrigger } from '../ui/popover';

type Props = {
  type?: ContractListType;
  data: ContractType | undefined;
  loading?: boolean;
  handleChangeOpenDialog: (v: OpenDialogType) => void;
  handleChangeSelectedContractId: (v: string) => void;
};

const ContractCard: React.FC<Props> = (props: Props) => {
  const {
    type,
    data,
    loading,
    handleChangeOpenDialog,
    handleChangeSelectedContractId,
  } = props;

  const t = useTranslations();
  const { userData } = useAuth();
  const router = useRouter();

  if (!data) return <></>;

  const handleAccept = () => {
    handleChangeOpenDialog('accept');
    handleChangeSelectedContractId(String(data.id));
  };

  const handleEdit = () => {
    handleChangeOpenDialog('edit');
    handleChangeSelectedContractId(String(data.id));
  };

  const handleCancel = () => {
    handleChangeOpenDialog('cancel');
    handleChangeSelectedContractId(String(data.id));
  };

  const handleCreateOrder = () => {
    handleChangeOpenDialog(null);
    handleChangeSelectedContractId(String(data.id));
  };

  return (
    <div className="h-full max-w-sm flex-col overflow-hidden rounded shadow-lg">
      <div className="relative flex h-[250px]">
        <Image
          alt="restaurant-image"
          src="/images/carbonara.png"
          fill
          className="object-cover"
        />
        <div className="absolute right-2 top-2">
          {userData?.type === USER_TYPES.EMPLOYEE &&
            orderableByDate(data.menu.days) && (
              <CustomTooltip content={t('menus.create_order_tooltip')}>
                <Button
                  onClick={handleCreateOrder}
                  className="h-[40px] w-[40px] rounded-full bg-green-700 p-0 hover:bg-green-500"
                >
                  <Plus color="#fff" className="h-4 w-4" />
                </Button>
              </CustomTooltip>
            )}

          {userData?.type !== USER_TYPES.EMPLOYEE && (
            <Popover>
              <PopoverTrigger>
                <MoreVertical
                  color="#ffffff"
                  className="h-6 w-6 hover:cursor-pointer"
                />
              </PopoverTrigger>
              <PopoverContent align="end" className="z-10 max-w-[150px] p-1">
                <div className="flex w-full flex-col gap-2">
                  {type === 'receiving' && (
                    <CustomButton
                      icon={
                        <CheckCheck color="#ffffff" className="mr-2 h-4 w-4" />
                      }
                      text={t('contracts.accept_contract')}
                      variant="default"
                      onClick={handleAccept}
                      loading={loading}
                      className={`h-8 w-full ${Colors.GREEN} ${Colors.HOVER_GREEN}`}
                    />
                  )}

                  <CustomButton
                    icon={<Pencil color="#ffffff" className="mr-2 h-4 w-4" />}
                    text={t('contracts.edit_contract')}
                    variant="outline"
                    onClick={handleEdit}
                    loading={loading}
                    className={`h-8 w-full text-white hover:text-white ${Colors.BLUE} ${Colors.HOVER_BLUE}`}
                  />

                  <CustomButton
                    icon={
                      <CircleSlash color="#ffffff" className="mr-2 h-4 w-4" />
                    }
                    text={
                      type === 'receiving'
                        ? t('contracts.reject_contract')
                        : t('contracts.cancel_contract')
                    }
                    onClick={handleCancel}
                    loading={loading}
                    className={`h-8 w-full ${Colors.RED} ${Colors.HOVER_RED}`}
                  />
                </div>
              </PopoverContent>
            </Popover>
          )}
        </div>

        {userData?.type !== USER_TYPES.EMPLOYEE && (
          <div className="absolute left-1 top-1">
            <HoverCard>
              <HoverCardTrigger>
                <div className="h-12 w-12">
                  <AvatarImage
                    username={
                      userData?.type === USER_TYPES.RESTAURANT
                        ? data.organization?.username
                        : data.restaurant?.username
                    }
                    size={12}
                  />
                </div>
              </HoverCardTrigger>
              <HoverCardContent align="start">
                <div
                  className="z-20 flex cursor-pointer flex-row items-center gap-2"
                  onClick={() => {
                    if (userData?.type === USER_TYPES.ORGANIZATION) {
                      router.push(
                        `${PRIVATE_ROUTES.ORGANIZATION_RESTAURANTS}/${data.restaurantId}`
                      );
                    } else if (userData?.type === USER_TYPES.EMPLOYEE) {
                      router.push(
                        `${PRIVATE_ROUTES.EMPLOYEE_RESTAURANTS}/${data.restaurantId}`
                      );
                    } else {
                      router.push(
                        `${PRIVATE_ROUTES.RESTAURANT_ORGANIZATIONS}/${data.organizationId}`
                      );
                    }
                  }}
                >
                  <div className="h-12 w-12">
                    <AvatarImage
                      username={
                        userData?.type === USER_TYPES.RESTAURANT
                          ? data.organization?.username
                          : data.restaurant?.username
                      }
                      size={12}
                    />
                  </div>
                  <div className="flex items-center text-lg">
                    {userData?.type === USER_TYPES.RESTAURANT
                      ? data.organization?.name
                      : data.restaurant?.name}
                  </div>
                </div>
              </HoverCardContent>
            </HoverCard>
          </div>
        )}
      </div>
      <div className="flex h-[calc(100%-250px)] flex-col justify-between px-5 pb-2 pt-4">
        <div className="mb-3">
          <div className="mb-2 text-lg font-bold">{data?.menu?.name}</div>
          <p className="text-sm text-gray-700">{data?.menu?.description}</p>
        </div>
        <div className="mb-3">
          <div className="flex flex-row items-center gap-2 text-sm">
            {t('contracts.order_time_frame', { time: data.orderLimitTime })}
          </div>
          <Badge variant="outline" className="text-md mt-3">
            {formatMoney(data.menuUnitPrice)}
          </Badge>
        </div>
      </div>
    </div>
  );
};

export default ContractCard;

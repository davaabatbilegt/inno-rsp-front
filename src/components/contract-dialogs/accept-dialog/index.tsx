'use client';

import { Button } from '@/components/ui/button';
import {
  Dialog,
  DialogClose,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
} from '@/components/ui/dialog';
import { useTranslations } from 'next-intl';

type Props = {
  open: boolean;
  handleClose: () => void;
  onSubmit: () => void;
};

const ContractAcceptDialog: React.FC<Props> = (props: Props) => {
  const { open, handleClose, onSubmit } = props;
  const t = useTranslations();

  return (
    <Dialog open={open} onOpenChange={handleClose}>
      <DialogClose onClick={handleClose} />
      <DialogContent className="sm:max-w-[425px]">
        <DialogHeader>
          <DialogTitle>{t('contracts.accept_dialog.title')}</DialogTitle>
          <DialogDescription>
            {t('contracts.accept_dialog.description')}
          </DialogDescription>
        </DialogHeader>
        <DialogFooter>
          <Button variant="outline" onClick={handleClose}>
            {t('common.no')}
          </Button>
          <Button variant="default" className="bg-green-700" onClick={onSubmit}>
            {t('common.yes')}
          </Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};

export default ContractAcceptDialog;

'use client';

import { Button } from '@/components/ui/button';
import {
  Dialog,
  DialogClose,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
} from '@/components/ui/dialog';
import { Colors } from '@/constants/color.constants';
import { useTranslations } from 'next-intl';

type Props = {
  open: boolean;
  handleClose: () => void;
  onSubmit: () => void;
};

const ContractCancelDialog: React.FC<Props> = (props: Props) => {
  const { open, handleClose, onSubmit } = props;
  const t = useTranslations();

  return (
    <Dialog open={open} onOpenChange={handleClose}>
      <DialogClose onClick={handleClose} />
      <DialogContent className="sm:max-w-[425px]">
        <DialogHeader>
          <DialogTitle>{t('contracts.cancel_dialog.title')}</DialogTitle>
          <DialogDescription>
            {t('contracts.cancel_dialog.description')}
          </DialogDescription>
        </DialogHeader>
        <DialogFooter>
          <Button
            variant="outline"
            onClick={handleClose}
            className="mt-4 w-full"
          >
            {t('common.no')}
          </Button>
          <Button
            variant="destructive"
            onClick={onSubmit}
            className={`mt-4 w-full ${Colors.RED} ${Colors.HOVER_RED}`}
          >
            {t('common.yes')}
          </Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};

export default ContractCancelDialog;

'use client';

import {
  ContractValuesType,
  useContractValidation,
} from '@/components/contract-dialogs/validation';
import CustomButton from '@/components/custom-button';
import { Badge } from '@/components/ui/badge';
import { Button } from '@/components/ui/button';
import {
  Dialog,
  DialogContent,
  DialogFooter,
  DialogTitle,
} from '@/components/ui/dialog';
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from '@/components/ui/form';
import { Input } from '@/components/ui/input';
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from '@/components/ui/select';
import { Colors } from '@/constants/color.constants';
import { STATUS } from '@/constants/common.constants';
import { QUERY_KEYS } from '@/constants/query-keys';
import { useAuth } from '@/contexts/auth';
import { formatMoney } from '@/lib/utils';
import { ContractType } from '@/services/contract.service';
import { getMenus } from '@/services/menu.service';
import { useQuery } from '@tanstack/react-query';
import { useTranslations } from 'next-intl';
import { z } from 'zod';

type Props = {
  data?: ContractType;
  open: boolean;
  handleClose: () => void;
  onSubmit: (values: ContractValuesType) => void;
  loading?: boolean;
  restaurantId?: number;
};

const PAGE_SIZE = 200;

const ContractCreateDialog: React.FC<Props> = (props: Props) => {
  const { data, open, handleClose, onSubmit, loading, restaurantId } = props;
  const t = useTranslations();

  const { contractedData } = useAuth();

  const { data: menuData, isLoading } = useQuery({
    queryKey: [QUERY_KEYS.MENU_LIST, data?.restaurantId, restaurantId],
    queryFn: () =>
      getMenus({
        restaurantId: String(data?.restaurantId || restaurantId),
        status: STATUS.ACTIVE,
        limit: PAGE_SIZE,
        page: 1,
      }),
    enabled:
      typeof data?.restaurantId === 'number' ||
      typeof restaurantId === 'number',
  });

  const { schema, form } = useContractValidation({
    menuId: String(data?.menuId || ''),
    price: String(data?.menuUnitPrice || ''),
    limitTime: String(data?.orderLimitTime || ''),
    limitCount: String(data?.orderLimitFoodCount || ''),
  });

  const handleSubmit = (values: z.infer<typeof schema>) => {
    onSubmit(values);
  };

  return (
    <Dialog open={open} onOpenChange={handleClose}>
      <DialogContent className="sm:max-w-[425px]">
        <DialogTitle>{t('contracts.create_dialog.title')}</DialogTitle>
        <Form {...form}>
          <form
            onSubmit={form.handleSubmit(handleSubmit)}
            className="space-y-4"
          >
            {(!!data || !!restaurantId) && (
              <FormField
                control={form.control}
                name="menuId"
                render={({ field }) => (
                  <FormItem className="w-full">
                    <FormLabel className="font-bold">
                      {t('contracts.create_dialog.restaurant_menu.title')}
                    </FormLabel>
                    <Select
                      onValueChange={field.onChange}
                      value={field.value}
                      disabled={isLoading}
                    >
                      <FormControl className="h-auto overflow-hidden">
                        <SelectTrigger>
                          <SelectValue
                            placeholder={t(
                              'contracts.create_dialog.restaurant_menu.title'
                            )}
                          />
                        </SelectTrigger>
                      </FormControl>
                      <SelectContent>
                        {menuData?.items.map((item) => {
                          const isContracted = contractedData?.items?.some(
                            (value) => value.menuId === item.id
                          );
                          return (
                            <SelectItem
                              key={item.id}
                              value={String(item.id)}
                              disabled={isContracted}
                            >
                              <span className="flex flex-row items-center">
                                <Badge className="mr-2 bg-blue-600">
                                  {formatMoney(item.price)}
                                </Badge>
                                <span className="flex flex-row items-center gap-2">
                                  {item.name}
                                  {isContracted && (
                                    <span className="font-normal italic">
                                      ({t('contracts.already_contracted')})
                                    </span>
                                  )}
                                </span>
                              </span>
                            </SelectItem>
                          );
                        })}
                      </SelectContent>
                    </Select>
                    <FormMessage />
                  </FormItem>
                )}
              />
            )}

            <FormField
              control={form.control}
              name="price"
              render={({ field }) => (
                <FormItem className="mt-2 w-full">
                  <FormLabel className="font-bold">
                    {t('contracts.create_dialog.price.title')}
                  </FormLabel>
                  <FormControl>
                    <Input
                      placeholder={t('contracts.create_dialog.price.title')}
                      type="number"
                      {...field}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="limitTime"
              render={({ field }) => (
                <FormItem className="mt-2 w-full">
                  <FormLabel className="font-bold">
                    {t('contracts.create_dialog.order_limit_time.title')}
                  </FormLabel>
                  <FormControl>
                    <Input
                      placeholder={t(
                        'contracts.create_dialog.order_limit_time.title'
                      )}
                      type="time"
                      {...field}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="limitCount"
              render={({ field }) => (
                <FormItem className="mt-2 w-full">
                  <FormLabel className="font-bold">
                    {t('contracts.create_dialog.order_limit_count.title')}
                  </FormLabel>
                  <FormControl>
                    <Input
                      placeholder={t(
                        'contracts.create_dialog.order_limit_count.title'
                      )}
                      type="number"
                      {...field}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />

            <DialogFooter>
              <Button
                variant="outline"
                onClick={handleClose}
                className="mt-4 w-full"
              >
                {t('contracts.create_dialog.cancel')}
              </Button>
              <CustomButton
                text={t('contracts.create_dialog.request')}
                variant="default"
                type="submit"
                disabled={
                  String(data?.menuId) === form.watch('menuId') &&
                  String(data?.menuUnitPrice) === form.watch('price') &&
                  String(data?.orderLimitTime) === form.watch('limitTime') &&
                  String(data?.orderLimitFoodCount) === form.watch('limitCount')
                }
                loading={loading}
                className={`mt-4 w-full ${Colors.GREEN} ${Colors.HOVER_GREEN}`}
              />
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};

export default ContractCreateDialog;

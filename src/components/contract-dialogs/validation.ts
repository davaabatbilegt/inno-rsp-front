'use client';

import { zodResolver } from '@hookform/resolvers/zod';
import dayjs from 'dayjs';
import { useTranslations } from 'next-intl';
import { useForm } from 'react-hook-form';
import { z } from 'zod';

export type ContractValuesType = {
  menuId: string;
  price: string;
  limitTime: string;
  limitCount: string;
};

const initialValues: ContractValuesType = {
  menuId: '',
  price: '',
  limitTime: dayjs().format('HH:mm'),
  limitCount: '',
};

export const useContractValidation = (initialData?: ContractValuesType) => {
  const t = useTranslations();

  const schema = z.object({
    menuId: z
      .string()
      .min(1, { message: t('login.validation.username_required') }),
    price: z.string().regex(/^\d+$/),
    limitTime: z.string().regex(/^([01]\d|2[0-3]):?([0-5]\d)(:?([0-5]\d))?$/, {
      message: 'Invalid time format. Please use HH:mm:ss or HH:mm.',
    }),
    limitCount: z.string().regex(/^\d+$/),
  });

  const form = useForm<z.infer<typeof schema>>({
    resolver: zodResolver(schema),
    defaultValues: initialData || initialValues,
  });

  return { schema, form };
};

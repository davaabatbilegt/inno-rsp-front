'use client';

import { Loader2 } from 'lucide-react';

import { Button } from '../ui/button';

type Props = {
  icon?: React.ReactNode;
  text: string;
  type?: 'button' | 'reset' | 'submit';
  variant?:
    | 'default'
    | 'destructive'
    | 'outline'
    | 'secondary'
    | 'ghost'
    | 'link';
  size?: 'default' | 'sm' | 'lg' | 'icon';
  loading?: boolean;
  disabled?: boolean;
  asChild?: boolean;
  className?: string;
  onClick?: () => void;
};

const CustomButton: React.FC<Props> = (props: Props) => {
  const {
    icon,
    text,
    type,
    variant = 'default',
    size = 'default',
    loading,
    disabled,
    asChild = false,
    className = '',
    onClick,
  } = props;

  return (
    <Button
      type={type}
      variant={variant}
      size={size}
      disabled={disabled || loading}
      asChild={asChild}
      onClick={onClick}
      className={className}
    >
      {loading ? (
        <Loader2 className="mr-2 h-4 w-4 animate-spin" />
      ) : icon ? (
        <span className="mr-2">{icon}</span>
      ) : (
        <></>
      )}
      {text}
    </Button>
  );
};

export default CustomButton;

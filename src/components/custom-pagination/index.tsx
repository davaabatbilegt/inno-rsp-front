import {
  Pagination,
  PaginationContent,
  PaginationEllipsis,
  PaginationItem,
  PaginationLink,
  PaginationNext,
  PaginationPrevious,
} from '@/components/ui/pagination';
import { usePathname } from '@/lib/navigation';
import { ChevronLeft, ChevronRight } from 'lucide-react';

import { Button } from '../ui/button';
import { Skeleton } from '../ui/skeleton';

type Props = {
  page?: number;
  totalPage: number | undefined;
  loading?: boolean;
};

const CustomPagination: React.FC<Props> = (props: Props) => {
  const { page = 1, totalPage = 1, loading = false } = props;

  const pathname = usePathname();

  const previousPage = page - 1;
  const nextPage = page + 1;
  const isFirstPage = page === 1;
  const isLastPage = page === totalPage;

  const DisabledPaginationPrevious = () => (
    <Button
      variant="ghost"
      disabled
      className="flex items-center gap-1 pl-2.5 first-line:flex-row"
    >
      <ChevronLeft className="h-4 w-4" />
      <span>Previous</span>
    </Button>
  );

  const DisabledPaginationNext = () => (
    <Button
      variant="ghost"
      disabled
      className="flex flex-row items-center gap-1 pr-2.5"
    >
      <span>Next</span>
      <ChevronRight className="h-4 w-4" />
    </Button>
  );

  const calculatePaginationData = (
    current: number,
    total: number
  ): number[] => {
    const result = [1];
    const ellipsis = -1;
    const prev = current - 1;
    const next = current + 1;

    if (
      current > total ||
      typeof current !== 'number' ||
      typeof total !== 'number'
    )
      return result;

    if (current - 1 >= 3) result.push(ellipsis);
    if (prev > 1 && prev < total) result.push(prev);
    if (current > 1 && current < total) result.push(current);
    if (next > 1 && next < total) result.push(next);
    if (total - current >= 3) result.push(ellipsis);
    if (total !== 1) result.push(total);

    return result;
  };

  return loading ? (
    <div className="flex w-full justify-center gap-4">
      <Skeleton className="h-[40px] w-[100px] rounded-xl" />
      <Skeleton className="h-[40px] w-[40px] rounded-xl" />
      <Skeleton className="h-[40px] w-[40px] rounded-xl" />
      <Skeleton className="h-[40px] w-[40px] rounded-xl" />
      <Skeleton className="h-[40px] w-[100px] rounded-xl" />
    </div>
  ) : (
    <Pagination>
      <PaginationContent>
        {isFirstPage ? (
          <DisabledPaginationPrevious />
        ) : (
          <PaginationItem>
            <PaginationPrevious href={`${pathname}?page=${previousPage}`} />
          </PaginationItem>
        )}

        {calculatePaginationData(page, totalPage).map((item, index) => {
          if (item === -1) {
            return (
              <PaginationItem key={index}>
                <PaginationEllipsis />
              </PaginationItem>
            );
          } else
            return (
              <PaginationItem key={index}>
                <PaginationLink
                  href={`${pathname}?page=${item}`}
                  isActive={item === page}
                >
                  {item}
                </PaginationLink>
              </PaginationItem>
            );
        })}

        {isLastPage ? (
          <DisabledPaginationNext />
        ) : (
          <PaginationItem>
            <PaginationNext href={`${pathname}?page=${nextPage}`} />
          </PaginationItem>
        )}
      </PaginationContent>
    </Pagination>
  );
};

export default CustomPagination;

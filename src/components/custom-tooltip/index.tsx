import {
  Tooltip,
  TooltipContent,
  TooltipProvider,
  TooltipTrigger,
} from '../ui/tooltip';

type Props = {
  content: React.ReactElement | string;
  children: React.ReactElement;
  side?: 'bottom' | 'top' | 'right' | 'left';
  align?: 'center' | 'end' | 'start';
};

const CustomTooltip: React.FC<Props> = (props: Props) => {
  const { content, children, side = 'top', align = 'center' } = props;

  return (
    <TooltipProvider delayDuration={200} skipDelayDuration={0}>
      <Tooltip>
        <TooltipTrigger asChild>{children}</TooltipTrigger>
        <TooltipContent side={side} align={align}>
          {content}
        </TooltipContent>
      </Tooltip>
    </TooltipProvider>
  );
};

export default CustomTooltip;

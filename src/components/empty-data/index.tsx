import { CircleSlash } from 'lucide-react';
import { useTranslations } from 'next-intl';
import React from 'react';

type Props = {
  size?: 'small' | 'medium' | 'large';
};

const EmptyData: React.FC<Props> = (props: Props) => {
  const { size = 'medium' } = props;
  const t = useTranslations();

  const SIZES_LIST = {
    small: {
      text: 'xs',
      icon: 18,
      margin: 0,
    },
    medium: {
      text: 'sm',
      icon: 24,
      margin: 1,
    },
    large: {
      text: 'md',
      icon: 28,
      margin: 3,
    },
  };

  return (
    <div className="flex h-full flex-col items-center justify-center py-1">
      <CircleSlash size={SIZES_LIST[size].icon} />
      <p
        className={`text-${SIZES_LIST[size].text} mt-${SIZES_LIST[size].margin} text-gray-500`}
      >
        {t('common.no_data')}
      </p>
    </div>
  );
};

export default EmptyData;

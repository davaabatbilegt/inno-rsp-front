import { Loader2 } from 'lucide-react';
import { useTranslations } from 'next-intl';
import React from 'react';

type Props = {
  loading?: boolean;
  size?: number;
};

const FullLoader: React.FC<Props> = (props: Props) => {
  const { size = 48 } = props;
  const t = useTranslations();

  return props.loading ? (
    <div className="fixed left-0 top-0 z-50 flex h-full w-full items-center justify-center bg-white bg-opacity-75">
      <div role="status">
        <Loader2
          className="animate-spin "
          color="rgb(5, 150, 105)"
          size={size}
        />
        <span className="sr-only">{t('common.loading')}</span>
      </div>
    </div>
  ) : (
    <></>
  );
};

export default FullLoader;

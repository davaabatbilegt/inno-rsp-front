'use client';

import React from 'react';

type Props = {
  type?: string;
  lat: number;
  lng: number;
};

const GoogleMap: React.FC<Props> = (props: Props) => {
  const { type = 'cosy' } = props;

  return (
    <iframe
      src={
        type === 'ikhmongol'
          ? 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d546.1801969652824!2d106.90844687512309!3d47.91287331229674!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x5d96924e24c19e25%3A0x427261cd18c10e0a!2sGreat%20Mongol!5e0!3m2!1sen!2smn!4v1713259216601!5m2!1sen!2smn'
          : 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1092.410337350547!2d106.91320664334806!3d47.9105073469251!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x5d96924eb020d6f3%3A0x72496fa97d9caaae!2sNCP%20Sport%20Complex!5e0!3m2!1sen!2smn!4v1713258438403!5m2!1sen!2smn'
      }
      width="100%"
      height="400"
      style={{ border: 0 }}
      allowFullScreen
      loading="lazy"
      referrerPolicy="no-referrer-when-downgrade"
    />
  );
};

export default GoogleMap;

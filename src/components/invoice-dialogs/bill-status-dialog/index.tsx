import CustomButton from '@/components/custom-button';
import { Button } from '@/components/ui/button';
import {
  Dialog,
  DialogClose,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
} from '@/components/ui/dialog';
import { Label } from '@/components/ui/label';
import { RadioGroup, RadioGroupItem } from '@/components/ui/radio-group';
import { Colors } from '@/constants/color.constants';
import { BILL_STATUS } from '@/constants/common.constants';
import {
  InvoiceEditParamType,
  InvoiceType,
  editInvoice,
} from '@/services/invoice.service';
import { useMutation } from '@tanstack/react-query';
import { useTranslations } from 'next-intl';
import React, { useEffect, useState } from 'react';
import { toast } from 'sonner';

type Props = {
  open: boolean;
  data: InvoiceType | null;
  onClose: () => void;
  refetch: () => void;
};

const DialogInvoiceBillStatus: React.FC<Props> = (props: Props) => {
  const t = useTranslations();
  const { open, data, onClose, refetch } = props;
  const [billStatus, setBillStatus] = useState(BILL_STATUS.BILLED);
  const [description, setDescription] = useState(
    t('invoices.status_dialog.billed_desc')
  );

  useEffect(() => {
    if (data?.billStatus === BILL_STATUS.BILLED) {
      setBillStatus(BILL_STATUS.CONFIRMED);
      t('invoices.status_dialog.billed_desc');
    } else if (data?.billStatus === BILL_STATUS.NOT_PAID) {
      setBillStatus(BILL_STATUS.BILLED);
      setDescription(t('invoices.status_dialog.not_paid_desc'));
    } else if (data?.billStatus === BILL_STATUS.CALCULATED) {
      setDescription(t('invoices.status_dialog.billed_desc'));
    }
  }, [open]);

  const { mutateAsync, isPending } = useMutation({
    mutationFn: (param: InvoiceEditParamType) => editInvoice(param),
    onSuccess: () => {
      toast.success(t('common.requests.success.change_invoice_bill_status'));
      refetch();
      onClose();
    },
    onError: () => {
      toast.error(t('common.requests.error.system'));
    },
  });

  const onSubmit = () => {
    if (data?.id) {
      mutateAsync({
        id: data.id,
        params: {
          billStatus: billStatus,
        },
      });
    } else {
      toast.error(t('common.requests.error.system'));
    }
  };

  return (
    <Dialog open={open} onOpenChange={() => onClose()}>
      <DialogContent className="sm:max-w-[425px]">
        <DialogHeader>
          <DialogTitle>{t('invoices.status_dialog.title')}</DialogTitle>
          <DialogDescription>{description}</DialogDescription>
          {data?.billStatus === BILL_STATUS.BILLED ? (
            <RadioGroup
              onValueChange={(checked: BILL_STATUS) => setBillStatus(checked)}
              defaultValue={BILL_STATUS.CONFIRMED}
              className="flex flex-row space-x-6 pt-2"
            >
              <div className="flex items-center space-x-4 space-y-0">
                <RadioGroupItem value={BILL_STATUS.CONFIRMED} />
                <Label>{t('invoices.status_dialog.confirm')}</Label>
              </div>
              <div className="flex items-center space-x-4 space-y-0">
                <RadioGroupItem value={BILL_STATUS.NOT_PAID} />
                <Label>{t('invoices.status_dialog.not_paid')}</Label>
              </div>
            </RadioGroup>
          ) : null}
        </DialogHeader>
        <DialogClose onClick={() => onClose()} />
        <DialogFooter>
          <Button
            variant="outline"
            onClick={() => onClose()}
            className="w-full"
          >
            {t('common.no')}
          </Button>
          <CustomButton
            text={t('common.yes')}
            onClick={() => onSubmit()}
            className={`w-full ${Colors.BLUE} ${Colors.HOVER_BLUE}`}
            loading={isPending}
          />
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};

export default DialogInvoiceBillStatus;

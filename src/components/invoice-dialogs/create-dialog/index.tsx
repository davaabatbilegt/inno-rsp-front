'use client';

import { useInvoiceValidation } from '@/components/invoice-form/valication';
import {
  Dialog,
  DialogContent,
  DialogHeader,
  DialogTitle,
} from '@/components/ui/dialog';
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from '@/components/ui/form';
import { Input } from '@/components/ui/input';
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from '@/components/ui/select';
import { Colors } from '@/constants/color.constants';
import { CreateInvoiceParamType } from '@/services/invoice.service';
import { OrganizationType } from '@/services/organization.service';
import { useTranslations } from 'next-intl';
import React from 'react';
import { z } from 'zod';

import CustomButton from '../../custom-button';
import { Button } from '../../ui/button';

type Props = {
  open: boolean;
  handleClose: () => void;
  onSubmit: (values: CreateInvoiceParamType) => void;
  loading?: boolean;
  organizationList: OrganizationType[];
};

const DialogInvoiceCreate: React.FC<Props> = (props: Props) => {
  const { open, handleClose, onSubmit, loading, organizationList } = props;
  const t = useTranslations();

  const { schema, form } = useInvoiceValidation();

  const handleSubmit = (values: z.infer<typeof schema>) => {
    onSubmit({
      organizationId: values.organization_id,
      yearMonth: `${values.month}-01`,
    });
  };

  return (
    <Dialog open={open} onOpenChange={handleClose}>
      <DialogContent className="sm:max-w-[425px]">
        <DialogHeader>
          <DialogTitle>{t('invoices.create_dialog.title')}</DialogTitle>
        </DialogHeader>
        <Form {...form}>
          <form
            onSubmit={form.handleSubmit(handleSubmit)}
            className="space-y-4"
          >
            <FormField
              control={form.control}
              name="organization_id"
              render={({ field }) => (
                <FormItem className="mt-2 w-full">
                  <FormLabel className="font-bold">
                    {t('invoices.create_dialog.organization')}
                  </FormLabel>
                  <FormControl>
                    <Select onValueChange={field.onChange} value={field.value}>
                      <SelectTrigger>
                        <SelectValue
                          placeholder={t('invoices.create_dialog.organization')}
                        />
                      </SelectTrigger>
                      <SelectContent>
                        {organizationList.map((item) => (
                          <SelectItem key={item.id} value={item.id.toString()}>
                            {item.name}
                          </SelectItem>
                        ))}
                      </SelectContent>
                    </Select>
                  </FormControl>
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="month"
              render={({ field }) => (
                <FormItem className="mt-2 w-full">
                  <FormLabel className="font-bold">
                    {t('invoices.create_dialog.month')}
                  </FormLabel>
                  <FormControl>
                    <Input
                      placeholder={t('invoices.create_dialog.month')}
                      type="month"
                      {...field}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <div className="flex w-full justify-end">
              <Button
                className="mr-5 w-full"
                variant={'outline'}
                onClick={handleClose}
              >
                {t('common.cancel')}
              </Button>
              <CustomButton
                text={t('common.create')}
                variant="default"
                type="submit"
                loading={loading}
                className={`w-full ${Colors.GREEN} ${Colors.HOVER_GREEN}`}
              />
            </div>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};

export default DialogInvoiceCreate;

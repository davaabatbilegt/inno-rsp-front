'use client';

import { zodResolver } from '@hookform/resolvers/zod';
import dayjs from 'dayjs';
import { useForm } from 'react-hook-form';
import { z } from 'zod';

export type OrganizationValuesType = {
  organization_id: string;
  month: string;
};

const initialValues: OrganizationValuesType = {
  organization_id: '1',
  month: dayjs().format('YYYY-MM'),
};

export const useInvoiceValidation = (initialData?: OrganizationValuesType) => {
  const schema = z.object({
    organization_id: z.string(),
    month: z.string(),
  });

  const form = useForm<z.infer<typeof schema>>({
    resolver: zodResolver(schema),
    defaultValues: initialData || initialValues,
  });

  return { schema, form };
};

import dayjs from 'dayjs';

const Footer: React.FC = (): JSX.Element => {
  return (
    <footer className="bg-emerald-600 py-4 text-gray-200">
      <div className="container mx-auto text-center text-sm">
        <p>&copy; {dayjs().year()} RSP App. All rights reserved.</p>
      </div>
    </footer>
  );
};

export default Footer;

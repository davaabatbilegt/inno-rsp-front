import {
  CONTRACT_STATUS,
  LOCALES,
  STATUS,
  USER_TYPES,
  UserTypesType,
} from '@/constants/common.constants';
import IMAGES from '@/constants/images.constants';
import { NAV_LIST } from '@/constants/navigation.constants';
import { QUERY_KEYS } from '@/constants/query-keys';
import { GENERAL_ROUTES } from '@/constants/routes.constants';
import { useAuth } from '@/contexts/auth';
import { usePathname, useRouter } from '@/lib/navigation';
import { getContractsList } from '@/services/contract.service';
import { useQuery } from '@tanstack/react-query';
import { Button } from '@ui/button';
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuGroup,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from '@ui/dropdown-menu';
import _ from 'lodash';
import { LogOut, Menu, SquareUser, User } from 'lucide-react';
import { useTranslations } from 'next-intl';
import Image from 'next/image';
import Link from 'next/link';

import AvatarImage from '../avatar-image';

type Props = {
  type: UserTypesType;
  locale: string;
};

const Header: React.FC<Props> = (props: Props): JSX.Element => {
  const { userData, logout } = useAuth();
  const router = useRouter();

  const t = useTranslations();
  const pathname = usePathname();
  const userType = pathname.split('/')[1];

  const handleLogout = async () => await logout(props.type);

  const handleChangeLanguage = () =>
    router.replace(pathname, {
      locale: props.locale === LOCALES.EN ? LOCALES.MN : LOCALES.EN,
    });

  const { data } = useQuery({
    queryKey: [QUERY_KEYS.CONTRACT_LIST_RECEIVING],
    queryFn: () => {
      if (userType === USER_TYPES.ORGANIZATION)
        return getContractsList({
          limit: 1,
          restaurantContractStatus: CONTRACT_STATUS.ACCEPTED,
          organizationContractStatus: CONTRACT_STATUS.PENDING,
          status: STATUS.ACTIVE,
        });

      return getContractsList({
        limit: 1,
        restaurantContractStatus: CONTRACT_STATUS.PENDING,
        organizationContractStatus: CONTRACT_STATUS.ACCEPTED,
        status: STATUS.ACTIVE,
      });
    },
    enabled:
      userType === USER_TYPES.ORGANIZATION ||
      userType === USER_TYPES.RESTAURANT,
  });

  return (
    <header className="sticky top-0 z-40 w-full border-b bg-background">
      <div className="border-b border-emerald-500">
        <div className="flex h-16 items-center justify-between px-4">
          <div className="flex flex-row items-center gap-4">
            <div className="relative h-[54px] w-[54px]">
              <Image
                alt="restaurant-logo"
                src={IMAGES.APP_LOGO}
                fill
                className="object-cover"
              />
            </div>
            <DropdownMenu>
              <DropdownMenuTrigger asChild>
                <Button variant="ghost" size="icon" className="lg:hidden">
                  <Menu color="rgb(5, 150, 105)" />
                </Button>
              </DropdownMenuTrigger>
              <DropdownMenuContent align="start" className="w-[200px]">
                <DropdownMenuGroup>
                  {NAV_LIST[props.type]?.map((nav, index) => {
                    const Icon = nav.icon;
                    const isCurrentRoute = pathname.includes(nav.route);
                    const linkClassName = `uppercase cursor-pointer hover:text-emerald-400 ${
                      isCurrentRoute
                        ? 'text-emerald-500 font-bold'
                        : 'transition-colors font-medium'
                    }`;

                    return (
                      <DropdownMenuItem key={index} className={linkClassName}>
                        <Link
                          href={nav.route}
                          className="flex w-full flex-row items-center gap-4"
                        >
                          <Icon size={20} />
                          {t(`nav.${nav.title}`)}
                        </Link>
                      </DropdownMenuItem>
                    );
                  })}
                </DropdownMenuGroup>
              </DropdownMenuContent>
            </DropdownMenu>
          </div>

          <nav className="flex items-center space-x-4 lg:space-x-6">
            {NAV_LIST[props.type]?.map((nav, index) => {
              const isCurrentRoute = pathname.includes(nav.route);
              const linkClassName = `uppercase hidden lg:block ${
                isCurrentRoute
                  ? 'text-emerald-500 font-bold text-lg'
                  : 'transition-colors hover:text-emerald-400 font-medium text-sm'
              }`;

              if (nav.route.includes(GENERAL_ROUTES.CONTRACTS)) {
                return (
                  <div key={index} className="relative hidden lg:block">
                    {Number(data?.meta?.totalItems) > 0 && (
                      <div className="absolute -right-4 -top-2 flex h-[20px] w-[20px] items-center justify-center rounded-full bg-red-500 text-xs font-semibold text-white">
                        {data?.meta.totalItems}
                      </div>
                    )}

                    <Link href={nav.route} className={linkClassName}>
                      {t(`nav.${nav.title}`)}
                    </Link>
                  </div>
                );
              }
              return (
                <Link key={index} href={nav.route} className={linkClassName}>
                  {t(`nav.${nav.title}`)}
                </Link>
              );
            })}
          </nav>
          <div className="flex items-center space-x-4">
            <DropdownMenu>
              <DropdownMenuTrigger asChild>
                <Button
                  variant="ghost"
                  className="relative h-8 w-8 rounded-full"
                >
                  <div className="h-8 w-8">
                    {userData ? (
                      <AvatarImage username={userData?.username} size={8} />
                    ) : (
                      <User />
                    )}
                  </div>
                </Button>
              </DropdownMenuTrigger>
              <DropdownMenuContent className="w-56" align="end" forceMount>
                <DropdownMenuLabel className="font-normal">
                  <div className="flex flex-col space-y-1">
                    <p className="text-sm font-medium leading-none">
                      {userData?.username}
                    </p>
                    <p className="text-xs leading-none text-muted-foreground">
                      {_.get(
                        userData,
                        'name',
                        _.get(userData, 'user.name', '')
                      )}
                    </p>
                  </div>
                </DropdownMenuLabel>
                <DropdownMenuSeparator />
                <DropdownMenuGroup>
                  <DropdownMenuItem
                    className="hover:cursor-pointer"
                    onClick={() =>
                      router.push(
                        `/${pathname.split('/')[0]}/${userType}/profile`
                      )
                    }
                  >
                    <SquareUser size={18} className="mr-2" />
                    {t('nav.profile')}
                  </DropdownMenuItem>
                </DropdownMenuGroup>
                <DropdownMenuSeparator />
                <DropdownMenuItem
                  className="hover:cursor-pointer"
                  onClick={handleChangeLanguage}
                >
                  <div className="flex flex-row items-center gap-2">
                    <Image
                      alt="language-flag"
                      src={
                        props.locale === LOCALES.EN
                          ? IMAGES.MN_ICON
                          : IMAGES.EN_ICON
                      }
                      width={18}
                      height={18}
                      className="rounded-full"
                    />
                    {t('nav.change_language')}
                  </div>
                </DropdownMenuItem>
                <DropdownMenuItem
                  onClick={handleLogout}
                  className="hover:cursor-pointer"
                >
                  <LogOut size={18} className="mr-2" />
                  {t('nav.logout')}
                </DropdownMenuItem>
              </DropdownMenuContent>
            </DropdownMenu>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;

import { UserTypesType } from '@/constants/common.constants';

import Footer from './footer';
import Header from './header';

type Props = {
  children: React.ReactNode;
  type: UserTypesType;
  locale: string;
};

const MainLayout: React.FC<Props> = (props: Props): JSX.Element => {
  return (
    <div className="flex h-screen flex-col">
      <Header type={props.type} locale={props.locale} />
      <main className="flex-1 bg-stone-100 px-10 pb-20 pt-5">
        {props.children}
      </main>
      <Footer />
    </div>
  );
};

export default MainLayout;

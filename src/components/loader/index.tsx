import { Loader2 } from 'lucide-react';
import React from 'react';

type Props = {
  size?: number;
};

const Loader: React.FC<Props> = (props: Props) => {
  const { size = 24 } = props;
  return (
    <Loader2 className="animate-spin " color="rgb(5, 150, 105)" size={size} />
  );
};

export default Loader;

'use client';

import { CONTRACTED_STATUS, USER_TYPES } from '@/constants/common.constants';
import IMAGES from '@/constants/images.constants';
import { PRIVATE_ROUTES } from '@/constants/routes.constants';
import { useAuth } from '@/contexts/auth';
import { useRouter } from '@/lib/navigation';
import { formatMoney } from '@/lib/utils';
import { MenuType } from '@/services/menu.service';
import { Plus } from 'lucide-react';
import { useTranslations } from 'next-intl';
import Image from 'next/image';

import AvatarImage from '../avatar-image';
import CustomTooltip from '../custom-tooltip';
import { Badge } from '../ui/badge';
import { Button } from '../ui/button';
import {
  HoverCard,
  HoverCardContent,
  HoverCardTrigger,
} from '../ui/hover-card';

type Props = {
  data: MenuType | undefined;
  handleChangeSelectedMenuId: (v: string) => void;
  handleOpenCreateMenu: () => void;
  handleOpenCreateOrder: () => void;
};

const MenuCard: React.FC<Props> = (props: Props) => {
  const {
    data,
    handleChangeSelectedMenuId,
    handleOpenCreateMenu,
    handleOpenCreateOrder,
  } = props;
  const t = useTranslations();
  const { userData } = useAuth();
  const router = useRouter();

  if (!data) return <></>;

  const handleCreateContract = () => {
    handleChangeSelectedMenuId(String(data.id));
    handleOpenCreateMenu();
  };

  const handleCreateOrder = () => {
    handleChangeSelectedMenuId(String(data.id));
    handleOpenCreateOrder();
  };

  return (
    <div className="h-full max-w-sm flex-col overflow-hidden rounded shadow-lg hover:bg-slate-50">
      <div className="relative flex h-[250px]">
        <Image
          alt="restaurant-image"
          src="/images/carbonara.png"
          fill
          className="object-cover"
        />
        {data.contracted === CONTRACTED_STATUS.YES && (
          <div className="absolute right-2 top-2">
            <Image
              alt="contracted-image"
              src={IMAGES.CONTRACTED}
              width={32}
              height={32}
              className="object-cover"
            />
          </div>
        )}
        {data.contracted === CONTRACTED_STATUS.NO &&
          userData?.type !== USER_TYPES.EMPLOYEE && (
            <div className="absolute left-2 top-2">
              <CustomTooltip
                content={t('contracts.create_dialog.title')}
                side="bottom"
                align="start"
              >
                <Button
                  onClick={handleCreateContract}
                  className="h-[40px] w-[40px] rounded-full border bg-green-700  p-0 hover:bg-green-500"
                >
                  <Plus color="#fff" className="h-4 w-4 " />
                </Button>
              </CustomTooltip>
            </div>
          )}
        {userData?.type === USER_TYPES.EMPLOYEE && (
          <>
            <div className="absolute right-2 top-2">
              <CustomTooltip content={t('menus.create_order_tooltip')}>
                <Button
                  onClick={handleCreateOrder}
                  className="h-[40px] w-[40px] rounded-full bg-green-700 p-0 hover:bg-green-500"
                >
                  <Plus color="#fff" className="h-4 w-4" />
                </Button>
              </CustomTooltip>
            </div>
            <div className="absolute left-2 top-2">
              <HoverCard>
                <HoverCardTrigger>
                  <AvatarImage username={data.restaurant?.username} size={10} />
                </HoverCardTrigger>
                <HoverCardContent align="start">
                  <div
                    className="z-20 flex cursor-pointer flex-row items-center gap-2"
                    onClick={() =>
                      router.push(
                        `${PRIVATE_ROUTES.EMPLOYEE_RESTAURANTS}/${data.restaurantId}`
                      )
                    }
                  >
                    <AvatarImage
                      username={data.restaurant?.username}
                      size={10}
                    />
                    <div className="flex items-center text-lg">
                      {data.restaurant?.name}
                    </div>
                  </div>
                </HoverCardContent>
              </HoverCard>
            </div>
          </>
        )}
      </div>
      <div className="flex h-[calc(100%-250px)] flex-col justify-between px-5 pb-3 pt-4">
        <div className="mb-3">
          <div className="mb-2 text-lg font-bold">{data.name}</div>
          <p className="text-sm text-gray-700">{data.description}</p>
        </div>
        <div className="mb-3">
          {data.days && (
            <Badge variant="outline">{t(`days.${data.days}`)}</Badge>
          )}
          <p className="mt-2 text-lg font-bold">{formatMoney(data.price)}</p>
        </div>
      </div>
    </div>
  );
};

export default MenuCard;

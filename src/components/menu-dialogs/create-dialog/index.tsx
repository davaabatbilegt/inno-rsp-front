'use client';

import {
  Dialog,
  DialogContent,
  DialogHeader,
  DialogTitle,
} from '@/components/ui/dialog';
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from '@/components/ui/form';
import { Input } from '@/components/ui/input';
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from '@/components/ui/select';
import { Colors } from '@/constants/color.constants';
import { WEEKDAYS } from '@/constants/common.constants';
import { CreateMenuParamType } from '@/services/menu.service';
import { useTranslations } from 'next-intl';
import { z } from 'zod';

import CustomButton from '../../custom-button';
import {
  MenuValuesType,
  useEditMenuValidation,
} from '../../menu-form/validation';
import { Button } from '../../ui/button';

type Props = {
  data?: CreateMenuParamType;
  open: boolean;
  handleClose: () => void;
  onSubmit: (values: MenuValuesType) => void;
  loading?: boolean;
};

const DialogMenusCreate: React.FC<Props> = (props: Props) => {
  const { data, open, handleClose, onSubmit, loading } = props;
  const t = useTranslations();

  const { schema, form } = useEditMenuValidation({
    name: String(),
    description: String(),
    price: String(),
    days: String(),
  });

  const handleSubmit = (values: z.infer<typeof schema>) => {
    onSubmit(values);
  };

  return (
    <Dialog open={open} onOpenChange={handleClose}>
      <DialogContent className="sm:max-w-[425px]">
        <DialogHeader>
          <DialogTitle>{t('menus_manage.create_dialog.title')}</DialogTitle>
        </DialogHeader>
        <Form {...form}>
          <form
            onSubmit={form.handleSubmit(handleSubmit)}
            className="space-y-4"
          >
            <FormField
              control={form.control}
              name="name"
              render={({ field }) => (
                <FormItem className="mt-2 w-full">
                  <FormLabel className="font-bold">
                    {t('menus_manage.form.name_input')}
                  </FormLabel>
                  <FormControl>
                    <Input
                      placeholder={t('menus_manage.form.name')}
                      type="text"
                      {...field}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="description"
              render={({ field }) => (
                <FormItem className="mt-2 w-full">
                  <FormLabel className="font-bold">
                    {t('menus_manage.form.desc_input')}
                  </FormLabel>
                  <FormControl>
                    <Input
                      placeholder={t('menus_manage.form.desc')}
                      type="text"
                      {...field}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="price"
              render={({ field }) => (
                <FormItem className="mt-2 w-full">
                  <FormLabel className="font-bold">
                    {t('menus_manage.form.price_input')}
                  </FormLabel>
                  <FormControl>
                    <Input
                      placeholder={t('menus_manage.form.price')}
                      type="number"
                      {...field}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="days"
              render={({ field }) => (
                <FormItem className="mt-2 w-full">
                  <FormLabel className="font-bold">
                    {t('menus_manage.form.days_input')}
                  </FormLabel>
                  <FormControl>
                    <Select onValueChange={field.onChange} value={field.value}>
                      <SelectTrigger className="w-[180px]">
                        <SelectValue
                          placeholder={t('menus_manage.form.days')}
                        />
                      </SelectTrigger>
                      <SelectContent>
                        {Object.values(WEEKDAYS).map((day) => (
                          <SelectItem key={day} value={day.toUpperCase()}>
                            {day}
                          </SelectItem>
                        ))}
                      </SelectContent>
                    </Select>
                  </FormControl>
                </FormItem>
              )}
            />
            <div className="flex w-full justify-end">
              <Button
                className="mr-5 w-full"
                variant={'outline'}
                onClick={handleClose}
              >
                {t('common.cancel')}
              </Button>
              <CustomButton
                text={t('common.create')}
                variant="default"
                type="submit"
                disabled={
                  String(data?.name) === form.watch('name') &&
                  String(data?.price) === form.watch('price')
                }
                loading={loading}
                className={`w-full ${Colors.GREEN} ${Colors.HOVER_GREEN}`}
              />
            </div>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};

export default DialogMenusCreate;

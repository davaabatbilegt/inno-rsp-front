'use client';

import CustomButton from '@/components/custom-button';
import { Button } from '@/components/ui/button';
import {
  Dialog,
  DialogClose,
  DialogContent,
  DialogFooter,
  DialogHeader,
  DialogTitle,
} from '@/components/ui/dialog';
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from '@/components/ui/form';
import { Input } from '@/components/ui/input';
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from '@/components/ui/select';
import { Colors } from '@/constants/color.constants';
import { WEEKDAYS } from '@/constants/common.constants';
import { EditMenuParamType } from '@/services/menu.service';
import { useTranslations } from 'next-intl';
import { z } from 'zod';

import {
  MenuValuesType,
  useEditMenuValidation,
} from '../../menu-form/validation';

type Props = {
  data?: EditMenuParamType;
  open: boolean;
  handleClose: () => void;
  onSubmit: (values: MenuValuesType) => void;
  loading?: boolean;
};

const DialogMenusEdit: React.FC<Props> = (props: Props) => {
  const { data, open, handleClose, onSubmit } = props;
  const t = useTranslations();

  const { schema, form } = useEditMenuValidation({
    name: String(data?.name),
    description: String(data?.description),
    price: String(data?.price),
    days: String(data?.days),
  });

  const handleSubmit = (values: z.infer<typeof schema>) => {
    onSubmit(values);
  };

  return (
    <Dialog open={open} onOpenChange={handleClose}>
      <DialogContent className="sm:max-w-[425px]">
        <Form {...form}>
          <form onSubmit={form.handleSubmit(handleSubmit)}>
            <DialogHeader>
              <DialogTitle>{t('menus_manage.edit_dialog.title')}</DialogTitle>
            </DialogHeader>
            <DialogClose onClick={handleClose} />
            <div className="mb-5 grid gap-1">
              <div className="grid-cols-4 items-center">
                <FormField
                  control={form.control}
                  name="name"
                  render={({ field }) => (
                    <FormItem className=" w-full">
                      <FormLabel className="font-bold">
                        {t('menus_manage.form.name_input')}
                      </FormLabel>
                      <FormControl>
                        <Input
                          placeholder={t('menus_manage.form.name')}
                          type="text"
                          {...field}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className="grid-cols-4 items-center gap-4">
                <FormField
                  control={form.control}
                  name="description"
                  render={({ field }) => (
                    <FormItem className="mt-2 w-full">
                      <FormLabel className="font-bold">
                        {t('menus_manage.form.desc_input')}
                      </FormLabel>
                      <FormControl>
                        <Input
                          placeholder={t('menus_manage.form.desc')}
                          type="text"
                          {...field}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className="grid-cols-4 items-center gap-4">
                <FormField
                  control={form.control}
                  name="price"
                  render={({ field }) => (
                    <FormItem className="mt-2 w-full">
                      <FormLabel className="font-bold">
                        {t('menus_manage.form.price_input')}
                      </FormLabel>
                      <FormControl>
                        <Input
                          placeholder={t('menus_manage.form.price')}
                          type="number"
                          {...field}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className="grid-cols-4 items-center gap-4">
                <FormField
                  control={form.control}
                  name="days"
                  render={({ field }) => (
                    <FormItem className="mt-2 w-full">
                      <FormLabel className="font-bold">
                        {t('menus_manage.form.days_input')}
                      </FormLabel>
                      <FormControl>
                        <Select
                          onValueChange={field.onChange}
                          value={field.value}
                        >
                          <SelectTrigger className="w-[180px]">
                            <SelectValue
                              placeholder={t('menus_manage.form.days')}
                            />
                          </SelectTrigger>
                          <SelectContent>
                            {Object.values(WEEKDAYS).map((day) => (
                              <SelectItem key={day} value={day.toUpperCase()}>
                                {day}
                              </SelectItem>
                            ))}
                          </SelectContent>
                        </Select>
                      </FormControl>
                    </FormItem>
                  )}
                />
              </div>
            </div>

            <DialogFooter>
              <Button
                variant="outline"
                onClick={handleClose}
                className="mt-4 w-full"
              >
                {t('common.cancel')}
              </Button>
              <CustomButton
                text={t('common.edit')}
                variant="default"
                type="submit"
                disabled={
                  String(data?.name) === form.watch('name') &&
                  String(data?.price) === form.watch('price') &&
                  String(data?.description) === form.watch('description') &&
                  String(data?.days) === form.watch('days')
                }
                className={`mt-4 w-full ${Colors.BLUE} ${Colors.HOVER_BLUE}`}
              />
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};

export default DialogMenusEdit;

'use client';

import { zodResolver } from '@hookform/resolvers/zod';
import { useTranslations } from 'next-intl';
import { useForm } from 'react-hook-form';
import { z } from 'zod';

export type MenuValuesType = {
  name: string;
  description: string;
  price: string;
  days: string;
};

const initialValues: MenuValuesType = {
  name: '',
  description: '',
  price: '',
  days: '',
};

export const useEditMenuValidation = (initialData?: MenuValuesType) => {
  const t = useTranslations();

  const schema = z.object({
    name: z
      .string()
      .min(1, { message: t('menus_manage.validation.name_required') }),
    description: z
      .string()
      .min(1, { message: t('menus_manage.validation.description_required') }),
    price: z
      .string()
      .regex(/^\d+$/, t('menus_manage.validation.price_validation_message')),
    days: z.string(),
  });

  const form = useForm<z.infer<typeof schema>>({
    resolver: zodResolver(schema),
    defaultValues: initialData || initialValues,
  });

  return { schema, form };
};

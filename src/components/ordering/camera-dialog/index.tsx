'use client';

import CustomButton from '@/components/custom-button';
import Loader from '@/components/loader';
import { Badge } from '@/components/ui/badge';
import { Button } from '@/components/ui/button';
import { Dialog, DialogContent } from '@/components/ui/dialog';
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from '@/components/ui/select';
import { Colors } from '@/constants/color.constants';
import { ORDER_STATUS, STATUS, USER_TYPES } from '@/constants/common.constants';
import { QUERY_KEYS } from '@/constants/query-keys';
import { OrderStatusType } from '@/constants/types.constants';
import { useAuth } from '@/contexts/auth';
import {
  DecryptParamType,
  OrderDetailType,
  decrypt,
} from '@/services/order.service';
import { getRestaurantTablesList } from '@/services/restaurant-table.service';
import { useMutation, useQuery } from '@tanstack/react-query';
import _ from 'lodash';
import { User } from 'lucide-react';
import { useTranslations } from 'next-intl';
import QrScanner from 'qr-scanner';
import { useEffect, useRef, useState } from 'react';
import { toast } from 'sonner';

type Props = {
  open: 'table' | 'order' | null;
  handleClose: () => void;
  data: OrderDetailType | null;
  onSubmit: (param: {
    status: OrderStatusType;
    tableId?: number;
    orderId?: number;
    orderDetailId?: number;
  }) => void;
};

type OrderQrDataType = {
  restaurant_id: number;
  order_id: number;
  order_detail_id: number;
};

const QrScanDialog: React.FC<Props> = (props: Props) => {
  const t = useTranslations();
  const { open, handleClose, data, onSubmit } = props;

  const { userData } = useAuth();

  const scanner = useRef<QrScanner>();
  const videoEl = useRef<HTMLVideoElement>(null);

  const [qrOn, setQrOn] = useState<boolean>(true);

  const [qrOrderData, setQrOrderData] = useState<OrderQrDataType | null>(null);
  const [tableId, setTableId] = useState<string>('');

  const { data: tableList, isLoading: tableListIsLoading } = useQuery({
    queryKey: [QUERY_KEYS.RESTAURANT_TABLE_LIST, userData?.id],
    queryFn: () =>
      getRestaurantTablesList({
        restaurantId: userData?.id,
        status: STATUS.ACTIVE,
        limit: 200,
        page: 1,
      }),

    enabled: !!qrOrderData && userData?.type === USER_TYPES.RESTAURANT,
  });

  const { mutateAsync, isPending } = useMutation({
    mutationFn: (param: DecryptParamType) => decrypt(param),
    onSuccess: (response) => {
      if (open === 'table') {
        const tableId = _.get(response, 'table_id');
        const restaurantId = _.get(response, 'restaurant_id');
        if (restaurantId !== data?.restaurantId) {
          toast.info(t('orders.camera_dialog.wrong_restaurant'));
          handleClose();
          return;
        }

        onSubmit({ status: ORDER_STATUS.ARRIVED, tableId });
      } else {
        const restaurantId = _.get(response, 'restaurant_id');

        if (restaurantId !== userData?.id) {
          toast.info(t('orders.camera_dialog.wrong_order'));
          handleClose();
          return;
        }
        setQrOrderData(response as OrderQrDataType);
      }
    },
    onError: () => {
      toast.error(t('common.requests.error.system'));
    },
  });

  const handleSetTable = () => {
    onSubmit({
      tableId: Number(tableId),
      status: ORDER_STATUS.ARRIVED,
      orderId: qrOrderData?.order_id,
      orderDetailId: qrOrderData?.order_detail_id,
    });
  };

  const onScanSuccess = (result: QrScanner.ScanResult) => {
    scanner?.current?.stop();
    mutateAsync({ encryptedText: String(result.data) });
  };

  const onScanFail = (err: string | Error) => {
    console.log(err);
  };

  useEffect(() => {
    if (videoEl?.current && !scanner.current) {
      scanner.current = new QrScanner(videoEl?.current, onScanSuccess, {
        onDecodeError: onScanFail,
        preferredCamera: 'environment',
        highlightScanRegion: true,
        highlightCodeOutline: true,
      });

      scanner?.current
        ?.start()
        .then(() => setQrOn(true))
        .catch(() => {
          setQrOn(false);
        });
    }

    return () => {
      if (!videoEl?.current) {
        scanner?.current?.stop();
      }
    };
  }, []);

  useEffect(() => {
    if (!qrOn) alert(t('orders.camera_dialog.blocked_sms'));
  }, [qrOn]);

  return (
    <Dialog open={!!open} onOpenChange={handleClose}>
      <DialogContent className="flex flex-col items-center justify-center sm:max-w-[425px]">
        <p className="font-semibold">
          {qrOrderData
            ? t('orders.camera_dialog.title_set_table')
            : t(`orders.camera_dialog.title_${open}`)}
        </p>
        {qrOrderData ? (
          <div className="w-full">
            <Select onValueChange={setTableId} value={tableId}>
              <SelectTrigger>
                <SelectValue
                  placeholder={t('orders.camera_dialog.table_number')}
                />
              </SelectTrigger>
              <SelectContent>
                {tableList?.items?.map((item) => (
                  <SelectItem key={item.id} value={String(item.id)}>
                    <span className="flex items-center gap-2 font-semibold text-slate-900">
                      <Badge className="flex flex-row gap-1 bg-cyan-400">
                        <User />
                        {item.capacity}
                      </Badge>
                      {item.id}
                    </span>
                  </SelectItem>
                ))}
              </SelectContent>
            </Select>
            <div className="mt-4 flex w-full flex-row gap-4">
              <Button
                variant="outline"
                onClick={handleClose}
                className="w-full"
              >
                {t('common.cancel')}
              </Button>
              <CustomButton
                text={t('orders.camera_dialog.set_table')}
                variant="default"
                onClick={handleSetTable}
                loading={tableListIsLoading}
                className={`w-full ${Colors.BLUE} ${Colors.HOVER_BLUE}`}
              />
            </div>
          </div>
        ) : isPending ? (
          <Loader />
        ) : (
          <video ref={videoEl} />
        )}
      </DialogContent>
    </Dialog>
  );
};

export default QrScanDialog;

'use client';

import CustomButton from '@/components/custom-button';
import { Button } from '@/components/ui/button';
import { Colors } from '@/constants/color.constants';
import { STATUS } from '@/constants/common.constants';
import { EditOrderDetailParamType } from '@/services/order.service';
import { useTranslations } from 'next-intl';

type Props = {
  handleClose: () => void;
  onSubmit: (values: EditOrderDetailParamType) => void;
  loading: boolean;
};

const OrderDeleteDialogContent: React.FC<Props> = (props: Props) => {
  const t = useTranslations();
  const { loading, onSubmit, handleClose } = props;

  const handleDelete = () => {
    onSubmit({ status: STATUS.INACTIVE });
  };

  return (
    <div>
      <p className="my-8 flex w-full flex-row justify-center font-medium">
        {t('orders.edit_dialog.delete_confirm_question')}
      </p>
      <div className="flex w-full justify-end">
        <Button
          className="mr-5 mt-4 w-full"
          variant="outline"
          onClick={handleClose}
        >
          {t('orders.edit_dialog.cancel')}
        </Button>
        <CustomButton
          text={t('orders.edit_dialog.delete')}
          className={`mt-4 w-full ${Colors.RED} ${Colors.HOVER_RED}`}
          loading={loading}
          onClick={handleDelete}
        />
      </div>
    </div>
  );
};

export default OrderDeleteDialogContent;

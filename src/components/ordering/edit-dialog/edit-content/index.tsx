'use client';

import CustomButton from '@/components/custom-button';
import EmptyData from '@/components/empty-data';
import { Badge } from '@/components/ui/badge';
import { Button } from '@/components/ui/button';
import { DialogFooter } from '@/components/ui/dialog';
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from '@/components/ui/form';
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from '@/components/ui/select';
import { Colors } from '@/constants/color.constants';
import {
  CONTRACT_STATUS,
  STATUS,
  USER_TYPES,
} from '@/constants/common.constants';
import { QUERY_KEYS } from '@/constants/query-keys';
import { useAuth } from '@/contexts/auth';
import { calculateInTime } from '@/lib/utils';
import { getContractsList } from '@/services/contract.service';
import {
  CreateOrderDetailParamType,
  OrderDetailType,
} from '@/services/order.service';
import { getRestaurantsList } from '@/services/restaurant.service';
import { useQuery } from '@tanstack/react-query';
import dayjs from 'dayjs';
import { useTranslations } from 'next-intl';

import { useOrderFormValidation } from '../../validation';

type Props = {
  handleClose: () => void;
  onSubmit: (values: CreateOrderDetailParamType) => void;
  loading?: boolean;
  data: OrderDetailType;
};

const PAGE_SIZE = 200;

const OrderEditDialogContent: React.FC<Props> = (props: Props) => {
  const { handleClose, onSubmit, loading, data } = props;
  const t = useTranslations();

  const { userData, orderedData } = useAuth();

  const { form } = useOrderFormValidation({
    restaurantId: String(data.restaurantId || ''),
    contractId: '',
  });

  const restaurantIdValue = form.watch('restaurantId');
  const contractIdValue = form.watch('contractId');

  const { data: restaurantData } = useQuery({
    queryKey: [QUERY_KEYS.RESTAURANT_LIST_ORDERING],
    queryFn: () =>
      getRestaurantsList({
        contracted: true,
        limit: PAGE_SIZE,
        page: 1,
      }),
    enabled: userData?.type === USER_TYPES.EMPLOYEE,
  });

  const { data: contractData, isLoading: contractIsLoading } = useQuery({
    queryKey: [
      QUERY_KEYS.CONTRACT_LIST_ACTIVE,
      data.restaurantId,
      restaurantIdValue,
    ],
    queryFn: () =>
      getContractsList({
        restaurantId: Number(restaurantIdValue),
        restaurantContractStatus: CONTRACT_STATUS.ACCEPTED,
        organizationContractStatus: CONTRACT_STATUS.ACCEPTED,
        status: STATUS.ACTIVE,
        menuDays: dayjs().day(),
        limit: PAGE_SIZE,
        page: 1,
      }),
    enabled: !!String(restaurantIdValue),
  });

  const selectedContractData = contractData?.items.find(
    (contract) => String(contract.id) === form.watch('contractId')
  );

  const handleSubmit = () => {
    if (selectedContractData) {
      const param: CreateOrderDetailParamType = {
        restaurantId: selectedContractData?.restaurantId,
        restaurantMenuId: Number(selectedContractData.menuId),
        unitPrice: selectedContractData.menuUnitPrice,
        unitCount: 1,
        totalPrice: Number(selectedContractData.menuUnitPrice),
        orderDate: dayjs().format('YYYY-MM-DD'),
        organizationId: userData?.organization?.id,
        isInsideContractRange: 'IN_CONTRACT',
      };
      onSubmit(param);
    }
  };

  return (
    <Form {...form}>
      <form onSubmit={form.handleSubmit(handleSubmit)} className="space-y-4">
        <FormLabel className="font-bold">
          {t('orders.edit_dialog.date')}
          <span className="ml-2 font-semibold">
            {dayjs().format('YYYY-MM-DD')}
          </span>
        </FormLabel>
        <FormField
          control={form.control}
          name="restaurantId"
          render={({ field }) => (
            <FormItem className="mt-4 w-full">
              <FormLabel className="font-bold">
                {t('orders.create_dialog.restaurant')}
              </FormLabel>
              <Select
                onValueChange={field.onChange}
                value={field.value}
                disabled={true}
              >
                <FormControl className="h-auto overflow-hidden">
                  <SelectTrigger>
                    <SelectValue
                      placeholder={t('orders.create_dialog.restaurant')}
                    />
                  </SelectTrigger>
                </FormControl>

                <SelectContent>
                  {restaurantData?.items.map((item) => (
                    <SelectItem key={item.id} value={String(item.id)}>
                      <div className="flex items-center font-semibold text-slate-900">
                        {item.name}
                      </div>
                    </SelectItem>
                  ))}
                </SelectContent>
              </Select>
              <FormMessage />
            </FormItem>
          )}
        />
        <FormField
          control={form.control}
          name="contractId"
          render={({ field }) => (
            <FormItem className="mt-2 w-full">
              <FormLabel className="font-bold">
                {t('orders.create_dialog.menu')}
              </FormLabel>
              <Select
                onValueChange={field.onChange}
                value={field.value}
                disabled={contractIsLoading}
              >
                <FormControl className="h-auto overflow-hidden">
                  <SelectTrigger>
                    <SelectValue placeholder={t('orders.create_dialog.menu')} />
                  </SelectTrigger>
                </FormControl>

                <SelectContent>
                  {contractData && contractData?.items?.length > 0 ? (
                    contractData?.items.map((item) => {
                      const isOrdered = orderedData?.items?.some(
                        (v) => v.menu.id === item.menuId
                      );
                      const isInTime = calculateInTime(item.orderLimitTime);
                      return (
                        <SelectItem
                          key={item.id}
                          value={String(item.id)}
                          disabled={!isInTime || isOrdered}
                        >
                          <div className="flex flex-row items-center overflow-hidden">
                            <Badge className="mr-1 bg-blue-600">
                              {item.menuUnitPrice.toLocaleString()}
                              {t('orders.create_dialog.tugrug')}
                            </Badge>
                            <div className="flex items-center gap-2 font-semibold text-slate-900">
                              {item.menu.name}
                              {isOrdered ? (
                                <span className="font-normal italic">
                                  ({t('orders.already_ordered')})
                                </span>
                              ) : (
                                !isInTime && (
                                  <span className="font-normal italic">
                                    ({t('orders.out_of_time')})
                                  </span>
                                )
                              )}
                            </div>
                          </div>
                        </SelectItem>
                      );
                    })
                  ) : (
                    <EmptyData size="small" />
                  )}
                </SelectContent>
              </Select>
              <FormMessage />
            </FormItem>
          )}
        />
        <DialogFooter>
          <Button
            variant="outline"
            onClick={handleClose}
            className="mt-4 w-full"
          >
            {t('orders.edit_dialog.cancel')}
          </Button>
          <CustomButton
            text={t('orders.edit_dialog.edit')}
            variant="default"
            type="submit"
            loading={loading}
            disabled={
              String(data.restaurantId) === restaurantIdValue &&
              String(data.restaurantOrderId) === contractIdValue
            }
            className={`mt-4 w-full ${Colors.BLUE} ${Colors.HOVER_BLUE}`}
          />
        </DialogFooter>
      </form>
    </Form>
  );
};

export default OrderEditDialogContent;

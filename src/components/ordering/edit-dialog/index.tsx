'use client';

import { Dialog, DialogContent } from '@/components/ui/dialog';
import { Tabs, TabsContent, TabsList, TabsTrigger } from '@/components/ui/tabs';
import {
  EditOrderDetailParamType,
  OrderDetailType,
} from '@/services/order.service';
import { useTranslations } from 'next-intl';

import OrderDeleteDialogContent from './delete-content';
import OrderEditDialogContent from './edit-content';

type Props = {
  open: boolean;
  handleClose: () => void;
  onEdit: (values: EditOrderDetailParamType) => void;
  onDelete: () => void;
  loading: boolean;
  data: OrderDetailType;
};

const OrderEditDialog: React.FC<Props> = (props: Props) => {
  const t = useTranslations();
  const { open, handleClose, onEdit, onDelete, loading, data } = props;

  return (
    <Dialog open={open} onOpenChange={handleClose}>
      <DialogContent className="sm:max-w-[425px]">
        <Tabs defaultValue="edit">
          <TabsList>
            <TabsTrigger value="edit">
              {t('orders.edit_dialog.edit_order_title')}
            </TabsTrigger>
            <TabsTrigger value="delete">
              {t('orders.edit_dialog.delete_order_title')}
            </TabsTrigger>
          </TabsList>
          <div className="mt-4">
            <TabsContent value="edit">
              <OrderEditDialogContent
                handleClose={handleClose}
                data={data}
                onSubmit={onEdit}
                loading={loading}
              />
            </TabsContent>
            <TabsContent value="delete">
              <OrderDeleteDialogContent
                handleClose={handleClose}
                onSubmit={onDelete}
                loading={loading}
              />
            </TabsContent>
          </div>
        </Tabs>
      </DialogContent>
    </Dialog>
  );
};

export default OrderEditDialog;

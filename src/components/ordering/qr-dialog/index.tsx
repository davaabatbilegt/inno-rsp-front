'use client';

import { Button } from '@/components/ui/button';
import { Dialog, DialogContent } from '@/components/ui/dialog';
import dayjs from 'dayjs';
import { useTranslations } from 'next-intl';
import Image from 'next/image';
import QRCode from 'qrcode';
import { useEffect, useState } from 'react';

type Props = {
  open: boolean;
  handleClose: () => void;
  data: string;
};

const QrDialog: React.FC<Props> = (props: Props) => {
  const t = useTranslations();
  const { open, handleClose, data } = props;

  const [qrData, setQrData] = useState<string | null>(null);
  const todayDate = dayjs().format('YYYY-MM-DD');

  const generateQR = async (value: string) => {
    try {
      const response = await QRCode.toDataURL(value);
      setQrData(response);
    } catch (err) {
      console.error(err);
    }
  };

  useEffect(() => {
    generateQR(data);
  }, []);

  const handleDownload = () => {
    if (qrData) {
      const downloadLink = document.createElement('a');
      downloadLink.href = qrData;
      downloadLink.download = `order-qr-code-${todayDate}.png`;
      downloadLink.click();
    }
  };

  return (
    <Dialog open={open} onOpenChange={handleClose}>
      <DialogContent className="sm:max-w-[425px]">
        {qrData && (
          <div className="flex flex-col justify-center">
            <Image alt="qr-code" src={qrData} width={400} height={400} />
            <Button onClick={handleDownload} className="bg-blue-600">
              {t('orders.qr_dialog.download')}
            </Button>
          </div>
        )}
      </DialogContent>
    </Dialog>
  );
};

export default QrDialog;

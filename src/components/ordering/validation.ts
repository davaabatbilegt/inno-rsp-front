'use client';

import { zodResolver } from '@hookform/resolvers/zod';
import { useTranslations } from 'next-intl';
import { useForm } from 'react-hook-form';
import { z } from 'zod';

export type OrderFormValuesType = {
  restaurantId: string;
  contractId: string;
};

const initialValues: OrderFormValuesType = {
  restaurantId: '',
  contractId: '',
};

export const useOrderFormValidation = (initialData?: OrderFormValuesType) => {
  const t = useTranslations();
  const schema = z.object({
    restaurantId: z
      .string()
      .min(1, { message: t('orders.validation.restaurant_required') }),
    contractId: z
      .string()
      .min(1, { message: t('orders.validation.contract_menu_required') }),
  });

  const form = useForm<z.infer<typeof schema>>({
    resolver: zodResolver(schema),
    defaultValues: initialData || initialValues,
  });

  return { schema, form };
};

'use client';

import IMAGES from '@/constants/images.constants';
import { PRIVATE_ROUTES } from '@/constants/routes.constants';
import { useRouter } from '@/lib/navigation';
import { getImage } from '@/lib/utils';
import { OrganizationType } from '@/services/organization.service';
import { Plus, User } from 'lucide-react';
import { useTranslations } from 'next-intl';
import Image from 'next/image';

import CustomTooltip from '../custom-tooltip';
import { Badge } from '../ui/badge';
import { Button } from '../ui/button';

type Props = {
  data: OrganizationType;
  handleOpenCreateContract: () => void;
  handleSelectOrganizationId: (v: number) => void;
};

const OrganizationCard: React.FC<Props> = (props: Props) => {
  const { data, handleOpenCreateContract, handleSelectOrganizationId } = props;
  const t = useTranslations();
  const router = useRouter();

  const handleCreateContract = (
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    e.stopPropagation();
    handleSelectOrganizationId(data.id);
    handleOpenCreateContract();
  };

  return (
    <div
      className="h-full max-w-sm flex-col overflow-hidden rounded shadow-lg transition delay-200 ease-out hover:scale-110 hover:cursor-pointer hover:bg-slate-50"
      onClick={() =>
        router.push(`${PRIVATE_ROUTES.RESTAURANT_ORGANIZATIONS}/${data.id}`)
      }
    >
      <div className="relative flex h-[250px]">
        <Image
          alt="organization-image"
          src={getImage(data.username)}
          fill
          className="object-cover"
        />
        {Number(data.contracted) > 0 && (
          <div className="absolute right-2 top-2">
            <CustomTooltip
              content={
                <div className="flex flex-row gap-2">
                  <Badge className="bg-green-700">{data.contracted}</Badge>
                  {t('organizations.contracted_tooltip')}
                </div>
              }
              side="bottom"
              align="end"
            >
              <Image
                alt="restaurant-image"
                src={IMAGES.CONTRACTED}
                width={32}
                height={32}
                className="object-cover"
              />
            </CustomTooltip>
          </div>
        )}
        <div className="absolute left-2 top-2">
          <CustomTooltip
            content={t('contracts.create_dialog.title')}
            side="bottom"
            align="start"
          >
            <Button
              onClick={(e) => handleCreateContract(e)}
              className="h-[40px] w-[40px] rounded-full border bg-green-700  p-0 hover:bg-green-500"
            >
              <Plus color="#fff" className="h-4 w-4 " />
            </Button>
          </CustomTooltip>
        </div>
      </div>
      <div className="px-5 pb-5 pt-4">
        <div className="mb-3">
          <div className="mb-2 text-lg font-bold">{data.name}</div>
          <p className="text-sm text-gray-700">{data.description}</p>
        </div>
        <div className="flex items-center gap-4">
          <div className="flex items-center gap-1">
            <User size={16} color="#8b8a84" strokeWidth={0.5} />
            <div className="text-xs text-gray-500">
              {data.employeeCount || 0}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default OrganizationCard;

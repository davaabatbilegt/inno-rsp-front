'use client';

import { Star, StarHalf } from 'lucide-react';

type Props = {
  rating: number;
  restaurantId: string;
};

const Rating: React.FC<Props> = (props: Props) => {
  const { rating, restaurantId } = props;

  const ratingToArray = (rating: number) => {
    const ratingArray = [0, 0, 0, 0, 0];
    if (rating >= 0 && rating <= 5) {
      const integerPart = Math.floor(rating);
      const decimalPart = rating - integerPart;
      for (let i = 0; i < integerPart; i++) {
        ratingArray[i] = 1;
      }
      if (decimalPart > 0) {
        ratingArray[integerPart] = decimalPart;
      }
    } else if (rating > 5) {
      ratingArray.fill(1);
    }
    return ratingArray;
  };

  return (
    <div className="flex w-max">
      {ratingToArray(rating).map((item, index) => {
        if (item === 1)
          return (
            <Star
              key={`${restaurantId}-${index}-full`}
              size={18}
              fill="#ffe234"
              color="#ffe234"
              strokeWidth={1}
            />
          );
        else if (item > 0)
          return (
            <StarHalf
              key={`${restaurantId}-${index}-half`}
              size={18}
              fill="#ffe234"
              color="#ffe234"
              strokeWidth={1}
            />
          );
        return null;
      })}
    </div>
  );
};

export default Rating;

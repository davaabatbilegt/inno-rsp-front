'use client';

import { USER_TYPES } from '@/constants/common.constants';
import IMAGES from '@/constants/images.constants';
import { useAuth } from '@/contexts/auth';
import { usePathname, useRouter } from '@/lib/navigation';
import { calculateDistance, getImage } from '@/lib/utils';
import { RestaurantType } from '@/services/restaurant.service';
import { MapPin, Plus, User } from 'lucide-react';
import { useTranslations } from 'next-intl';
import Image from 'next/image';
import { useEffect, useState } from 'react';

import CustomTooltip from '../custom-tooltip';
import Rating from '../rating';
import { Badge } from '../ui/badge';
import { Button } from '../ui/button';

type Props = {
  data: RestaurantType;
  handleOpenCreateContract: () => void;
  handleSelectRestaurantId: (v: number) => void;
};

const RestaurantCard: React.FC<Props> = (props: Props) => {
  const { data, handleOpenCreateContract, handleSelectRestaurantId } = props;
  const { userData } = useAuth();
  const t = useTranslations();
  const router = useRouter();
  const pathname = usePathname();

  const [distance, setDistance] = useState<string | null>(null);

  const handleCreateContract = (
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    e.stopPropagation();
    handleSelectRestaurantId(data.id);
    handleOpenCreateContract();
  };

  useEffect(() => {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        if (data.latitude && data.longitude) {
          const distanceInKm = calculateDistance(
            data.latitude,
            data.longitude,
            position.coords.latitude,
            position.coords.longitude
          );
          if (distanceInKm < 1) {
            setDistance(
              t('restaurants.m', { distance: (distanceInKm * 1000).toFixed(0) })
            );
          } else {
            setDistance(
              t('restaurants.km', { distance: distanceInKm.toFixed(1) })
            );
          }
        } else {
          setDistance(t('restaurants.km', { distance: 0 }));
        }
      });
    }
  }, []);

  return (
    <div
      className="h-full max-w-sm flex-col overflow-hidden rounded shadow-lg transition delay-200 ease-out hover:scale-110 hover:cursor-pointer hover:bg-slate-50"
      onClick={() => router.push(`${pathname}/${data.id}`)}
    >
      <div className="relative flex h-[250px]">
        <Image
          alt="restaurant-image"
          src={getImage(data.username)}
          fill
          className="object-cover"
        />
        {Number(data.contracted) > 0 && (
          <div className="absolute right-2 top-2">
            <CustomTooltip
              content={
                <div className="flex flex-row gap-2">
                  <Badge className="bg-green-700">{data.contracted}</Badge>
                  {t('restaurants.contracted_tooltip')}
                </div>
              }
              side="bottom"
              align="end"
            >
              <Image
                alt="contracted-image"
                src={IMAGES.CONTRACTED}
                width={32}
                height={32}
                className="object-cover"
              />
            </CustomTooltip>
          </div>
        )}
        {userData?.type === USER_TYPES.ORGANIZATION && (
          <div className="absolute left-2 top-2">
            <CustomTooltip
              content={t('contracts.create_dialog.title')}
              side="bottom"
              align="start"
            >
              <Button
                onClick={(e) => handleCreateContract(e)}
                className="h-[40px] w-[40px] rounded-full border bg-green-700  p-0 hover:bg-green-500"
              >
                <Plus color="#fff" className="h-4 w-4 " />
              </Button>
            </CustomTooltip>
          </div>
        )}
      </div>
      <div className="px-5 pb-5 pt-4">
        <div className="mb-2 flex items-center gap-2 text-sm text-gray-500">
          {typeof data?.rating === 'number' ? (
            <span className="flex flex-row items-center gap-2">
              {data?.rating.toFixed(1)}/5
              <Rating rating={data?.rating} restaurantId={String(data.id)} />
            </span>
          ) : (
            t('restaurants.unrated')
          )}
        </div>
        <div className="mb-3">
          <div className="mb-2 text-lg font-bold">{data.name}</div>
          <p className="text-sm text-gray-700">{data.description}</p>
        </div>
        <div className="flex items-center gap-4">
          <div className="flex items-center gap-1">
            <User size={16} color="#8b8a84" strokeWidth={0.5} />
            <div className="text-xs text-gray-500">
              {data?.capacity ? data?.capacity : 0}
            </div>
          </div>
          {distance ? (
            <div className="flex items-center gap-1">
              <MapPin size={16} color="#8b8a84" strokeWidth={0.5} />
              <div className="text-xs text-gray-500">{distance}</div>
            </div>
          ) : null}
        </div>
      </div>
    </div>
  );
};

export default RestaurantCard;

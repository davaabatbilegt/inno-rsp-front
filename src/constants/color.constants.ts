export const Colors = {
  RED: 'bg-red-500',
  GREEN: 'bg-green-500',
  BLUE: 'bg-blue-500',
  WHITE: 'bg-white-500',
  HOVER_RED: 'hover:bg-red-400',
  HOVER_GREEN: 'hover:bg-green-400',
  HOVER_BLUE: 'hover:bg-blue-400',
  HOVER_WHITE: 'hover:bg-white-400',
  CREATE_GREEN: 'bg-green-700',
};

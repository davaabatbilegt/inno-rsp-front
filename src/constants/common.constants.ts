export const EMPTY_VALUE = '-';

export enum USER_TYPES {
  ADMIN = 'admin',
  RESTAURANT = 'restaurant',
  ORGANIZATION = 'organization',
  EMPLOYEE = 'employee',
  USER = 'user',
}

export type UserTypesType = (typeof USER_TYPES)[keyof typeof USER_TYPES];

export enum LOCALES {
  EN = 'en',
  MN = 'mn',
}

export const USER_TYPES_LIST = Object.values(USER_TYPES);

export const LOCALES_LIST = Object.values(LOCALES);

export const COOKIE_KEYS = {
  ACCESS_TOKEN: 'access_token',
  TYPE: 'type',
};

export enum CONTRACT_STATUS {
  PENDING = 'PENDING',
  ACCEPTED = 'ACCEPTED',
}

export enum STATUS {
  ACTIVE = 'ACTIVE',
  INACTIVE = 'INACTIVE',
}

export enum WEEKDAYS {
  ALL = 'All',
  MONDAY = 'Monday',
  TUESDAY = 'Tuesday',
  WEDNESDAY = 'Wednesday',
  THURSDAY = 'Thursday',
  FRIDAY = 'Friday',
  SATURDAY = 'Saturday',
  SUNDAY = 'Sunday',
}

export type StatusItem = {
  value: STATUS;
  label: string;
};

export const STATUS_LIST: StatusItem[] = [
  {
    value: STATUS.ACTIVE,
    label: 'active',
  },
  {
    value: STATUS.INACTIVE,
    label: 'inactive',
  },
];

export enum CONTRACTED_STATUS {
  YES = '1',
  NO = '0',
}

export enum ORDER_STATUS {
  ORDERED = 'ORDERED',
  ARRIVED = 'ARRIVED',
  DELIVERED = 'DELIVERED',
  BILLED = 'BILLED',
}

export enum BILL_STATUS {
  CALCULATED = 'CALCULATED',
  NOT_PAID = 'NOT_PAID',
  BILLED = 'BILLED',
  CONFIRMED = 'CONFIRMED',
}

export type BillStatusItem = {
  value: BILL_STATUS;
  label: string;
};

export const BILL_STATUS_LIST: BillStatusItem[] = [
  {
    value: BILL_STATUS.CALCULATED,
    label: 'calculated',
  },
  {
    value: BILL_STATUS.BILLED,
    label: 'billed',
  },
  {
    value: BILL_STATUS.NOT_PAID,
    label: 'not_paid',
  },
  {
    value: BILL_STATUS.CONFIRMED,
    label: 'confirmed',
  },
];

export type RestaurantItem = {
  id: number;
  name: string;
};

export type OrganizationSelectItem = {
  id: number;
  name: string;
};

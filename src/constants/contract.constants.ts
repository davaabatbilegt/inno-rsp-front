import { CONTRACT_STATUS, USER_TYPES } from './common.constants';
import { QUERY_KEYS } from './query-keys';

export const CONFIG_CONSTANT = {
  active: {
    [USER_TYPES.ORGANIZATION]: {
      key: QUERY_KEYS.CONTRACT_LIST_ACTIVE,
      restaurantContractStatus: CONTRACT_STATUS.ACCEPTED,
      organizationContractStatus: CONTRACT_STATUS.ACCEPTED,
    },
    [USER_TYPES.RESTAURANT]: {
      key: QUERY_KEYS.CONTRACT_LIST_ACTIVE,
      restaurantContractStatus: CONTRACT_STATUS.ACCEPTED,
      organizationContractStatus: CONTRACT_STATUS.ACCEPTED,
    },
  },
  requesting: {
    [USER_TYPES.ORGANIZATION]: {
      key: QUERY_KEYS.CONTRACT_LIST_REQUESTING,
      restaurantContractStatus: CONTRACT_STATUS.PENDING,
      organizationContractStatus: CONTRACT_STATUS.ACCEPTED,
    },
    [USER_TYPES.RESTAURANT]: {
      key: QUERY_KEYS.CONTRACT_LIST_REQUESTING,
      restaurantContractStatus: CONTRACT_STATUS.ACCEPTED,
      organizationContractStatus: CONTRACT_STATUS.PENDING,
    },
  },
  receiving: {
    [USER_TYPES.ORGANIZATION]: {
      key: QUERY_KEYS.CONTRACT_LIST_RECEIVING,
      restaurantContractStatus: CONTRACT_STATUS.ACCEPTED,
      organizationContractStatus: CONTRACT_STATUS.PENDING,
    },
    [USER_TYPES.RESTAURANT]: {
      key: QUERY_KEYS.CONTRACT_LIST_RECEIVING,
      restaurantContractStatus: CONTRACT_STATUS.PENDING,
      organizationContractStatus: CONTRACT_STATUS.ACCEPTED,
    },
  },
};

import CONTRACTED from '/public/images/contracted.png';
import COSY from '/public/images/cosy_logo.jpg';
import MN_ICON from '/public/images/flag-mn.svg';
import EN_ICON from '/public/images/flag-us.svg';
import IKH_MONGOL from '/public/images/ikh_mongol_logo.jpg';
import LOGIN_IMAGE from '/public/images/login.png';
import APP_LOGO from '/public/images/logo.png';
import UNIMEDIA from '/public/images/unimedia_logo.png';

const IMAGES = {
  EN_ICON,
  MN_ICON,
  COSY,
  IKH_MONGOL,
  UNIMEDIA,
  CONTRACTED,
  LOGIN_IMAGE,
  APP_LOGO,
};

export default IMAGES;

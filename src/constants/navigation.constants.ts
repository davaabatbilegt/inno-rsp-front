import {
  Banknote,
  Building,
  ChefHat,
  Handshake,
  LucideIcon,
  MenuSquare,
  NotepadText,
  Soup,
  UserRound,
} from 'lucide-react';

import { UserTypesType } from './common.constants';
import { GENERAL_ROUTES, ROUTES } from './routes.constants';

type ListType = {
  title: string;
  route: string;
  icon: LucideIcon;
};

type NavListType = {
  [key: string]: ListType[];
};

type NavGuardType = {
  [key in UserTypesType]: string[];
};

export const NAV_GUARD: NavGuardType = {
  restaurant: [
    GENERAL_ROUTES.ORDERS,
    GENERAL_ROUTES.MENUS,
    GENERAL_ROUTES.CONTRACTS,
    GENERAL_ROUTES.ORGANIZATIONS,
    GENERAL_ROUTES.REPORTS,
    GENERAL_ROUTES.INVOICES,
    GENERAL_ROUTES.REPORTS,
  ],
  organization: [
    GENERAL_ROUTES.ORDERS,
    GENERAL_ROUTES.EMPLOYEES,
    GENERAL_ROUTES.CONTRACTS,
    GENERAL_ROUTES.RESTAURANTS,
    GENERAL_ROUTES.REPORTS,
    GENERAL_ROUTES.INVOICES,
    GENERAL_ROUTES.REPORTS,
    GENERAL_ROUTES.EMPLOYEE_REPORTS,
  ],
  employee: [
    GENERAL_ROUTES.ORDERS,
    GENERAL_ROUTES.MENUS,
    GENERAL_ROUTES.RESTAURANTS,
  ],
  user: [
    GENERAL_ROUTES.MENUS,
    GENERAL_ROUTES.RESTAURANTS,
    GENERAL_ROUTES.ORDERS,
  ],
  admin: [
    GENERAL_ROUTES.ORDERS,
    GENERAL_ROUTES.MENUS,
    GENERAL_ROUTES.RESTAURANTS,
    GENERAL_ROUTES.ORGANIZATIONS,
    GENERAL_ROUTES.EMPLOYEES,
  ],
};

const RESTAURANT: ListType[] = [
  {
    title: 'ordering',
    route: ROUTES.RESTAURANT_ORDERS,
    icon: Soup,
  },
  {
    title: 'menu',
    route: ROUTES.RESTAURANT_MENUS,
    icon: MenuSquare,
  },
  {
    title: 'contract',
    route: ROUTES.RESTAURANT_CONTRACTS,
    icon: Handshake,
  },
  {
    title: 'organization',
    route: ROUTES.RESTAURANT_ORGANIZATIONS,
    icon: Building,
  },
  {
    title: 'invoice',
    route: ROUTES.RESTAURANT_INVOICES,
    icon: Banknote,
  },
  {
    title: 'report',
    route: ROUTES.RESTAURANT_REPORTS,
    icon: NotepadText,
  },
];

const ORGANIZATION: ListType[] = [
  {
    title: 'ordering',
    route: ROUTES.ORGANIZATION_ORDERS,
    icon: Soup,
  },
  {
    title: 'employee',
    route: ROUTES.ORGANIZATION_EMPLOYEES,
    icon: UserRound,
  },
  {
    title: 'contract',
    route: ROUTES.ORGANIZATION_CONTRACTS,
    icon: Handshake,
  },
  {
    title: 'restaurant',
    route: ROUTES.ORGANIZATION_RESTAURANTS,
    icon: ChefHat,
  },
  {
    title: 'invoice',
    route: ROUTES.ORGANIZATION_INVOICES,
    icon: Banknote,
  },
  {
    title: 'report',
    route: ROUTES.ORGANIZATION_REPORTS,
    icon: NotepadText,
  },
  {
    title: 'employee_report',
    route: ROUTES.ORGANIZATION_EMPLOYEE_REPORTS,
    icon: NotepadText,
  },
];

const EMPLOYEE: ListType[] = [
  {
    title: 'ordering',
    route: ROUTES.EMPLOYEE_ORDERS,
    icon: Soup,
  },
  {
    title: 'menu',
    route: ROUTES.EMPLOYEE_MENUS,
    icon: MenuSquare,
  },
  {
    title: 'restaurant',
    route: ROUTES.EMPLOYEE_RESTAURANTS,
    icon: ChefHat,
  },
];

const USER: ListType[] = [
  {
    title: 'menu',
    route: ROUTES.USER_MENUS,
    icon: MenuSquare,
  },
  {
    title: 'restaurant',
    route: ROUTES.USER_RESTAURANTS,
    icon: ChefHat,
  },
  {
    title: 'ordering',
    route: ROUTES.USER_ORDERS,
    icon: Soup,
  },
];

export const NAV_LIST: NavListType = {
  restaurant: RESTAURANT,
  organization: ORGANIZATION,
  employee: EMPLOYEE,
  user: USER,
};

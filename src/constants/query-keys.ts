export const QUERY_KEYS = {
  AUTH: 'auth',
  USER_DATA: 'user_data',
  RESTAURANT_LIST: 'restaurant_list',
  ORGANIZATION_EMPLOYEE_LIST: 'organization_employee_list',
  ORGANIZATION_LIST: 'organization_list',
  MENU_LIST: 'menu_list',
  CONTRACT_LIST_ACTIVE: 'contract_list_active',
  CONTRACT_LIST_REQUESTING: 'contract_list_requesting',
  CONTRACT_LIST_RECEIVING: 'contract_list_receiving',
  ORDERS_LIST: 'order_list',
  EMPLOYEE_ORDERED_DETAILS_LIST: 'employee_ordered_details_list',
  CONTRACTED_LIST: 'contracted_list',
  INVOICES_LIST: 'invoice_list',
  RESTAURANT_LIST_ORDERING: 'restaurant_list_ordering',
  RESTAURANT_DETAIL: 'restaurant_detail',
  REPORT_LIST: 'report_list',
  RESTAURANT_TABLE_LIST: 'restaurant_table_list',
} as const;

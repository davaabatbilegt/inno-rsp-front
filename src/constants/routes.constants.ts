const ADMIN_ROUTES = {
  PUBLIC: {
    ADMIN_LOGIN: '/admin/login',
  },
  PRIVATE: {
    ADMIN_RESTAURANTS: '/admin/restaurants',
    ADMIN_ORGANIZATIONS: '/admin/organizations',
    ADMIN_EMPLOYEES: '/admin/employees',
    ADMIN_USERS: '/admin/users',
    ADMIN_ORDERS: '/admin/orders',
  },
};

const RESTAURANT_ROUTES = {
  PUBLIC: {
    RESTAURANT_LOGIN: '/restaurant/login',
  },
  PRIVATE: {
    RESTAURANT_MENUS: '/restaurant/menus',
    RESTAURANT_CONTRACTS: '/restaurant/contracts',
    RESTAURANT_ORGANIZATIONS: '/restaurant/organizations',
    RESTAURANT_ORGANIZATION_DETAIL: '/restaurant/organizations/:id',
    RESTAURANT_MENU_DETAIL: '/restaurant/menus/:id',
    RESTAURANT_ORDERS: '/restaurant/orders',
    RESTAURANT_INVOICES: '/restaurant/invoices',
    RESTAURANT_REPORTS: '/restaurant/reports',
  },
};

const ORGANIZATION_ROUTES = {
  PUBLIC: {
    ORGANIZATION_LOGIN: '/organization/login',
  },
  PRIVATE: {
    ORGANIZATION_EMPLOYEES: '/organization/employees',
    ORGANIZATION_CONTRACTS: '/organization/contracts',
    ORGANIZATION_MENUS: '/organization/menus',
    ORGANIZATION_RESTAURANTS: '/organization/restaurants',
    ORGANIZATION_RESTAURANT_DETAIL: '/organization/restaurants/:id',
    ORGANIZATION_RESTAURANT_MENU: '/organization/restaurants/:id/menus',
    ORGANIZATION_ORDERS: '/organization/orders',
    ORGANIZATION_INVOICES: '/organization/invoices',
    ORGANIZATION_REPORTS: '/organization/reports',
    ORGANIZATION_EMPLOYEE_REPORTS: '/organization/employee-reports',
  },
};

const EMPLOYEE_ROUTES = {
  PUBLIC: {
    EMPLOYEE_LOGIN: '/employee/login',
  },
  PRIVATE: {
    EMPLOYEE_MENUS: '/employee/menus',
    EMPLOYEE_RESTAURANTS: '/employee/restaurants',
    EMPLOYEE_RESTAURANT_DETAIL: '/employee/restaurants/:id',
    EMPLOYEE_RESTAURANT_MENU: '/employee/restaurants/:id/menus',
    EMPLOYEE_ORDERS: '/employee/orders',
  },
};

const USER_ROUTES = {
  PUBLIC: {
    USER_LOGIN: '/user/login',
  },
  PRIVATE: {
    USER_RESTAURANTS: '/user/restaurants',
    USER_RESTAURANT_DETAIL: '/user/restaurants/:id',
    USER_RESTAURANT_MENU: '/user/restaurants/:id/menus',
    USER_MENUS: '/user/menus',
    USER_ORDERS: '/user/orders',
  },
};

export const GENERAL_ROUTES = {
  RESTAURANTS: '/restaurants',
  MENUS: '/menus',
  ORGANIZATIONS: '/organizations',
  EMPLOYEES: '/employees',
  CONTRACTS: '/contracts',
  ORDERS: '/orders',
  REPORTS: '/reports',
  INVOICES: '/invoices',
  EMPLOYEE_REPORTS: '/employee-reports',
};

export const PUBLIC_ROUTES = {
  ...ADMIN_ROUTES.PUBLIC,
  ...RESTAURANT_ROUTES.PUBLIC,
  ...ORGANIZATION_ROUTES.PUBLIC,
  ...EMPLOYEE_ROUTES.PUBLIC,
  ...USER_ROUTES.PUBLIC,
};

export const PRIVATE_ROUTES = {
  ...ADMIN_ROUTES.PRIVATE,
  ...RESTAURANT_ROUTES.PRIVATE,
  ...ORGANIZATION_ROUTES.PRIVATE,
  ...EMPLOYEE_ROUTES.PRIVATE,
  ...USER_ROUTES.PRIVATE,
};

export const ROUTES = {
  ...PUBLIC_ROUTES,
  ...PRIVATE_ROUTES,
};

export type MetaType = {
  totalItems: number;
  itemCount: number;
  itemsPerPage: number;
  totalPages: number;
  currentPage: number;
};

export type LinksType = {
  first: string;
  previous: string;
  next: string;
  last: string;
};

export type SortOrderType = 'ASC' | 'DESC';

export type GetListParam = {
  page?: number;
  limit?: number;
  sortOrder?: SortOrderType;
  status?: StatusType;
};

export type ContractStatusType = 'PENDING' | 'ACCEPTED';

export type ContractListType = 'active' | 'requesting' | 'receiving';

export type StatusType = 'ACTIVE' | 'INACTIVE';

export type IsContractedType = '0' | '1';

export type OrderStatusType = 'ORDERED' | 'ARRIVED' | 'DELIVERED' | 'BILLED';

export type IsInsideContractRangeType = 'NON_CONTRACT' | 'IN_CONTRACT';

export type BillStatusType = 'CALCULATED' | 'NOT_PAID' | 'BILLED' | 'CONFIRMED';

'use client';

import CardList from '@/components/card-list';
import ContractCard from '@/components/contract-card';
import OrderCreateDialog from '@/components/ordering/create-dialog';
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from '@/components/ui/select';
import {
  CONTRACT_STATUS,
  RestaurantItem,
  STATUS,
} from '@/constants/common.constants';
import { QUERY_KEYS } from '@/constants/query-keys';
import { getContractsList } from '@/services/contract.service';
import {
  CreateOrderDetailParamType,
  createOrderDetail,
} from '@/services/order.service';
import {
  RestaurantType,
  getRestaurantsList,
} from '@/services/restaurant.service';
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import { useTranslations } from 'next-intl';
import { useSearchParams } from 'next/navigation';
import { useEffect, useState } from 'react';
import { toast } from 'sonner';

const PAGE_SIZE = 10;

type Props = {
  restaurant_id?: string;
};

const ContractsMenuListContainer: React.FC<Props> = (props: Props) => {
  const t = useTranslations();
  const searchParams = useSearchParams();
  const queryClient = useQueryClient();

  const [restaurantList, setRestaurantList] = useState<RestaurantItem[]>([
    { id: 0, name: t('menus.all') },
  ]);
  const [selectedRestaurant, setSelectedRestaurant] = useState<string>(
    props.restaurant_id || '0'
  );

  const [selectedContractId, setSelectedContractId] = useState<string | null>(
    null
  );
  const [openCreateOrder, setOpenCreateOrder] = useState<boolean>(false);

  const currentPage = searchParams.has('page')
    ? Number(searchParams.get('page'))
    : 1;

  useEffect(() => {
    const fetchRestaurants = async () => {
      try {
        const response = await getRestaurantsList({ status: STATUS.ACTIVE });
        response.items.forEach((item: RestaurantType) => {
          setRestaurantList((prevList) => {
            if (!prevList.find((restaurant) => restaurant.id === item.id)) {
              return [...prevList, { name: item.name, id: item.id }];
            }
            return prevList;
          });
        });
      } catch (error) {
        toast.error(t('common.requests.error.system'));
      }
    };
    fetchRestaurants();
  }, []);

  const { data, isLoading } = useQuery({
    queryKey: [
      QUERY_KEYS.CONTRACT_LIST_ACTIVE,
      currentPage,
      selectedRestaurant,
    ],
    queryFn: () =>
      getContractsList({
        limit: PAGE_SIZE,
        page: currentPage,
        restaurantContractStatus: CONTRACT_STATUS.ACCEPTED,
        organizationContractStatus: CONTRACT_STATUS.ACCEPTED,
        status: STATUS.ACTIVE,
        restaurantId:
          selectedRestaurant !== '0' ? parseInt(selectedRestaurant) : undefined,
      }),
  });

  const { mutateAsync: createOrderMutate, isPending: createOrderIsPending } =
    useMutation({
      mutationFn: (param: CreateOrderDetailParamType) =>
        createOrderDetail(param),
      onSuccess: () => {
        setOpenCreateOrder(false);
        queryClient.invalidateQueries({ queryKey: [QUERY_KEYS.ORDERS_LIST] });
        toast.success(t('common.requests.success.create_order'));
      },
      onError: () => {
        toast.error(t('common.requests.error.system'));
      },
    });

  const selectedContractData = data?.items?.find(
    (item) => item.id === Number(selectedContractId)
  );

  const handleCreateOrder = (v: CreateOrderDetailParamType) => {
    createOrderMutate(v);
  };

  return (
    <div className="flex-col">
      {!props.restaurant_id && (
        <Select
          onValueChange={(value) => setSelectedRestaurant(value)}
          value={selectedRestaurant}
        >
          <SelectTrigger className="ml-4 w-64">
            <SelectValue placeholder={t('menus.select_menu')} />
          </SelectTrigger>
          <SelectContent>
            {restaurantList.map((restaurant: RestaurantItem) => (
              <SelectItem key={restaurant.id} value={restaurant.id.toString()}>
                {restaurant.name}
              </SelectItem>
            ))}
          </SelectContent>
        </Select>
      )}
      <CardList meta={data?.meta} loading={isLoading} pageSize={PAGE_SIZE}>
        <div className="mt-5 grid grid-cols-1 gap-4 p-4 sm:grid-cols-2 sm:gap-6 md:grid-cols-3 md:gap-8 lg:grid-cols-5 lg:gap-8">
          {data?.items?.map((item) => (
            <ContractCard
              key={item.id}
              data={item}
              loading={createOrderIsPending}
              handleChangeOpenDialog={() => setOpenCreateOrder(true)}
              handleChangeSelectedContractId={() =>
                setSelectedContractId(String(item.id))
              }
            />
          ))}
        </div>
      </CardList>
      {selectedContractData && openCreateOrder && (
        <OrderCreateDialog
          open={openCreateOrder}
          handleClose={() => setOpenCreateOrder(false)}
          onSubmit={handleCreateOrder}
          loading={createOrderIsPending}
          restaurantId={String(selectedContractData.restaurant.id)}
          contractId={String(selectedContractData.id)}
        />
      )}
    </div>
  );
};

export default ContractsMenuListContainer;

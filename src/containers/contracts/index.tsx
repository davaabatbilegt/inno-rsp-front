import { Tabs, TabsContent, TabsList, TabsTrigger } from '@/components/ui/tabs';
import { useTranslations } from 'next-intl';

import ContractsListContainer from './list';

const ContractsContainer: React.FC = () => {
  const t = useTranslations();

  return (
    <Tabs defaultValue="active">
      <TabsList>
        <TabsTrigger value="active">{t('contracts.active')}</TabsTrigger>
        <TabsTrigger value="requesting">
          {t('contracts.requesting')}
        </TabsTrigger>
        <TabsTrigger value="receiving">{t('contracts.receiving')}</TabsTrigger>
      </TabsList>
      <TabsContent value="active">
        <ContractsListContainer type="active" />
      </TabsContent>
      <TabsContent value="requesting">
        <ContractsListContainer type="requesting" />
      </TabsContent>
      <TabsContent value="receiving">
        <ContractsListContainer type="receiving" />
      </TabsContent>
    </Tabs>
  );
};

export default ContractsContainer;

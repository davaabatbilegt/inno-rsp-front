'use client';

import CardList from '@/components/card-list';
import ContractCard from '@/components/contract-card';
import ContractAcceptDialog from '@/components/contract-dialogs/accept-dialog';
import ContractCancelDialog from '@/components/contract-dialogs/cancel-dialog';
import ContractEditDialog from '@/components/contract-dialogs/create-dialog';
import { ContractValuesType } from '@/components/contract-dialogs/validation';
import { STATUS, USER_TYPES } from '@/constants/common.constants';
import { CONFIG_CONSTANT } from '@/constants/contract.constants';
import { ContractListType } from '@/constants/types.constants';
import { useAuth } from '@/contexts/auth';
import {
  EditContractParamType,
  editContract,
  getContractsList,
} from '@/services/contract.service';
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import _ from 'lodash';
import { useTranslations } from 'next-intl';
import { useSearchParams } from 'next/navigation';
import { useState } from 'react';
import { toast } from 'sonner';

const PAGE_SIZE = 10;

type Props = {
  type: ContractListType;
};

export type OpenDialogType = null | 'accept' | 'cancel' | 'edit';

const ContractsListContainer: React.FC<Props> = (props: Props) => {
  const { type } = props;
  const t = useTranslations();
  const queryClient = useQueryClient();
  const searchParams = useSearchParams();

  const { userData } = useAuth();

  const [selectedContractId, setSelectedContractId] = useState<string | null>(
    null
  );
  const [openDialog, setOpenDialog] = useState<OpenDialogType>(null);

  const handleChangeOpenDialog = (value: OpenDialogType) => {
    setOpenDialog(value);
  };

  const handleChangeSelectedContractId = (value: string) => {
    setSelectedContractId(value);
  };

  const currentPage = searchParams.has('page')
    ? Number(searchParams.get('page'))
    : 1;

  const config = _.get(CONFIG_CONSTANT, [type, userData?.type]);

  const { data, isLoading, refetch } = useQuery({
    queryKey: [config?.key, currentPage],
    queryFn: () => {
      if (userData?.type === USER_TYPES.ORGANIZATION)
        return getContractsList({
          limit: PAGE_SIZE,
          page: currentPage,
          restaurantContractStatus: config.restaurantContractStatus,
          organizationContractStatus: config.organizationContractStatus,
          status: STATUS.ACTIVE,
        });

      return getContractsList({
        limit: PAGE_SIZE,
        page: currentPage,
        restaurantContractStatus: config.restaurantContractStatus,
        organizationContractStatus: config.organizationContractStatus,
        status: STATUS.ACTIVE,
      });
    },
    enabled:
      userData?.type === USER_TYPES.ORGANIZATION ||
      userData?.type === USER_TYPES.RESTAURANT,
  });

  const { mutateAsync: editMutate, isPending: editIsPending } = useMutation({
    mutationFn: ({ id, param }: { id: string; param: EditContractParamType }) =>
      editContract(id, param),
    onSuccess: () => {
      refetch();
      queryClient.invalidateQueries(config.key);
      if (openDialog === 'accept')
        toast.success(t('common.requests.success.contract_accept'));
      else if (openDialog === 'edit')
        toast.success(t('common.requests.success.contract_request'));
      else if (props.type === 'receiving' && openDialog === 'cancel')
        toast.success(t('common.requests.success.contract_deny'));
      else if (openDialog === 'cancel')
        toast.success(t('common.requests.success.contract_cancel'));
      handleChangeOpenDialog(null);
    },
    onError: () => {
      toast.error(t('common.requests.error.system'));
    },
  });

  const handleActionSubmit = (values?: ContractValuesType) => {
    if (selectedContractId) {
      switch (openDialog) {
        case 'cancel':
          editMutate({
            id: selectedContractId,
            param: {
              status: STATUS.INACTIVE,
            },
          });
          break;
        case 'accept':
          editMutate({
            id: selectedContractId,
            param: {
              organizationStatus: 'ACCEPTED',
              restaurantStatus: 'ACCEPTED',
            },
          });
          break;
        case 'edit':
          const param: { id: string; param: EditContractParamType } = {
            id: selectedContractId,
            param: {
              menuId: Number(values?.menuId),
              menuUnitPrice: Number(values?.price),
              orderLimitTime: values?.limitTime,
              orderLimitFoodCount: Number(values?.limitCount),
              orderLimitFoodMoney: Number(values?.price),
            },
          };

          if (userData?.type === USER_TYPES.RESTAURANT) {
            param.param.restaurantStatus = 'ACCEPTED';
            param.param.organizationStatus = 'PENDING';
          } else {
            param.param.organizationStatus = 'ACCEPTED';
            param.param.restaurantStatus = 'PENDING';
          }

          editMutate(param);
          break;
        default:
          toast.error(t('common.requests.error.system'));
          break;
      }
    } else {
      toast.error(t('common.requests.error.system'));
    }
  };

  return (
    <div className="flex-col">
      <CardList meta={data?.meta} loading={isLoading} pageSize={PAGE_SIZE}>
        <div className="mt-5 grid grid-cols-1 gap-4 p-4 sm:grid-cols-2 sm:gap-6 md:grid-cols-3 md:gap-8 lg:grid-cols-5 lg:gap-8">
          {data?.items?.map((item) => (
            <ContractCard
              key={item.id}
              type={type}
              data={item}
              loading={editIsPending}
              handleChangeOpenDialog={handleChangeOpenDialog}
              handleChangeSelectedContractId={handleChangeSelectedContractId}
            />
          ))}
        </div>
      </CardList>
      <ContractAcceptDialog
        open={openDialog === 'accept'}
        handleClose={() => handleChangeOpenDialog(null)}
        onSubmit={handleActionSubmit}
      />
      {openDialog === 'edit' && (
        <ContractEditDialog
          data={data?.items.find(
            (item) => item.id === Number(selectedContractId)
          )}
          open={openDialog === 'edit'}
          handleClose={() => handleChangeOpenDialog(null)}
          onSubmit={handleActionSubmit}
        />
      )}
      <ContractCancelDialog
        open={openDialog === 'cancel'}
        handleClose={() => handleChangeOpenDialog(null)}
        onSubmit={handleActionSubmit}
      />
    </div>
  );
};

export default ContractsListContainer;

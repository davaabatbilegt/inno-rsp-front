'use client';

import { Input } from '@/components/ui/input';
import { QUERY_KEYS } from '@/constants/query-keys';
import { getEmployeeReportsList } from '@/services/employee-report.service';
import { useQuery } from '@tanstack/react-query';
import dayjs from 'dayjs';
import { usePathname, useRouter, useSearchParams } from 'next/navigation';
import React from 'react';

import EmployeeReportTableContainer from './table';

const EmployeesReportContainer: React.FC = () => {
  const router = useRouter();
  const pathname = usePathname();

  const searchParams = useSearchParams();

  const targetMonth = searchParams.has('targetMonth')
    ? dayjs(searchParams.get('targetMonth'))
    : undefined;

  const { data, isLoading } = useQuery({
    queryKey: [QUERY_KEYS.REPORT_LIST, targetMonth],
    queryFn: () => {
      return getEmployeeReportsList({
        targetMonth: dayjs(targetMonth).format('YYYY-MM-DD'),
      });
    },
  });

  return (
    <div>
      <section className="mb-4 mt-2 grid justify-end">
        <div className="mt-4 flex flex-row items-center justify-end space-x-3 sm:mt-0 sm:justify-end">
          <Input
            type="month"
            defaultValue={dayjs().format('YYYY-MM')}
            className="w-42 h-8"
            onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
              if (event.target.value) {
                const params = new URLSearchParams(searchParams);
                params.set('targetMonth', event.target.value);
                router.replace(`${pathname}?${params.toString()}`);
              } else {
                const params = new URLSearchParams(searchParams);
                params.delete('targetMonth');
                router.replace(`${pathname}?${params.toString()}`);
              }
            }}
          />
        </div>
      </section>
      <section className="w-full">
        <EmployeeReportTableContainer data={data} isLoading={isLoading} />
      </section>
      <div />
    </div>
  );
};

export default EmployeesReportContainer;

'use client';

import EmptyData from '@/components/empty-data';
import { Skeleton } from '@/components/ui/skeleton';
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from '@/components/ui/table';
import { EmployeeReportType } from '@/services/employee-report.service';
import {
  ColumnDef,
  flexRender,
  getCoreRowModel,
  useReactTable,
} from '@tanstack/react-table';
import { useTranslations } from 'next-intl';
import React from 'react';

type Props = {
  data?: EmployeeReportType[];
  isLoading: boolean;
};

const EmployeeReportTableContainer: React.FC<Props> = (props: Props) => {
  const { data, isLoading } = props;
  const t = useTranslations();

  const orderedSum = data?.reduce(
    (acc, curr) => acc + parseInt(curr.ordered),
    0
  );
  const takenSum = data?.reduce((acc, curr) => acc + parseInt(curr.taken), 0);
  const uniqueEmployees = new Set(data?.map((item) => item.employee_code));
  const totalEmployees = uniqueEmployees.size;

  const columns: ColumnDef<EmployeeReportType>[] = [
    {
      id: 'id',
      header: t('reports.id'),
      cell: ({ row }) => <div>{row.index + 1}</div>,
    },
    {
      accessorKey: 'employee_code',
      header: t('reports.employee_code'),
      cell: ({ row }) => <div>{row.getValue('employee_code')}</div>,
    },
    {
      accessorKey: 'ordered',
      header: t('reports.ordered'),
      cell: ({ row }) => <div>{row.getValue('ordered')}</div>,
    },
    {
      accessorKey: 'taken',
      header: t('reports.taken'),
      cell: ({ row }) => <div>{row.getValue('taken')}</div>,
    },
  ];

  const table = useReactTable({
    data: data || [],
    columns,
    getCoreRowModel: getCoreRowModel(),
  });

  return (
    <Table>
      <TableHeader>
        {table.getHeaderGroups().map((headerGroup) => (
          <TableRow key={headerGroup.id} className="bg-slate-100">
            {headerGroup.headers.map((header) => {
              return (
                <TableHead key={header.id} className="border text-center">
                  {header.isPlaceholder
                    ? null
                    : flexRender(
                        header.column.columnDef.header,
                        header.getContext()
                      )}
                </TableHead>
              );
            })}
          </TableRow>
        ))}
      </TableHeader>
      <TableBody>
        {table.getRowModel().rows?.length ? (
          <>
            {table.getRowModel().rows.map((row) => (
              <TableRow key={row.id} className="bg-white">
                {row.getVisibleCells().map((cell) => (
                  <TableCell key={cell.id} className="border text-center">
                    {isLoading ? (
                      <Skeleton
                        key={row.id}
                        className="h-[30px] w-full rounded-xl"
                      />
                    ) : (
                      flexRender(cell.column.columnDef.cell, cell.getContext())
                    )}
                  </TableCell>
                ))}
              </TableRow>
            ))}
            <TableRow className="bg-slate-300">
              <TableCell className="border text-center">
                {t('reports.total')}
              </TableCell>
              <TableCell className="border text-center">
                {totalEmployees}
              </TableCell>
              <TableCell className="border text-center">{orderedSum}</TableCell>
              <TableCell className="border text-center">{takenSum}</TableCell>
            </TableRow>
          </>
        ) : (
          <TableRow>
            <TableCell
              colSpan={columns.length}
              className="h-24 border bg-white text-center"
            >
              <EmptyData size="large" />
            </TableCell>
          </TableRow>
        )}
      </TableBody>
    </Table>
  );
};

export default EmployeeReportTableContainer;

'use client';

import CustomButton from '@/components/custom-button';
import { Button } from '@/components/ui/button';
import {
  Dialog,
  DialogContent,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from '@/components/ui/dialog';
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from '@/components/ui/form';
import { Input } from '@/components/ui/input';
import { Colors } from '@/constants/color.constants';
import { STATUS } from '@/constants/common.constants';
import { useAuth } from '@/contexts/auth';
import {
  EmployeeParamType,
  addEmployee,
} from '@/services/organization-employee.service';
import { useMutation } from '@tanstack/react-query';
import { UserPlus } from 'lucide-react';
import { useTranslations } from 'next-intl';
import React, { useEffect, useState } from 'react';
import { toast } from 'sonner';
import { z } from 'zod';

import { useEmployeeValidation } from './validation';

type Props = {
  refetch: () => void;
};

const EmployeeAddForm: React.FC<Props> = (props: Props) => {
  const t = useTranslations();
  const { userData } = useAuth();
  const { schema, form } = useEmployeeValidation();
  const [open, setOpen] = useState(false);
  const { refetch } = props;

  const { mutateAsync, isPending } = useMutation({
    mutationFn: (param: EmployeeParamType) => addEmployee(param),
    onSuccess: () => {
      toast.success(t('common.requests.success.add_employee'));
      refetch();
      setOpen(false);
    },
    onError: () => {
      toast.error(t('common.requests.error.system'));
    },
  });

  const onSubmit = (values: z.infer<typeof schema>) => {
    mutateAsync({
      organizationId: userData?.id,
      employeeCode: values.employeeCode,
      username: values.employeeUsername,
      password: values.employeePassword,
      status: STATUS.ACTIVE,
    });
  };

  useEffect(() => {
    form.reset();
  }, [open]);

  return (
    <Dialog open={open} onOpenChange={setOpen}>
      <DialogTrigger asChild>
        <CustomButton
          icon={<UserPlus color="#ffffff" className="h-4 w-4" />}
          text={t('employees_manage.create')}
          onClick={() => setOpen(true)}
          className={`mt-4 h-[40px] text-white sm:mt-0 ${Colors.CREATE_GREEN} ${Colors.HOVER_GREEN}`}
        />
      </DialogTrigger>
      <DialogContent className="sm:max-w-[425px]">
        <DialogHeader>
          <DialogTitle>
            {t('employees_manage.add_employee_form_title')}
          </DialogTitle>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-4">
            <FormField
              control={form.control}
              name="employeeCode"
              render={({ field }) => (
                <FormItem className="w-full">
                  <FormLabel className="font-bold">
                    {t('employees_manage.employee_code')}
                  </FormLabel>
                  <FormControl>
                    <Input
                      placeholder={t('employees_manage.employee_code')}
                      {...field}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="employeeUsername"
              render={({ field }) => (
                <FormItem className="w-full">
                  <FormLabel className="font-bold">
                    {t('employees_manage.username')}
                  </FormLabel>
                  <FormControl>
                    <Input
                      placeholder={t('employees_manage.username')}
                      {...field}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="employeePassword"
              render={({ field }) => (
                <FormItem className="w-full">
                  <FormLabel className="font-bold">
                    {t('employees_manage.password')}
                  </FormLabel>
                  <FormControl>
                    <Input
                      placeholder={t('employees_manage.password')}
                      type="password"
                      {...field}
                    />
                  </FormControl>
                  <FormMessage />
                </FormItem>
              )}
            />
            <div className="flex w-full justify-end">
              <Button
                type="button"
                className="mr-5 w-full"
                variant={'outline'}
                onClick={() => setOpen(false)}
              >
                {t('employees_manage.cancel')}
              </Button>
              <CustomButton
                text={t('employees_manage.submit')}
                type="submit"
                className={`w-full ${Colors.GREEN} ${Colors.HOVER_GREEN}`}
                loading={isPending}
              />
            </div>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};

export default EmployeeAddForm;

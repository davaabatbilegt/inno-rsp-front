'use client';

import { zodResolver } from '@hookform/resolvers/zod';
import { useTranslations } from 'next-intl';
import { useForm } from 'react-hook-form';
import { z } from 'zod';

const initialValues = {
  employeeCode: '',
  employeeUsername: '',
  employeePassword: '',
};

export const useEmployeeValidation = () => {
  const t = useTranslations();

  const schema = z.object({
    employeeCode: z.string().min(1, {
      message: t('employees_manage.validation.employee_code_required'),
    }),
    employeeUsername: z
      .string()
      .min(1, { message: t('employees_manage.validation.username_required') }),
    employeePassword: z
      .string()
      .min(1, { message: t('employees_manage.validation.password_required') }),
  });

  const form = useForm<z.infer<typeof schema>>({
    resolver: zodResolver(schema),
    defaultValues: initialValues,
  });

  return { schema, form };
};

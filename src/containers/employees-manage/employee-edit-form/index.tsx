import CustomButton from '@/components/custom-button';
import { Button } from '@/components/ui/button';
import {
  Dialog,
  DialogContent,
  DialogHeader,
  DialogTitle,
} from '@/components/ui/dialog';
import {
  Form,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from '@/components/ui/form';
import { Input } from '@/components/ui/input';
import { Colors } from '@/constants/color.constants';
import {
  EmployeeEditParamType,
  EmployeeType,
  editEmployee,
} from '@/services/organization-employee.service';
import { useMutation } from '@tanstack/react-query';
import { useTranslations } from 'next-intl';
import React, { useEffect } from 'react';
import { toast } from 'sonner';
import { z } from 'zod';

import { useEmployeeValidation } from './validation';

type Props = {
  open: boolean;
  data: EmployeeType | null;
  onClose: () => void;
  refetch: () => void;
};

const EmployeeEditForm: React.FC<Props> = (props: Props) => {
  const t = useTranslations();
  const { form, schema } = useEmployeeValidation();
  const { open, data, onClose, refetch } = props;

  const { mutateAsync, isPending } = useMutation({
    mutationFn: (param: EmployeeEditParamType) => editEmployee(param),
    onSuccess: () => {
      toast.success(t('common.requests.success.edit_employee'));
      refetch();
      onClose();
    },
    onError: () => {
      toast.error(t('common.requests.error.system'));
    },
  });

  const onSubmit = (values: z.infer<typeof schema>) => {
    let params;
    if (values.employeePassword) {
      params = {
        employeeCode: values.employeeCode,
        username: values.employeeUsername,
        password: values.employeePassword,
      };
    } else {
      params = {
        employeeCode: values.employeeCode,
        username: values.employeeUsername,
      };
    }
    if (data?.id) {
      mutateAsync({
        id: data.id,
        params: params,
      });
    } else {
      toast.error(t('common.requests.error.system'));
    }
  };

  useEffect(() => {
    if (data) {
      form.setValue('employeeCode', data.employeeCode);
      form.setValue('employeeUsername', data.username);
    }
  });
  return (
    <Dialog
      open={open}
      onOpenChange={() => {
        onClose();
      }}
    >
      <DialogContent className="sm:max-w-[425px]">
        <DialogHeader>
          <DialogTitle>
            {t('employees_manage.edit_employee_form_title')}
          </DialogTitle>
        </DialogHeader>
        <Form {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-4">
            <FormField
              control={form.control}
              name="employeeCode"
              render={({ field }) => (
                <FormItem className="w-full">
                  <FormLabel className="font-bold">
                    {t('employees_manage.employee_code')}
                  </FormLabel>
                  <Input
                    placeholder={t('employees_manage.employee_code')}
                    {...field}
                  />
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="employeeUsername"
              render={({ field }) => (
                <FormItem className="w-full">
                  <FormLabel className="font-bold">
                    {t('employees_manage.username')}
                  </FormLabel>
                  <Input
                    placeholder={t('employees_manage.username')}
                    {...field}
                  />
                  <FormMessage />
                </FormItem>
              )}
            />
            <FormField
              control={form.control}
              name="employeePassword"
              render={({ field }) => (
                <FormItem className="w-full">
                  <FormLabel className="font-bold">
                    {t('employees_manage.password')}
                  </FormLabel>
                  <Input
                    placeholder={t('employees_manage.password')}
                    type="password"
                    {...field}
                  />
                  <FormMessage />
                </FormItem>
              )}
            />
            <div className="flex w-full justify-end">
              <Button
                type="button"
                className="mr-5 w-full"
                variant={'outline'}
                onClick={() => onClose()}
              >
                {t('employees_manage.cancel')}
              </Button>
              <CustomButton
                text={t('employees_manage.submit')}
                type="submit"
                className={`w-full ${Colors.BLUE} ${Colors.HOVER_BLUE}`}
                loading={isPending}
              />
            </div>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  );
};

export default EmployeeEditForm;

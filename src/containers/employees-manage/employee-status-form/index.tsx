import CustomButton from '@/components/custom-button';
import { Button } from '@/components/ui/button';
import {
  Dialog,
  DialogClose,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
} from '@/components/ui/dialog';
import { Colors } from '@/constants/color.constants';
import { STATUS } from '@/constants/common.constants';
import {
  EmployeeEditParamType,
  EmployeeType,
  editEmployee,
} from '@/services/organization-employee.service';
import { useMutation } from '@tanstack/react-query';
import { useTranslations } from 'next-intl';
import React from 'react';
import { toast } from 'sonner';

type Props = {
  open: boolean;
  data: EmployeeType | null;
  onClose: () => void;
  refetch: () => void;
};

const EmployeeStatusForm: React.FC<Props> = (props: Props) => {
  const t = useTranslations();
  const { open, data, onClose, refetch } = props;

  const { mutateAsync, isPending } = useMutation({
    mutationFn: (param: EmployeeEditParamType) => editEmployee(param),
    onSuccess: () => {
      toast.success(t('common.requests.success.change_employee_status'));
      refetch();
      onClose();
    },
    onError: () => {
      toast.error(t('common.requests.error.system'));
    },
  });

  const onSubmit = () => {
    const status =
      data?.status === STATUS.INACTIVE ? STATUS.ACTIVE : STATUS.INACTIVE;
    if (data?.id) {
      mutateAsync({
        id: data.id,
        params: {
          status: status,
        },
      });
    } else {
      toast.error(t('common.requests.error.system'));
    }
  };

  return (
    <Dialog open={open} onOpenChange={() => onClose()}>
      <DialogContent className="sm:max-w-[425px]">
        <DialogHeader>
          <DialogTitle>{t('employees_manage.delete_dialog.title')}</DialogTitle>
          <DialogDescription>
            {t('employees_manage.delete_dialog.description')}
          </DialogDescription>
        </DialogHeader>
        <DialogClose onClick={() => onClose()} />
        <DialogFooter>
          <Button
            variant="outline"
            onClick={() => onClose()}
            className="mt-4 w-full"
          >
            {t('common.no')}
          </Button>
          <CustomButton
            text={t('common.yes')}
            onClick={() => onSubmit()}
            className={`mt-4 w-full ${Colors.RED} ${Colors.HOVER_RED}`}
            loading={isPending}
          />
        </DialogFooter>
      </DialogContent>
    </Dialog>
  );
};

export default EmployeeStatusForm;

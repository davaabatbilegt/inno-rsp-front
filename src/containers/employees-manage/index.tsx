'use client';

import CustomPagination from '@/components/custom-pagination';
import EmptyData from '@/components/empty-data';
import { Badge } from '@/components/ui/badge';
import { Button } from '@/components/ui/button';
import { Separator } from '@/components/ui/separator';
import { Skeleton } from '@/components/ui/skeleton';
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from '@/components/ui/table';
import { STATUS, STATUS_LIST } from '@/constants/common.constants';
import { QUERY_KEYS } from '@/constants/query-keys';
import { StatusType } from '@/constants/types.constants';
import {
  EmployeeType,
  getEmployeesList,
} from '@/services/organization-employee.service';
import { useQuery } from '@tanstack/react-query';
import {
  ColumnDef,
  flexRender,
  getCoreRowModel,
  useReactTable,
} from '@tanstack/react-table';
import _ from 'lodash';
import { Settings, SquarePen } from 'lucide-react';
import { useTranslations } from 'next-intl';
import { useSearchParams } from 'next/navigation';
import React, { useState } from 'react';

import EmployeeAddForm from './employee-add-form';
import EmployeeEditForm from './employee-edit-form';
import EmployeeFilter from './employee-filter';
import EmployeeStatusForm from './employee-status-form';

const PAGE_SIZE = 10;

const EmployeesManageContainer: React.FC = () => {
  const t = useTranslations();
  const [editDialog, setEditDialog] = useState<boolean>(false);
  const [statusDialog, setStatusDialog] = useState<boolean>(false);
  const [editData, setEditData] = useState<EmployeeType | null>(null);

  const searchParams = useSearchParams();

  const currentPage = searchParams.has('page')
    ? Number(searchParams.get('page'))
    : 1;

  const statusFilter =
    searchParams.has('status') &&
    STATUS_LIST.some((status) => status.value === searchParams.get('status'))
      ? (searchParams.get('status') as StatusType)
      : undefined;

  const { data, isLoading, refetch } = useQuery({
    queryKey: [
      QUERY_KEYS.ORGANIZATION_EMPLOYEE_LIST,
      currentPage,
      statusFilter,
    ],
    queryFn: () => {
      return getEmployeesList({
        limit: PAGE_SIZE,
        page: currentPage,
        status: statusFilter,
      });
    },
  });

  const columns: ColumnDef<EmployeeType>[] = [
    {
      id: 'id',
      header: t('employees_manage.id'),
      cell: ({ row }) => (
        <div>{(currentPage - 1) * PAGE_SIZE + row.index + 1}</div>
      ),
    },
    {
      accessorKey: 'employeeCode',
      header: t('employees_manage.employee_code'),
      cell: ({ row }) => <div>{row.getValue('employeeCode')}</div>,
    },
    {
      accessorKey: 'username',
      header: t('employees_manage.username'),
      cell: ({ row }) => <div>{row.getValue('username')}</div>,
    },
    {
      accessorKey: 'status',
      header: t('employees_manage.status'),
      cell: ({ row }) => (
        <div>
          <Badge
            className={`w-20 justify-center ${
              row.getValue('status') === STATUS.ACTIVE
                ? 'bg-green-500'
                : 'bg-red-500'
            }`}
          >
            {t(`employees_manage.${row.getValue('status')}`)}
          </Badge>
        </div>
      ),
    },
    {
      id: 'actions',
      header: t('employees_manage.actions'),
      enableHiding: false,
      cell: ({ row }) => {
        return (
          <div className="flex flex-row justify-center">
            {row.getValue('status') === STATUS.ACTIVE ? (
              <>
                <Button
                  variant="ghost"
                  size="icon"
                  onClick={() => {
                    setEditDialog(true), setEditData(row.original);
                  }}
                >
                  <SquarePen className="h-5 w-5" />
                </Button>
                <Separator orientation="vertical" className="mx-1 mt-1 h-8" />
              </>
            ) : (
              <></>
            )}
            <Button
              variant="ghost"
              size="icon"
              onClick={() => {
                setStatusDialog(true), setEditData(row.original);
              }}
            >
              <Settings className="h-5 w-5" />
            </Button>
          </div>
        );
      },
    },
  ];

  const table = useReactTable({
    data: _.get(data, 'items', []),
    columns,
    getCoreRowModel: getCoreRowModel(),
  });

  return (
    <div>
      <section className="grid grid-cols-1 p-2 pb-4 md:grid-cols-2">
        <h2 className="font-extrabold">{t('employees_manage.index')}</h2>
        <div className="flex flex-col items-start space-x-3 sm:flex-row sm:items-center sm:justify-end">
          <EmployeeFilter value={statusFilter} />
          <EmployeeAddForm refetch={refetch} />
        </div>
        <EmployeeEditForm
          open={editDialog}
          data={editData}
          onClose={() => setEditDialog(false)}
          refetch={refetch}
        />
        <EmployeeStatusForm
          open={statusDialog}
          data={editData}
          onClose={() => setStatusDialog(false)}
          refetch={refetch}
        />
      </section>
      <section className="w-full">
        <Table>
          <TableHeader>
            {table.getHeaderGroups().map((headerGroup) => (
              <TableRow key={headerGroup.id} className="bg-slate-100">
                {headerGroup.headers.map((header) => {
                  return (
                    <TableHead key={header.id} className="border text-center">
                      {header.isPlaceholder
                        ? null
                        : flexRender(
                            header.column.columnDef.header,
                            header.getContext()
                          )}
                    </TableHead>
                  );
                })}
              </TableRow>
            ))}
          </TableHeader>
          <TableBody>
            {table.getRowModel().rows?.length ? (
              table.getRowModel().rows.map((row) => (
                <TableRow key={row.id} className="bg-white">
                  {row.getVisibleCells().map((cell) => (
                    <TableCell key={cell.id} className="border text-center">
                      {isLoading ? (
                        <Skeleton
                          key={row.id}
                          className="h-[30px] w-full rounded-xl"
                        />
                      ) : (
                        flexRender(
                          cell.column.columnDef.cell,
                          cell.getContext()
                        )
                      )}
                    </TableCell>
                  ))}
                </TableRow>
              ))
            ) : (
              <TableRow>
                <TableCell
                  colSpan={columns.length}
                  className="h-24 border bg-white text-center"
                >
                  <EmptyData size="large" />
                </TableCell>
              </TableRow>
            )}
          </TableBody>
        </Table>
      </section>
      {table.getRowModel().rows?.length ? (
        <div className="mt-8 flex-col">
          <CustomPagination
            page={_.get(data, 'meta.currentPage')}
            totalPage={_.get(data, 'meta.totalPages')}
            loading={isLoading}
          />
        </div>
      ) : null}
      <div />
    </div>
  );
};

export default EmployeesManageContainer;

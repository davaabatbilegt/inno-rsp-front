'use client';

import { USER_TYPES, UserTypesType } from '@/constants/common.constants';
import { NextPage } from 'next';

import InvoiceOrganizationContainer from './organization';
import InvoiceRestaurantContainer from './restaurant';

type Props = {
  type: UserTypesType;
};
const InvoiceContainer: NextPage<Props> = (props: Props) => {
  const { type } = props;
  if (type === USER_TYPES.ORGANIZATION) {
    return <InvoiceOrganizationContainer />;
  } else {
    return <InvoiceRestaurantContainer />;
  }
};
export default InvoiceContainer;

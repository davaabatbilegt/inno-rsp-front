import { Badge } from '@/components/ui/badge';
import { Button } from '@/components/ui/button';
import {
  Command,
  CommandGroup,
  CommandList,
  CommandSeparator,
} from '@/components/ui/command';
import {
  Popover,
  PopoverContent,
  PopoverTrigger,
} from '@/components/ui/popover';
import { Separator } from '@/components/ui/separator';
import {
  BILL_STATUS,
  BILL_STATUS_LIST,
  BillStatusItem,
} from '@/constants/common.constants';
import { BillStatusType } from '@/constants/types.constants';
import { usePathname, useRouter } from '@/lib/navigation';
import { cn } from '@/lib/utils';
import { CheckIcon, PlusCircleIcon } from 'lucide-react';
import { useTranslations } from 'next-intl';
import { useSearchParams } from 'next/navigation';
import { useState } from 'react';

type Props = {
  value: BillStatusType | undefined;
};

const InvoiceFilter: React.FC<Props> = (props: Props) => {
  const { value } = props;
  const router = useRouter();
  const pathname = usePathname();
  const searchParams = useSearchParams();
  const t = useTranslations();

  const currentValue = value === undefined ? [] : [value];
  const [filter, setFilter] = useState(currentValue);

  const handleFilter = (selectedOption: BillStatusType) => {
    const params = new URLSearchParams(searchParams);
    params.set('billStatus', selectedOption);
    router.replace(`${pathname}?${params.toString()}`);
    setFilter([selectedOption]);
  };

  const handleClear = () => {
    const params = new URLSearchParams(searchParams);
    params.delete('billStatus');
    router.replace(`${pathname}?${params.toString()}`);
    setFilter([]);
  };

  const getStatusColor = (status: BillStatusType) => {
    switch (status) {
      case BILL_STATUS.CONFIRMED:
        return 'bg-green-500 text-white';
      case BILL_STATUS.NOT_PAID:
        return 'bg-yellow-500 text-black';
      case BILL_STATUS.BILLED:
        return 'bg-orange-500 text-black';
      case BILL_STATUS.CALCULATED:
        return 'bg-orange-700 text-white';
      default:
        return '';
    }
  };

  return (
    <Popover>
      <PopoverTrigger asChild>
        <Button variant="outline" size="sm" className="h-8 border-dashed">
          <PlusCircleIcon className="mr-2 h-4 w-4" />
          {t('invoices.status')}
          {currentValue.length > 0 && (
            <>
              <Separator orientation="vertical" className="mx-2 h-4" />
              <div className="space-x-1 overflow-hidden lg:flex">
                {BILL_STATUS_LIST.map((option: BillStatusItem) => {
                  const isSelected = filter.includes(option.value);
                  if (isSelected) {
                    const colorClasses = getStatusColor(option.value);
                    return (
                      <Badge
                        key={option.value}
                        className={`w-24 justify-center rounded-sm px-1 font-normal ${colorClasses} ${
                          isSelected
                            ? 'opacity-100'
                            : 'opacity-50 [&_svg]:invisible'
                        }`}
                        onClick={() => handleFilter(option.value)}
                      >
                        {t(`common.status_${option.label}`)}
                      </Badge>
                    );
                  }
                  return null;
                })}
              </div>
            </>
          )}
        </Button>
      </PopoverTrigger>
      <PopoverContent className="m-1 w-[200px] pb-2 pt-0" align="start">
        <Command>
          <CommandList>
            <CommandGroup>
              {BILL_STATUS_LIST.map((option: BillStatusItem) => {
                const isSelected = filter.includes(option.value);
                return (
                  <div
                    key={option.value}
                    onClick={() => handleFilter(option.value)}
                    className="flex flex-row items-center space-y-3"
                  >
                    <div
                      className={cn(
                        'mr-3 mt-3 flex h-4 w-4 rounded-sm border border-primary',
                        isSelected
                          ? 'bg-primary text-primary-foreground'
                          : 'opacity-50 [&_svg]:invisible'
                      )}
                    >
                      <CheckIcon className={cn('h-4 w-4')} />
                    </div>
                    <span>{t(`common.status_${option.label}`)}</span>
                  </div>
                );
              })}
            </CommandGroup>
            {filter.length > 0 && (
              <>
                <CommandSeparator className="mt-2" />
                <CommandGroup>
                  <div
                    onClick={() => handleClear()}
                    className="cursor-pointer justify-center text-center"
                  >
                    {t('invoices.clear_filters')}
                  </div>
                </CommandGroup>
              </>
            )}
          </CommandList>
        </Command>
      </PopoverContent>
    </Popover>
  );
};

export default InvoiceFilter;

'use client';

import CustomButton from '@/components/custom-button';
import CustomPagination from '@/components/custom-pagination';
import EmptyData from '@/components/empty-data';
import DialogInvoiceBillStatus from '@/components/invoice-dialogs/bill-status-dialog';
import DialogInvoiceCreate from '@/components/invoice-dialogs/create-dialog';
import { Badge } from '@/components/ui/badge';
import { Button } from '@/components/ui/button';
import { Input } from '@/components/ui/input';
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from '@/components/ui/select';
import { Skeleton } from '@/components/ui/skeleton';
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from '@/components/ui/table';
import { Colors } from '@/constants/color.constants';
import {
  BILL_STATUS,
  BILL_STATUS_LIST,
  EMPTY_VALUE,
  OrganizationSelectItem,
} from '@/constants/common.constants';
import { QUERY_KEYS } from '@/constants/query-keys';
import { BillStatusType } from '@/constants/types.constants';
import { formatMoney } from '@/lib/utils';
import {
  CreateInvoiceParamType,
  InvoiceType,
  createInvoice,
  getInvoicesList,
} from '@/services/invoice.service';
import {
  OrganizationType,
  getOrganizationsList,
} from '@/services/organization.service';
import { useMutation, useQuery } from '@tanstack/react-query';
import {
  ColumnDef,
  flexRender,
  getCoreRowModel,
  useReactTable,
} from '@tanstack/react-table';
import dayjs from 'dayjs';
import _ from 'lodash';
import { CheckCheck, PackageCheck } from 'lucide-react';
import { useTranslations } from 'next-intl';
import { usePathname, useRouter, useSearchParams } from 'next/navigation';
import React, { useEffect, useState } from 'react';
import { toast } from 'sonner';

import InvoiceFilter from '../invoice-filter';

const PAGE_SIZE = 12;

const InvoiceRestaurantContainer: React.FC = () => {
  const t = useTranslations();
  const router = useRouter();
  const pathname = usePathname();
  const [openCreateDialog, setOpenCreateDialog] = useState<boolean>(false);
  const [openBillStatusDialog, setOpenBillStatusDialog] =
    useState<boolean>(false);
  const [editData, setEditData] = useState<InvoiceType | null>(null);
  const [organizationList, setOrganizationList] = useState<OrganizationType[]>(
    []
  );
  const [organizationSelectList, setOrganizationSelectList] = useState<
    OrganizationSelectItem[]
  >([{ id: 0, name: t('invoices.all') }]);
  const [selectedOrganization, setSelectedOrganization] = useState<string>('0');

  const searchParams = useSearchParams();

  const currentPage = searchParams.has('page')
    ? Number(searchParams.get('page'))
    : 1;

  const billStatusFilter =
    searchParams.has('billStatus') &&
    BILL_STATUS_LIST.some(
      (status) => status.value === searchParams.get('billStatus')
    )
      ? (searchParams.get('billStatus') as BillStatusType)
      : undefined;

  const selected = searchParams.has('selectedOrganization')
    ? searchParams.get('selectedOrganization')
    : undefined;

  const yearMonth = searchParams.has('yearMonth')
    ? dayjs(searchParams.get('yearMonth'))
    : undefined;

  const { data, isLoading, refetch } = useQuery({
    queryKey: [
      QUERY_KEYS.INVOICES_LIST,
      currentPage,
      billStatusFilter,
      selected,
      yearMonth,
    ],
    queryFn: () => {
      return getInvoicesList({
        limit: PAGE_SIZE,
        page: currentPage,
        billStatus: billStatusFilter,
        organizationId:
          selectedOrganization !== '0'
            ? parseInt(selectedOrganization)
            : undefined,
        yearMonth: yearMonth
          ? dayjs(yearMonth).format('YYYY-MM-DD')
          : undefined,
      });
    },
  });

  useEffect(() => {
    const fetchOrganizations = async () => {
      try {
        const response = await getOrganizationsList({ contracted: true });
        setOrganizationList(response.items);
        response.items.forEach((item: OrganizationType) => {
          setOrganizationSelectList((prevList) => {
            if (!prevList.find((organization) => organization.id === item.id)) {
              return [...prevList, { name: item.name, id: item.id }];
            }
            return prevList;
          });
        });
      } catch (error) {
        toast.error(t('common.requests.error.system'));
      }
    };
    fetchOrganizations();
  }, []);

  const { mutateAsync: createInvoiceMutate, isPending: createIsPending } =
    useMutation({
      mutationFn: (param: CreateInvoiceParamType) => createInvoice(param),
      onSuccess: () => {
        toast.success(t('common.requests.success.create_invoice'));
        refetch();
        setOpenCreateDialog(false);
      },
      onError: () => {
        toast.error(t('common.requests.error.system'));
      },
    });

  const handleCreateActionSubmit = (values: CreateInvoiceParamType) => {
    createInvoiceMutate(values);
  };

  const totalMoneySum = _.get(data, 'items', []).reduce(
    (acc, curr) => acc + curr.totalMoney,
    0
  );

  const columns: ColumnDef<InvoiceType>[] = [
    {
      id: 'id',
      header: t('invoices.id'),
      cell: ({ row }) => (
        <div>{(currentPage - 1) * PAGE_SIZE + row.index + 1}</div>
      ),
    },
    {
      accessorKey: 'organization',
      header: t('invoices.organization'),
      cell: ({ row }) => <div>{row.original.organization.name}</div>,
    },
    {
      accessorKey: 'yearMonth',
      header: t('invoices.year_month'),
      cell: ({ row }) => (
        <div>{dayjs(row.getValue('yearMonth')).format('YYYY-MM')}</div>
      ),
    },
    {
      accessorKey: 'totalMoney',
      header: t('invoices.total_price'),
      cell: ({ row }) => <div>{formatMoney(row.getValue('totalMoney'))}</div>,
    },
    {
      accessorKey: 'billStatus',
      header: t('invoices.bill_status'),
      cell: ({ row }) => (
        <div>
          <Badge
            className={`w-24 justify-center ${
              row.getValue('billStatus') === BILL_STATUS.CONFIRMED
                ? 'bg-green-500'
                : row.getValue('billStatus') === BILL_STATUS.NOT_PAID
                  ? 'bg-yellow-500'
                  : row.getValue('billStatus') === BILL_STATUS.BILLED
                    ? 'bg-orange-500'
                    : 'bg-orange-700'
            }`}
          >
            {t(`invoices.${row.getValue('billStatus')}`)}
          </Badge>
        </div>
      ),
    },
    {
      id: 'actions',
      header: t('invoices.actions'),
      enableHiding: false,
      cell: ({ row }) => {
        return (
          <div className="flex flex-row justify-center">
            {row.getValue('billStatus') === BILL_STATUS.BILLED ? (
              <CustomButton
                icon={<PackageCheck color="#ffffff" className="h-4 w-4" />}
                text={t(`invoices.BUTTON_${row.getValue('billStatus')}`)}
                variant="outline"
                className="bg-sky-500 text-white hover:bg-sky-400 hover:text-white"
                onClick={() => {
                  setEditData(row.original), setOpenBillStatusDialog(true);
                }}
              />
            ) : (
              <Button variant="ghost">{EMPTY_VALUE}</Button>
            )}
          </div>
        );
      },
    },
  ];

  const table = useReactTable({
    data: _.get(data, 'items', []),
    columns,
    getCoreRowModel: getCoreRowModel(),
  });

  return (
    <div>
      <section className="flex justify-between p-2 pb-4">
        <h2 className="font-extrabold">{t('invoices.index')}</h2>
        <div className="grid grid-cols-1 items-center justify-end space-x-3 space-y-4 md:grid-cols-2 lg:grid-cols-4 lg:space-y-0">
          <div className="md:mt-4 lg:mt-0">
            <Select
              onValueChange={(value) => {
                setSelectedOrganization(value);
                const params = new URLSearchParams(searchParams);
                params.set('selectedOrganization', value);
                router.replace(`${pathname}?${params.toString()}`);
              }}
              value={selectedOrganization}
            >
              <SelectTrigger className="ml-4 h-8 w-full sm:w-48">
                <SelectValue />
              </SelectTrigger>
              <SelectContent>
                {organizationSelectList.map(
                  (organization: OrganizationSelectItem) => (
                    <SelectItem
                      key={organization.id}
                      value={organization.id.toString()}
                    >
                      {organization.name}
                    </SelectItem>
                  )
                )}
              </SelectContent>
            </Select>
          </div>
          <Input
            type="month"
            className="w-42 h-8"
            onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
              if (event.target.value) {
                const params = new URLSearchParams(searchParams);
                params.set('yearMonth', event.target.value);
                router.replace(`${pathname}?${params.toString()}`);
              } else {
                const params = new URLSearchParams(searchParams);
                params.delete('yearMonth');
                router.replace(`${pathname}?${params.toString()}`);
              }
            }}
          />
          <InvoiceFilter value={billStatusFilter} />
          <CustomButton
            icon={<CheckCheck color="#ffffff" className="h-4 w-4" />}
            text={t('invoices.create')}
            variant="default"
            className={`w-130 h-8 overflow-hidden ${Colors.CREATE_GREEN} ${Colors.HOVER_GREEN}`}
            onClick={() => setOpenCreateDialog(true)}
          />
        </div>
      </section>
      <section className="w-full">
        <Table>
          <TableHeader>
            {table.getHeaderGroups().map((headerGroup) => (
              <TableRow key={headerGroup.id} className="bg-slate-100">
                {headerGroup.headers.map((header) => {
                  return (
                    <TableHead key={header.id} className="border text-center">
                      {header.isPlaceholder
                        ? null
                        : flexRender(
                            header.column.columnDef.header,
                            header.getContext()
                          )}
                    </TableHead>
                  );
                })}
              </TableRow>
            ))}
          </TableHeader>
          <TableBody>
            {table.getRowModel().rows?.length ? (
              <>
                {table.getRowModel().rows.map((row) => (
                  <TableRow key={row.id} className="bg-white">
                    {row.getVisibleCells().map((cell) => (
                      <TableCell key={cell.id} className="border text-center">
                        {isLoading ? (
                          <Skeleton
                            key={row.id}
                            className="h-[30px] w-full rounded-xl"
                          />
                        ) : (
                          flexRender(
                            cell.column.columnDef.cell,
                            cell.getContext()
                          )
                        )}
                      </TableCell>
                    ))}
                  </TableRow>
                ))}
                <TableRow className="bg-slate-300">
                  <TableCell
                    colSpan={columns.length / 2}
                    className="border text-center"
                  >
                    {t('invoices.total')}
                  </TableCell>
                  <TableCell
                    colSpan={columns.length / 2}
                    className="border text-center"
                  >
                    {formatMoney(totalMoneySum)}
                  </TableCell>
                </TableRow>
              </>
            ) : (
              <TableRow>
                <TableCell
                  colSpan={columns.length}
                  className="h-24 border bg-white text-center"
                >
                  <EmptyData size="large" />
                </TableCell>
              </TableRow>
            )}
          </TableBody>
        </Table>
      </section>
      {table.getRowModel().rows?.length ? (
        <div className="mt-8 flex-col">
          <CustomPagination
            page={_.get(data, 'meta.currentPage')}
            totalPage={_.get(data, 'meta.totalPages')}
            loading={isLoading}
          />
        </div>
      ) : null}
      <div />
      <DialogInvoiceCreate
        open={openCreateDialog}
        onSubmit={handleCreateActionSubmit}
        handleClose={() => setOpenCreateDialog(false)}
        loading={createIsPending}
        organizationList={organizationList}
      />
      <DialogInvoiceBillStatus
        open={openBillStatusDialog}
        onClose={() => setOpenBillStatusDialog(false)}
        data={editData}
        refetch={refetch}
      />
    </div>
  );
};

export default InvoiceRestaurantContainer;

'use client';

import CustomButton from '@/components/custom-button';
import FullLoader from '@/components/full-loader';
import { Button } from '@/components/ui/button';
import { Card } from '@/components/ui/card';
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from '@/components/ui/form';
import { Input } from '@/components/ui/input';
import { Label } from '@/components/ui/label';
import { RadioGroup, RadioGroupItem } from '@/components/ui/radio-group';
import { COOKIE_KEYS, UserTypesType } from '@/constants/common.constants';
import IMAGES from '@/constants/images.constants';
import { useAuth } from '@/contexts/auth';
import { getHomeRoute, usePathname, useRouter } from '@/lib/navigation';
import { LoginParamType, login } from '@/services/auth.service';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { setCookie } from 'cookies-next';
import { Building, UsersRound, Utensils } from 'lucide-react';
import { useTranslations } from 'next-intl';
import Image from 'next/image';
import { useEffect, useState } from 'react';
import { toast } from 'sonner';
import { z } from 'zod';

import { useLoginValidation } from './validation';

type Props = {
  type: UserTypesType;
};

const LoginContainer: React.FC<Props> = (props: Props) => {
  const t = useTranslations();
  const router = useRouter();
  const pathname = usePathname();
  const queryClient = useQueryClient();
  const { setIsAuthenticated } = useAuth();
  const { schema, form } = useLoginValidation();

  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    queryClient.resetQueries();
  }, []);

  const { mutateAsync, isPending } = useMutation({
    mutationFn: (param: LoginParamType) => login(param),
    onSuccess: (data) => {
      toast.success(t('common.requests.success.login'));
      setCookie(COOKIE_KEYS.ACCESS_TOKEN, data.access_token);
      setCookie(COOKIE_KEYS.TYPE, props.type);
      setIsAuthenticated(true);
      router.push(getHomeRoute(props.type));
    },
    onError: () => {
      toast.error(t('common.requests.error.system'));
      setLoading(false);
    },
  });

  const onSubmit = (values: z.infer<typeof schema>) => {
    setLoading(true);
    mutateAsync({
      username: values.username,
      password: values.password,
      type: props.type,
    });
  };

  const USER_TYPE_DATA: {
    icon: React.ReactNode;
    value: string;
  }[] = [
    {
      icon: <Utensils />,
      value: 'restaurant',
    },
    {
      icon: <Building />,
      value: 'organization',
    },
    {
      icon: <UsersRound />,
      value: 'employee',
    },
  ];

  const userType = pathname.split('/')[1];

  return (
    <div className="flex flex-col items-center justify-center bg-gradient-to-r from-emerald-100 via-teal-400 to-cyan-600 md:h-screen">
      <FullLoader loading={loading} />
      <div className="mb-6 mt-4 flex flex-col items-center md:mt-0">
        <RadioGroup
          value={userType}
          onValueChange={(e) => router.push(`/${e}/login`)}
          className="grid grid-cols-2 gap-2 md:grid-cols-3"
        >
          {USER_TYPE_DATA.map((item, index) => (
            <div key={index}>
              <RadioGroupItem
                value={item.value}
                id={item.value}
                className="peer sr-only"
              />
              <Label
                htmlFor={item.value}
                className="flex flex-col items-center justify-between rounded-md border-2 border-muted bg-popover p-4 hover:bg-accent hover:text-accent-foreground peer-data-[state=checked]:border-emerald-500 peer-data-[state=checked]:bg-emerald-500 peer-data-[state=checked]:text-white [&:has([data-state=checked])]:border-emerald-500"
              >
                <div className="mb-4">{item.icon}</div>
                {t(`login.${item.value}`)}
              </Label>
            </div>
          ))}
        </RadioGroup>
        <Card className="mt-5 flex flex-col gap-4 p-6 sm:w-[350px] md:w-[700px] md:flex-row">
          <div className="relative h-[300px] w-full max-w-md overflow-hidden rounded md:h-full md:w-1/2">
            <Image
              alt="restaurant-image"
              src={IMAGES.LOGIN_IMAGE}
              fill
              className="object-cover"
            />
          </div>
          <div className="rounded bg-slate-50 p-4 sm:w-full md:w-1/2">
            <div className="mb-2">{t('login.title')}</div>
            <Form {...form}>
              <form
                onSubmit={form.handleSubmit(onSubmit)}
                className="space-y-4"
              >
                <FormField
                  control={form.control}
                  name="username"
                  render={({ field }) => (
                    <FormItem className="w-full">
                      <FormLabel className="font-bold">
                        {t('login.username')}
                      </FormLabel>
                      <FormControl>
                        <Input
                          placeholder={t('login.username')}
                          autoComplete="off"
                          {...field}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
                <FormField
                  control={form.control}
                  name="password"
                  render={({ field }) => (
                    <FormItem className="w-full">
                      <FormLabel className="font-bold">
                        {t('login.password')}
                      </FormLabel>
                      <FormControl>
                        <Input
                          placeholder={t('login.password')}
                          type="password"
                          autoComplete="off"
                          {...field}
                        />
                      </FormControl>
                      <FormMessage />
                    </FormItem>
                  )}
                />
                <CustomButton
                  text={t('login.submit')}
                  type="submit"
                  className="w-full bg-emerald-500"
                  loading={isPending}
                />
              </form>
            </Form>
            <div className="flex w-full justify-center">
              <Button variant="link" className="mt-2 w-full text-xs">
                {t('login.forgot_password')}
              </Button>
            </div>
          </div>
        </Card>
      </div>
    </div>
  );
};

export default LoginContainer;

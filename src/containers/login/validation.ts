'use client';

import { zodResolver } from '@hookform/resolvers/zod';
import { useTranslations } from 'next-intl';
import { useForm } from 'react-hook-form';
import { z } from 'zod';

const initialValues = {
  username: '',
  password: '',
};

export const useLoginValidation = () => {
  const t = useTranslations();

  const schema = z.object({
    username: z
      .string()
      .min(1, { message: t('login.validation.username_required') }),
    password: z
      .string()
      .min(1, { message: t('login.validation.password_required') }),
  });

  const form = useForm<z.infer<typeof schema>>({
    resolver: zodResolver(schema),
    defaultValues: initialValues,
  });

  return { schema, form };
};

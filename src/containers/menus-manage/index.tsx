'use client';

import CustomButton from '@/components/custom-button';
import CustomPagination from '@/components/custom-pagination';
import EmptyData from '@/components/empty-data';
import DialogMenusCreate from '@/components/menu-dialogs/create-dialog';
import DialogMenusDelete from '@/components/menu-dialogs/delete-dialog';
import DialogMenusEdit from '@/components/menu-dialogs/edit-dialog';
import { MenuValuesType } from '@/components/menu-form/validation';
import { Badge } from '@/components/ui/badge';
import { Button } from '@/components/ui/button';
import { Skeleton } from '@/components/ui/skeleton';
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from '@/components/ui/table';
import { Colors } from '@/constants/color.constants';
import { STATUS, STATUS_LIST } from '@/constants/common.constants';
import { QUERY_KEYS } from '@/constants/query-keys';
import { StatusType } from '@/constants/types.constants';
import { formatMoney } from '@/lib/utils';
import {
  CreateMenuParamType,
  EditMenuParamType,
  MenuType,
  createMenu,
  editMenu,
  getMenusList,
} from '@/services/menu.service';
import { useMutation, useQuery } from '@tanstack/react-query';
import {
  ColumnDef,
  VisibilityState,
  flexRender,
  getCoreRowModel,
  getPaginationRowModel,
  useReactTable,
} from '@tanstack/react-table';
import _ from 'lodash';
import { CheckCheck, Settings, SquarePen } from 'lucide-react';
import { useTranslations } from 'next-intl';
import { useSearchParams } from 'next/navigation';
import * as React from 'react';
import { useState } from 'react';
import { toast } from 'sonner';

import MenuFilter from './menu-filter';

const PAGE_SIZE = 8;

type Props = {
  restaurant_id?: string;
};

const MenusManageContainer: React.FC<Props> = () => {
  const t = useTranslations();
  const searchParams = useSearchParams();

  const [selectedMenuId, setSelectedMenuId] = useState<string | null>(null);
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [openEditDialog, setOpenEditDialog] = useState<boolean>(false);
  const [openCreateDialog, setOpenCreateDialog] = useState<boolean>(false);
  const [menuItem, setMenuItem] = useState<EditMenuParamType>();

  const [columnVisibility, setColumnVisibility] =
    React.useState<VisibilityState>({});

  const currentPage = searchParams.has('page')
    ? Number(searchParams.get('page'))
    : 1;

  const statusFilter =
    searchParams.has('status') &&
    STATUS_LIST.some((status) => status.value === searchParams.get('status'))
      ? (searchParams.get('status') as StatusType)
      : undefined;

  const { data, isLoading, refetch } = useQuery({
    queryKey: [QUERY_KEYS.MENU_LIST, currentPage, statusFilter],
    queryFn: () =>
      getMenusList({
        limit: PAGE_SIZE,
        page: currentPage,
        status: statusFilter,
      }),
  });

  const { mutateAsync: createMutate, isPending: createIsPending } = useMutation(
    {
      mutationFn: (param: CreateMenuParamType) => createMenu(param),
      onSuccess: () => {
        refetch();
        setOpenCreateDialog(false);
        toast.success(t('common.requests.success.contract_request'));
      },
      onError: () => {
        toast.error(t('common.requests.error.system'));
      },
    }
  );

  const { mutateAsync: editMutate, isPending: editIsPending } = useMutation({
    mutationFn: ({ id, param }: { id: string; param: EditMenuParamType }) =>
      editMenu(id, param),
    onSuccess: () => {
      refetch();
      setOpenDeleteDialog(false);
      setOpenEditDialog(false);
      setSelectedMenuId(null);
    },
    onError: () => {
      toast.error(t('common.requests.error.system'));
    },
  });

  // createa menu
  const handleCreateActionSubmit = (values?: MenuValuesType) => {
    if (values) {
      createMutate({
        name: String(values.name),
        price: Number(values.price),
        description: String(values.description),
        days: String(values.days),
        restaurantMenuTypeId: 0,
        externalMenuId: 0,
      });
    } else {
      toast.error(t('common.requests.error.system'));
    }
  };

  // edit menu
  const handleEditActionSubmit = (values?: MenuValuesType) => {
    if (selectedMenuId) {
      const param: { id: string; param: EditMenuParamType } = {
        id: selectedMenuId,
        param: {
          name: String(values?.name),
          price: Number(values?.price),
          description: String(values?.description),
          days: String(values?.days),
          restaurantMenuTypeId: 0,
          externalMenuId: 0,
          status: STATUS.ACTIVE,
        },
      };

      editMutate(param);
    } else {
      toast.error(t('common.requests.error.system'));
    }
  };

  const handleDeleteActionSubmit = () => {
    const status =
      menuItem?.status === STATUS.INACTIVE ? STATUS.ACTIVE : STATUS.INACTIVE;
    if (selectedMenuId) {
      editMutate({
        id: selectedMenuId,
        param: {
          status: status,
        },
      });
    } else {
      toast.error(t('common.requests.error.system'));
    }
  };

  const handleCancel = (id: number, data: EditMenuParamType) => {
    setMenuItem(data);
    setOpenDeleteDialog(true);
    setSelectedMenuId(String(id));
  };

  const handleEdit = (id: number, data: EditMenuParamType) => {
    setMenuItem(data);
    setOpenEditDialog(true);
    setSelectedMenuId(String(id));
  };

  const handleCreate = () => {
    setOpenCreateDialog(true);
  };

  const columns: ColumnDef<MenuType>[] = [
    {
      id: 'id',
      header: t('employees_manage.id'),
      cell: ({ row }) => (
        <div>{(currentPage - 1) * PAGE_SIZE + row.index + 1}</div>
      ),
    },
    {
      accessorKey: 'name',
      header: t('menus_manage.name'),
      cell: ({ row }) => <div>{row.getValue('name')}</div>,
      enableHiding: false,
    },
    {
      accessorKey: 'description',
      header: t('menus_manage.description'),
      cell: ({ row }) => <div>{row.getValue('description')}</div>,
    },
    {
      accessorKey: 'price',
      header: t('menus_manage.price'),
      cell: ({ row }) => {
        return (
          <div className="font-medium">
            {formatMoney(row.getValue('price'))}
          </div>
        );
      },
    },
    {
      accessorKey: 'days',
      header: t('menus_manage.days'),
      cell: ({ row }) => <div>{row.getValue('days')}</div>,
    },
    {
      accessorKey: 'status',
      header: t('menus_manage.status'),
      cell: ({ row }) => (
        <div>
          <Badge
            className={`w-20 justify-center ${
              row.getValue('status') === STATUS.ACTIVE
                ? 'bg-green-500'
                : 'bg-red-500'
            }`}
          >
            {row.getValue('status')}
          </Badge>
        </div>
      ),
    },
    {
      id: 'actions',
      header: t('menus_manage.actions'),
      enableHiding: false,
      cell: ({ row }) => {
        const food = row.original;
        return (
          <div className="flex flex-row justify-center">
            {row.getValue('status') === STATUS.ACTIVE ? (
              <>
                <Button
                  variant="ghost"
                  size="icon"
                  onClick={() => handleEdit(food.id, food)}
                >
                  <SquarePen className="h-5 w-5" />
                </Button>
                <div className="mx-1 mt-1 h-7 border border-gray-400" />
              </>
            ) : (
              <></>
            )}
            <Button
              variant="ghost"
              size="icon"
              onClick={() => handleCancel(food.id, food)}
            >
              <Settings className="h-5 w-5" />
            </Button>
          </div>
        );
      },
    },
  ];

  const table = useReactTable({
    data: _.get(data, 'items', []),
    columns,
    getCoreRowModel: getCoreRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
    onColumnVisibilityChange: setColumnVisibility,
    state: {
      columnVisibility,
    },
  });

  return (
    <>
      <section className="grid grid-cols-1 p-2 pb-4 md:grid-cols-2">
        <h2 className="font-extrabold">{t('menus_manage.index')}</h2>
        <div className="flex flex-col items-start space-x-3 sm:flex-row sm:items-center sm:justify-end">
          <MenuFilter value={statusFilter} />
          <CustomButton
            icon={<CheckCheck color="#ffffff" className="h-4 w-4" />}
            text={t('menus_manage.create')}
            variant="default"
            className={`mt-4 h-[40px] w-[140px] sm:mt-0 ${Colors.CREATE_GREEN} ${Colors.HOVER_GREEN}`}
            onClick={() => handleCreate()}
          />
        </div>
      </section>

      <section className="w-full">
        <Table>
          <TableHeader>
            {table.getHeaderGroups().map((headerGroup) => (
              <TableRow key={headerGroup.id} className="bg-slate-100">
                {headerGroup.headers.map((header) => {
                  return (
                    <TableHead key={header.id} className="border text-center">
                      {header.isPlaceholder
                        ? null
                        : flexRender(
                            header.column.columnDef.header,
                            header.getContext()
                          )}
                    </TableHead>
                  );
                })}
              </TableRow>
            ))}
          </TableHeader>
          <TableBody>
            {table.getRowModel().rows?.length ? (
              table.getRowModel().rows.map((row) => (
                <TableRow key={row.id} className="bg-white">
                  {row.getVisibleCells().map((cell) => (
                    <TableCell key={cell.id} className="border text-center">
                      {isLoading ? (
                        <Skeleton
                          key={row.id}
                          className="h-[30px] w-full rounded-xl"
                        />
                      ) : (
                        flexRender(
                          cell.column.columnDef.cell,
                          cell.getContext()
                        )
                      )}
                    </TableCell>
                  ))}
                </TableRow>
              ))
            ) : (
              <TableRow>
                <TableCell
                  colSpan={columns.length}
                  className="h-24 border bg-white text-center"
                >
                  <EmptyData size="large" />
                </TableCell>
              </TableRow>
            )}
          </TableBody>
        </Table>

        {table.getRowModel().rows?.length ? (
          <div className="mt-8 flex-col">
            <CustomPagination
              page={_.get(data, 'meta.currentPage')}
              totalPage={_.get(data, 'meta.totalPages')}
              loading={isLoading}
            />
          </div>
        ) : null}
        {openCreateDialog && (
          <DialogMenusCreate
            open={true}
            onSubmit={handleCreateActionSubmit}
            handleClose={() => setOpenCreateDialog(false)}
            loading={createIsPending}
          />
        )}
        {openEditDialog && (
          <DialogMenusEdit
            data={menuItem}
            open={openEditDialog === true}
            handleClose={() => setOpenEditDialog(false)}
            onSubmit={handleEditActionSubmit}
            loading={editIsPending}
          />
        )}
        {openDeleteDialog && (
          <DialogMenusDelete
            open={openDeleteDialog === true}
            handleClose={() => setOpenDeleteDialog(false)}
            onSubmit={handleDeleteActionSubmit}
            loading={editIsPending}
          />
        )}
      </section>
    </>
  );
};

export default MenusManageContainer;

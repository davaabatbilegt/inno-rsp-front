import CustomButton from '@/components/custom-button';
import { Badge } from '@/components/ui/badge';
import { Button } from '@/components/ui/button';
import {
  Command,
  CommandGroup,
  CommandList,
  CommandSeparator,
} from '@/components/ui/command';
import {
  Popover,
  PopoverContent,
  PopoverTrigger,
} from '@/components/ui/popover';
import { Separator } from '@/components/ui/separator';
import { STATUS, STATUS_LIST, StatusItem } from '@/constants/common.constants';
import { StatusType } from '@/constants/types.constants';
import { usePathname, useRouter } from '@/lib/navigation';
import { cn } from '@/lib/utils';
import _ from 'lodash';
import { CheckIcon, PlusCircleIcon } from 'lucide-react';
import { useTranslations } from 'next-intl';
import { useSearchParams } from 'next/navigation';
import { useState } from 'react';

type Props = {
  value: StatusType | undefined;
};

const MenuFilter: React.FC<Props> = (props: Props) => {
  const { value } = props;
  const router = useRouter();
  const pathname = usePathname();
  const searchParams = useSearchParams();
  const t = useTranslations();

  const currentValue =
    value === undefined ? [STATUS.ACTIVE, STATUS.INACTIVE] : [value];
  const [filter, setFilter] = useState(currentValue);

  const handleFilter = () => {
    const params = new URLSearchParams(searchParams);

    if (filter.length === 1) {
      params.set('status', filter[0]);
      router.replace(`${pathname}?${params.toString()}`);
    } else {
      params.delete('status');
      router.replace(`${pathname}?${params.toString()}`);
    }
  };

  return (
    <Popover>
      <PopoverTrigger asChild>
        <Button
          variant="outline"
          size="sm"
          className="ml-3 mt-4 h-8 border-dashed sm:mt-0"
        >
          <PlusCircleIcon className="mr-2 h-4 w-4" />
          {t('menus_manage.status')}
          {currentValue.length > 0 && (
            <>
              <Separator orientation="vertical" className="mx-2 h-4" />
              <div className="space-x-1 lg:flex">
                {STATUS_LIST.filter((option: StatusItem) =>
                  currentValue.includes(option.value)
                ).map((option: StatusItem) => (
                  <Badge
                    variant="secondary"
                    key={option.value}
                    className={`w-20 justify-center rounded-sm px-1 font-normal text-white ${
                      option.value === STATUS.ACTIVE
                        ? 'bg-green-500'
                        : 'bg-red-500'
                    }`}
                  >
                    {t(`common.status_${option.label}`)}
                  </Badge>
                ))}
              </div>
            </>
          )}
        </Button>
      </PopoverTrigger>
      <PopoverContent className="m-1 w-[150px] pb-2 pt-0" align="start">
        <Command>
          <CommandList>
            <CommandGroup>
              {STATUS_LIST.map((option: StatusItem) => {
                const isSelected = filter.includes(option.value);
                return (
                  <div
                    key={option.value}
                    onClick={() => {
                      if (isSelected) {
                        setFilter(
                          filter.length === 1
                            ? [STATUS.ACTIVE, STATUS.INACTIVE]
                            : filter.filter((value) => value !== option.value)
                        );
                      } else {
                        setFilter([...filter, option.value]);
                      }
                    }}
                    className="flex flex-row items-center space-y-3"
                  >
                    <div
                      className={cn(
                        'mr-3 mt-3 flex h-4 w-4 rounded-sm border border-primary',
                        isSelected
                          ? 'bg-primary text-primary-foreground'
                          : 'opacity-50 [&_svg]:invisible'
                      )}
                    >
                      <CheckIcon className={cn('h-4 w-4')} />
                    </div>
                    <span>{t(`common.status_${option.label}`)}</span>
                  </div>
                );
              })}
            </CommandGroup>
            {!_.isEqual(currentValue, filter) && (
              <div className="mb-1">
                <CommandSeparator className="my-1.5" />
                <CustomButton
                  text={t('common.filter')}
                  onClick={handleFilter}
                  className="h-[24px] w-full text-xs"
                />
              </div>
            )}
          </CommandList>
        </Command>
      </PopoverContent>
    </Popover>
  );
};

export default MenuFilter;

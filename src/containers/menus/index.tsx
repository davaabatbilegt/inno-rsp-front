import CardList from '@/components/card-list';
import ContractCreateDialog from '@/components/contract-dialogs/create-dialog';
import { ContractValuesType } from '@/components/contract-dialogs/validation';
import MenuCard from '@/components/menu-card';
import OrderCreateDialog from '@/components/ordering/create-dialog';
import { QUERY_KEYS } from '@/constants/query-keys';
import {
  CreateContractParamType,
  createContract,
} from '@/services/contract.service';
import { getMenus } from '@/services/menu.service';
import {
  CreateOrderDetailParamType,
  createOrderDetail,
} from '@/services/order.service';
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import { useTranslations } from 'next-intl';
import { useSearchParams } from 'next/navigation';
import { useState } from 'react';
import { toast } from 'sonner';

const PAGE_SIZE = 10;

type Props = {
  restaurant_id?: string;
};

const MenusContainer: React.FC<Props> = (props: Props) => {
  const t = useTranslations();
  const searchParams = useSearchParams();
  const queryClient = useQueryClient();

  const [selectedMenuId, setSelectedMenuId] = useState<string | null>(null);
  const [openCreateMenu, setOpenCreateMenu] = useState<boolean>(false);
  const [openCreateOrder, setOpenCreateOrder] = useState<boolean>(false);

  const currentPage = searchParams.has('page')
    ? Number(searchParams.get('page'))
    : 1;

  const { data, isLoading } = useQuery({
    queryKey: [QUERY_KEYS.MENU_LIST, props.restaurant_id, currentPage],
    queryFn: () => {
      return getMenus({
        limit: PAGE_SIZE,
        page: currentPage,
        restaurantId: props?.restaurant_id,
      });
    },
  });

  const { mutateAsync: createMenuMutate, isPending: createMenuIsPending } =
    useMutation({
      mutationFn: (param: CreateContractParamType) => createContract(param),
      onSuccess: () => {
        setOpenCreateMenu(false);
        toast.success(t('common.requests.success.contract_request'));
      },
      onError: () => {
        toast.error(t('common.requests.error.system'));
      },
    });

  const { mutateAsync: createOrderMutate, isPending: createOrderIsPending } =
    useMutation({
      mutationFn: (param: CreateOrderDetailParamType) =>
        createOrderDetail(param),
      onSuccess: () => {
        setOpenCreateOrder(false);
        queryClient.invalidateQueries({ queryKey: [QUERY_KEYS.ORDERS_LIST] });
        toast.success(t('common.requests.success.create_order'));
      },
      onError: () => {
        toast.error(t('common.requests.error.system'));
      },
    });

  const handleChangeSelectedMenuId = (value: string) => {
    setSelectedMenuId(value);
  };

  const handleActionSubmit = (v?: ContractValuesType) => {
    if (selectedMenuId && v) {
      createMenuMutate({
        restaurantId: Number(props.restaurant_id),
        menuId: Number(selectedMenuId),
        organizationStatus: 'ACCEPTED',
        menuUnitPrice: Number(v.price),
        orderLimitFoodCount: Number(v.limitCount),
        orderLimitFoodMoney: Number(v.price),
        orderLimitTime: v.limitTime,
      });
    } else {
      toast.error(t('common.requests.error.system'));
    }
  };

  const handleCreateOrder = (values: CreateOrderDetailParamType) => {
    createOrderMutate(values);
  };

  return (
    <>
      <CardList meta={data?.meta} loading={isLoading} pageSize={PAGE_SIZE}>
        <div className="mt-5 grid grid-cols-1 gap-4 p-4 sm:grid-cols-2 sm:gap-6 md:grid-cols-3 md:gap-8 lg:grid-cols-5 lg:gap-12">
          {data?.items.map((item) => (
            <MenuCard
              key={item.id}
              data={item}
              handleChangeSelectedMenuId={handleChangeSelectedMenuId}
              handleOpenCreateMenu={() => setOpenCreateMenu(true)}
              handleOpenCreateOrder={() => setOpenCreateOrder(true)}
            />
          ))}
        </div>
      </CardList>
      {selectedMenuId && (
        <>
          {openCreateMenu && (
            <ContractCreateDialog
              open={openCreateMenu}
              handleClose={() => setOpenCreateMenu(false)}
              onSubmit={handleActionSubmit}
              loading={createMenuIsPending}
            />
          )}
          {openCreateOrder && (
            <OrderCreateDialog
              open={openCreateOrder}
              handleClose={() => setOpenCreateOrder(false)}
              onSubmit={handleCreateOrder}
              loading={createOrderIsPending}
              restaurantId={props.restaurant_id}
            />
          )}
        </>
      )}
    </>
  );
};

export default MenusContainer;

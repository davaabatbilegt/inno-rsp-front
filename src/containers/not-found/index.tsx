'use client';

import { useRouter } from '@/lib/navigation';
import { useTranslations } from 'next-intl';

export default function NotFoundContainer() {
  const t = useTranslations();
  const router = useRouter();
  return (
    <div className="flex h-screen flex-col items-center justify-center">
      <h1 className="mb-4 text-4xl font-bold">{t('not_found.title')}</h1>
      <p className="mb-8 text-lg">{t('not_found.description')}</p>
      <button
        onClick={() => router.back()}
        className="focus:shadow-outline rounded bg-blue-500 px-4 py-2 font-bold text-white hover:bg-blue-600 focus:outline-none"
      >
        {t('not_found.go_back')}
      </button>
    </div>
  );
}

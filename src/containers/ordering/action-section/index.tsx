'use client';

import CustomButton from '@/components/custom-button';
import { Button } from '@/components/ui/button';
import {
  EMPTY_VALUE,
  ORDER_STATUS,
  USER_TYPES,
} from '@/constants/common.constants';
import { OrderStatusType } from '@/constants/types.constants';
import { useAuth } from '@/contexts/auth';
import { OrderDetailType } from '@/services/order.service';
import dayjs from 'dayjs';
import { Camera, Loader, QrCode, Settings, Soup } from 'lucide-react';
import { useTranslations } from 'next-intl';
import { useSearchParams } from 'next/navigation';

type Props = {
  data: OrderDetailType;
  onClick: (v: 'edit' | 'qr' | 'camera', status?: OrderStatusType) => void;
  qrLoading: boolean;
  editLoading: boolean;
};

const OrderActionSection: React.FC<Props> = (props: Props) => {
  const { data, onClick, qrLoading, editLoading } = props;
  const t = useTranslations();
  const { userData } = useAuth();
  const searchParams = useSearchParams();

  const dateParam = searchParams.has('date')
    ? String(searchParams.get('date'))
    : dayjs().format('YYYY-MM-DD');

  if (dateParam !== dayjs().format('YYYY-MM-DD')) return EMPTY_VALUE;

  if (userData?.type === USER_TYPES.RESTAURANT) {
    return (
      <div className="flex flex-row justify-center">
        {data.orderStatus === ORDER_STATUS.ARRIVED ? (
          <CustomButton
            variant="outline"
            size="default"
            text={t('orders.delivered_food')}
            icon={<Soup className="h-5 w-5" />}
            loading={editLoading}
            onClick={() => onClick('edit', ORDER_STATUS.DELIVERED)}
            className="bg-sky-500 text-white"
          />
        ) : (
          EMPTY_VALUE
        )}
      </div>
    );
  }

  if (userData?.id !== data.employeeId) return EMPTY_VALUE;

  return (
    <div className="flex flex-row justify-center">
      {data.orderStatus === ORDER_STATUS.DELIVERED && EMPTY_VALUE}
      {data.orderStatus === ORDER_STATUS.ORDERED && (
        <Button variant="ghost" size="icon" onClick={() => onClick('edit')}>
          <Settings className="h-5 w-5" />
        </Button>
      )}
      {data.orderStatus === ORDER_STATUS.ORDERED && (
        <Button
          variant="ghost"
          size="icon"
          disabled={qrLoading}
          onClick={() => onClick('qr')}
        >
          {qrLoading ? <Loader /> : <QrCode className="h-5 w-5" />}
        </Button>
      )}
      {data.orderStatus === ORDER_STATUS.ARRIVED && (
        <CustomButton
          variant="outline"
          size="default"
          text={t('orders.taken_food')}
          icon={<Soup className="h-5 w-5" />}
          loading={editLoading}
          onClick={() => onClick('edit', ORDER_STATUS.DELIVERED)}
          className="bg-sky-500 text-white"
        />
      )}
      {data.orderStatus === ORDER_STATUS.ORDERED && (
        <Button variant="ghost" size="icon" onClick={() => onClick('camera')}>
          <Camera className="h-5 w-5" />
        </Button>
      )}
    </div>
  );
};

export default OrderActionSection;

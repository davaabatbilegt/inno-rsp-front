'use client';

import CustomButton from '@/components/custom-button';
import CustomPagination from '@/components/custom-pagination';
import EmptyData from '@/components/empty-data';
import QrScanDialog from '@/components/ordering/camera-dialog';
import OrderCreateDialog from '@/components/ordering/create-dialog';
import OrderEditDialog from '@/components/ordering/edit-dialog';
import QrDialog from '@/components/ordering/qr-dialog';
import { Badge } from '@/components/ui/badge';
import { Button } from '@/components/ui/button';
import { Calendar } from '@/components/ui/calendar';
import {
  Popover,
  PopoverContent,
  PopoverTrigger,
} from '@/components/ui/popover';
import { Separator } from '@/components/ui/separator';
import { Skeleton } from '@/components/ui/skeleton';
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from '@/components/ui/table';
import { Colors } from '@/constants/color.constants';
import {
  EMPTY_VALUE,
  ORDER_STATUS,
  STATUS,
  USER_TYPES,
} from '@/constants/common.constants';
import { QUERY_KEYS } from '@/constants/query-keys';
import { OrderStatusType } from '@/constants/types.constants';
import { useAuth } from '@/contexts/auth';
import { usePathname, useRouter } from '@/lib/navigation';
import { cn, formatMoney } from '@/lib/utils';
import {
  CreateOrderDetailParamType,
  EditOrderDetailParamType,
  EditOrderParamType,
  OrderDetailType,
  OrderType,
  createOrderDetail,
  editOrder,
  editOrderDetail,
  encrypt,
  getOrdersList,
} from '@/services/order.service';
import { useMutation, useQuery } from '@tanstack/react-query';
import {
  ColumnDef,
  flexRender,
  getCoreRowModel,
  useReactTable,
} from '@tanstack/react-table';
import dayjs from 'dayjs';
import _ from 'lodash';
import { CalendarIcon, CopyPlus, QrCode } from 'lucide-react';
import { useTranslations } from 'next-intl';
import { useSearchParams } from 'next/navigation';
import { useState } from 'react';
import { toast } from 'sonner';

import OrderActionSection from './action-section';

const PAGE_SIZE = 10;

const OrderingContainer = () => {
  const t = useTranslations();

  const router = useRouter();
  const searchParams = useSearchParams();
  const pathname = usePathname();

  const { userData } = useAuth();

  const currentPage = searchParams.has('page')
    ? Number(searchParams.get('page'))
    : 1;

  const dateParam = searchParams.has('date')
    ? String(searchParams.get('date'))
    : dayjs().format('YYYY-MM-DD');

  const [date, setDate] = useState<Date>(dayjs(dateParam).toDate());
  const [selectedData, setSelectedData] = useState<{
    order: OrderType | null;
    orderDetail: OrderDetailType | null;
  }>({ order: null, orderDetail: null });
  const [createOpen, setCreateOpen] = useState<boolean>(false);
  const [editOpen, setEditOpen] = useState<boolean>(false);
  const [qrData, setQrData] = useState<string | null>(null);
  const [cameraOpen, setCameraOpen] = useState<'table' | 'order' | null>(null);

  const { data, isLoading, refetch } = useQuery({
    queryKey: [QUERY_KEYS.ORDERS_LIST, currentPage, dateParam],
    queryFn: () =>
      getOrdersList({
        orderDate: dateParam,
        isInsideContractRange: 'IN_CONTRACT',
        status: STATUS.ACTIVE,
        limit: PAGE_SIZE,
        page: currentPage,
      }),

    enabled: !!userData?.type,
  });

  const { mutateAsync: createMutate, isPending: createIsPending } = useMutation(
    {
      mutationFn: (param: CreateOrderDetailParamType) =>
        createOrderDetail(param),
      onSuccess: () => {
        setCreateOpen(false);
      },
      onError: () => {
        toast.error(t('common.requests.error.system'));
      },
    }
  );

  const { mutateAsync: editMutate, isPending: editIsPending } = useMutation({
    mutationFn: ({
      id,
      param,
    }: {
      id: string;
      param: EditOrderDetailParamType;
    }) => editOrderDetail(id, param),
    onSuccess: (data) => {
      setEditOpen(false);
      toast.success(
        data.status === STATUS.INACTIVE
          ? t('common.requests.success.delete_order')
          : t('common.requests.success.edit_order')
      );
    },
    onError: () => {
      toast.error(t('common.requests.error.system'));
    },
  });

  const { mutateAsync: editOrderMutate, isPending: editOrderIsPending } =
    useMutation({
      mutationFn: ({ id, param }: { id: string; param: EditOrderParamType }) =>
        editOrder(id, param),
    });

  const { mutateAsync: encryptMutate, isPending: encryptIsPending } =
    useMutation({
      mutationFn: (params: object) => encrypt(params),
      onSuccess: async (data) => {
        setQrData(data);
      },
      onError: () => {
        toast.error(t('common.requests.error.system'));
      },
    });

  const handleOrderFood = async (v: CreateOrderDetailParamType) => {
    await createMutate(v);
    refetch();
  };

  const handleEditOrder = async (v: EditOrderDetailParamType) => {
    await editMutate({ id: String(selectedData?.orderDetail?.id), param: v });
    refetch();
  };

  const handleDeleteOrder = async () => {
    await editOrderMutate({
      id: String(selectedData?.order?.id),
      param: { status: STATUS.INACTIVE },
    });
    await editMutate({
      id: String(selectedData?.orderDetail?.id),
      param: { status: STATUS.INACTIVE },
    });
    refetch();
  };

  const handleChangeStatus = async (param: {
    status: OrderStatusType;
    tableId?: number;
    orderId?: number;
    orderDetailId?: number;
  }) => {
    const { status, tableId, orderId, orderDetailId } = param;
    await editOrderMutate({
      id: String(orderId || selectedData?.order?.id),
      param: { orderStatus: status, ...(tableId && { tableId }) },
    });
    await editMutate({
      id: String(orderDetailId || selectedData?.orderDetail?.id),
      param: { orderStatus: status },
    });
    setCameraOpen(null);
    refetch();
  };

  const multipleCellRender = (
    value: OrderDetailType[],
    key: string,
    renderValue?: (v: string) => React.ReactElement | string
  ) => {
    const data = _.map(value, key);

    return (
      <div className="flex flex-col">
        {data.map((item, index) => (
          <div key={key + item + index} className="flex flex-col items-center">
            <div className="flex h-[40px] flex-row items-center justify-center text-center">
              {renderValue ? renderValue(item) : item}
            </div>
            {data.length - 1 !== index && <Separator className="my-1" />}
          </div>
        ))}
      </div>
    );
  };

  const columns: ColumnDef<OrderType>[] = [
    {
      accessorKey: 'id',
      header: t('orders.id'),
      cell: ({ row }) => (
        <div className="flex justify-center text-center capitalize">
          {(currentPage - 1) * PAGE_SIZE + row.index + 1}
        </div>
      ),
    },
    {
      accessorKey: 'employee',
      header: t('orders.employee_code'),
      cell: ({ getValue }) => _.get(getValue(), 'employeeCode', EMPTY_VALUE),
    },
    {
      accessorKey: 'organization',
      header: t('orders.organization_name'),
      cell: ({ row }) => _.get(row.original, 'organization.name', EMPTY_VALUE),
    },
    {
      id: 'employee_name',
      accessorKey: 'employee',
      header: t('orders.employee_name'),
      cell: ({ getValue }) =>
        _.capitalize(_.get(getValue(), 'username', EMPTY_VALUE)),
    },
    {
      accessorKey: 'restaurant',
      header: t('orders.restaurant'),
      cell: ({ getValue }) => _.get(getValue(), 'name', EMPTY_VALUE),
    },
    {
      id: 'order_menu_name',
      accessorKey: 'orderDetails',
      header: t('orders.order_name'),
      cell: ({ row }) =>
        multipleCellRender(row.getValue('order_menu_name'), 'menu.name'),
    },
    {
      id: 'tableId',
      accessorKey: 'tableId',
      header: t('orders.table_id'),
      cell: ({ row }) => row.original.tableId || EMPTY_VALUE,
    },
    {
      id: 'order_date',
      accessorKey: 'orderDetails',
      header: t('orders.order_date'),
      cell: ({ row }) => {
        const dateParser = (v: string) => {
          if (typeof v !== 'string') return EMPTY_VALUE;
          return dayjs(v).format('YYYY-MM-DD HH:mm');
        };

        return multipleCellRender(
          row.getValue('order_menu_name'),
          'createdAt',
          dateParser
        );
      },
    },
    {
      id: 'order_detail_status',
      accessorKey: 'orderDetails',
      header: t('orders.status'),
      cell: ({ row }) => {
        const statusBadgeRender = (v: string) => (
          <div className="capitalize">
            <Badge
              className={`w-24 justify-center ${
                v === ORDER_STATUS.ORDERED
                  ? 'bg-blue-500'
                  : v === ORDER_STATUS.DELIVERED
                    ? 'bg-green-500'
                    : 'bg-yellow-500'
              }`}
            >
              {v}
            </Badge>
          </div>
        );

        return multipleCellRender(
          row.getValue('order_menu_name'),
          'orderStatus',
          statusBadgeRender
        );
      },
    },
    {
      id: 'order_detail_unit_price',
      accessorKey: 'orderDetails',
      header: t('orders.unit_price'),
      cell: ({ row }) => {
        const moneyParser = (v: string) => formatMoney(v);

        return multipleCellRender(
          row.getValue('order_menu_name'),
          'unitPrice',
          moneyParser
        );
      },
    },
    {
      id: 'order_detail_unit_count',
      accessorKey: 'orderDetails',
      header: t('orders.unit_count'),
      cell: ({ row }) =>
        multipleCellRender(row.getValue('order_menu_name'), 'unitCount'),
    },
    {
      accessorKey: 'totalBill',
      header: t('orders.total_price'),
      cell: ({ row }) => formatMoney(row.getValue('totalBill')),
    },
    {
      id: 'action',
      header: t('orders.action'),
      cell: ({ row }) => {
        const orderDetails: OrderDetailType[] = row.getValue('order_menu_name');

        return orderDetails.map((orderDetail, index) => {
          const handleClickAction = (
            open: 'edit' | 'qr' | 'camera',
            status?: OrderStatusType
          ) => {
            setSelectedData({ order: row.original, orderDetail });

            switch (open) {
              case 'edit':
                if (status) {
                  handleChangeStatus({
                    status,
                    orderId: row.original.id,
                    orderDetailId: orderDetail.id,
                  });
                } else {
                  setEditOpen(true);
                }

                break;
              case 'qr':
                encryptMutate({
                  order_id: row.original.id,
                  order_detail_id: orderDetail.id,
                  restaurant_id: row.original.restaurantId,
                });
                break;
              case 'camera':
                setCameraOpen('table');
                break;
              default:
                return;
            }
          };

          return (
            <div key={orderDetail.id} className="flex flex-col items-center">
              <OrderActionSection
                data={orderDetail}
                onClick={handleClickAction}
                qrLoading={
                  encryptIsPending &&
                  orderDetail.id === selectedData?.orderDetail?.id
                }
                editLoading={editIsPending || editOrderIsPending}
              />
              {orderDetails.length - 1 !== index && (
                <Separator className="my-1" />
              )}
            </div>
          );
        });
      },
    },
  ];

  const table = useReactTable({
    data: _.get(data, 'items', []),
    columns,
    getCoreRowModel: getCoreRowModel(),
    initialState: {
      columnVisibility: {
        employee_name: pathname.split('/')[1] === USER_TYPES.EMPLOYEE,
        restaurant: pathname.split('/')[1] !== USER_TYPES.RESTAURANT,
        action: pathname.split('/')[1] !== USER_TYPES.ORGANIZATION,
        organization: pathname.split('/')[1] === USER_TYPES.RESTAURANT,
      },
    },
  });

  return (
    <>
      <section className="mb-4 grid grid-cols-1 p-2 sm:grid-cols-2">
        <p className="text-xl font-bold">{t('orders.order_title')}</p>
        <div
          className="flex flex-row justify-start
         gap-4 sm:justify-end"
        >
          <Popover>
            <PopoverTrigger asChild>
              <Button
                variant={'outline'}
                className={cn(
                  'h-[40px] w-[140px] justify-start text-left font-normal',
                  !dateParam && 'text-muted-foreground'
                )}
              >
                <CalendarIcon className="mr-2 h-4 w-4" />
                {dateParam ? dateParam : <span>Pick a date</span>}
              </Button>
            </PopoverTrigger>
            <PopoverContent className="w-auto p-0">
              <Calendar
                mode="single"
                selected={date}
                // @ts-ignore
                onSelect={(v: Date) => {
                  setDate(v);
                  router.replace(
                    `${pathname}?page=${currentPage}&date=${dayjs(v).format('YYYY-MM-DD')}`
                  );
                }}
                toDate={new Date()}
                initialFocus
              />
            </PopoverContent>
          </Popover>
          {userData?.type === USER_TYPES.EMPLOYEE && (
            <CustomButton
              icon={<CopyPlus color="#ffffff" className="mr-2 h-[40px] w-4" />}
              text={t('orders.order_food')}
              onClick={() => setCreateOpen(true)}
              className={`${Colors.CREATE_GREEN} ${Colors.HOVER_GREEN}`}
            />
          )}
          {userData?.type === USER_TYPES.RESTAURANT && (
            <CustomButton
              icon={<QrCode color="#ffffff" className="mr-2 h-[40px] w-4" />}
              text={t('orders.scan_qr')}
              onClick={() => setCameraOpen('order')}
              className={`${Colors.CREATE_GREEN} ${Colors.HOVER_GREEN}`}
            />
          )}
        </div>
      </section>

      <section className="w-full">
        <Table>
          <TableHeader>
            {table.getHeaderGroups().map((headerGroup) => (
              <TableRow key={headerGroup.id} className="bg-slate-100">
                {headerGroup.headers.map((header) => {
                  return (
                    <TableHead key={header.id} className="border text-center">
                      {header.isPlaceholder
                        ? null
                        : flexRender(
                            header.column.columnDef.header,
                            header.getContext()
                          )}
                    </TableHead>
                  );
                })}
              </TableRow>
            ))}
          </TableHeader>
          <TableBody>
            {table.getRowModel().rows?.length ? (
              table.getRowModel().rows.map((row) => {
                const rowData = row.original;
                return (
                  <TableRow
                    key={row.id}
                    className={`bg-white ${userData?.type === USER_TYPES.EMPLOYEE && userData?.id === rowData.employeeId ? 'bg-slate-100' : userData?.type === USER_TYPES.RESTAURANT && rowData.orderStatus === ORDER_STATUS.ARRIVED ? 'bg-slate-100' : 'bg-white'}`}
                  >
                    {row.getVisibleCells().map((cell) => (
                      <TableCell key={cell.id} className="border text-center">
                        {isLoading ? (
                          <Skeleton
                            key={row.id}
                            className="h-[30px] w-full rounded-xl"
                          />
                        ) : (
                          flexRender(
                            cell.column.columnDef.cell,
                            cell.getContext()
                          )
                        )}
                      </TableCell>
                    ))}
                  </TableRow>
                );
              })
            ) : (
              <TableRow>
                <TableCell
                  colSpan={columns.length}
                  className="h-24 border bg-white text-center"
                >
                  <EmptyData size="large" />
                </TableCell>
              </TableRow>
            )}
          </TableBody>
        </Table>
        {table.getRowModel().rows?.length ? (
          <div className="mt-8 flex-col">
            <CustomPagination
              page={_.get(data, 'meta.currentPage')}
              totalPage={_.get(data, 'meta.totalPages')}
              loading={isLoading}
            />
          </div>
        ) : null}
      </section>
      {createOpen && (
        <OrderCreateDialog
          open={createOpen}
          handleClose={() => setCreateOpen(false)}
          onSubmit={handleOrderFood}
          loading={createIsPending}
        />
      )}
      {editOpen && selectedData.orderDetail && (
        <OrderEditDialog
          open={editOpen}
          handleClose={() => setEditOpen(false)}
          onEdit={handleEditOrder}
          onDelete={handleDeleteOrder}
          loading={editIsPending || editOrderIsPending}
          data={selectedData.orderDetail}
        />
      )}
      {!!qrData && (
        <QrDialog
          open={!!qrData}
          handleClose={() => setQrData(null)}
          data={qrData}
        />
      )}
      {!!cameraOpen && (
        <QrScanDialog
          open={cameraOpen}
          handleClose={() => setCameraOpen(null)}
          data={selectedData.orderDetail}
          onSubmit={handleChangeStatus}
        />
      )}
    </>
  );
};
export default OrderingContainer;

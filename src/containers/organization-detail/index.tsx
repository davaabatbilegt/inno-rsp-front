'use client';

import { Badge } from '@/components/ui/badge';
import { Separator } from '@/components/ui/separator';
import { Skeleton } from '@/components/ui/skeleton';
import { STATUS } from '@/constants/common.constants';
import { QUERY_KEYS } from '@/constants/query-keys';
import { getImage } from '@/lib/utils';
import { getOrganizationDetail } from '@/services/organization.service';
import { useQuery } from '@tanstack/react-query';
import { FlagTriangleRight, MapPin, ReceiptText, Users } from 'lucide-react';
import { useTranslations } from 'next-intl';
import Image from 'next/image';

type Props = { id: string };

const OrganizationDetailContainer: React.FC<Props> = (props: Props) => {
  const { id } = props;
  const t = useTranslations();

  const { data, isLoading } = useQuery({
    queryKey: [QUERY_KEYS.ORGANIZATION_LIST, id],
    queryFn: () => getOrganizationDetail(id),
  });

  return (
    <div className="flex flex-1 justify-center">
      <div className="mt-5 grid max-w-screen-lg overflow-hidden rounded p-10 shadow-lg transition lg:grid-cols-2">
        {isLoading ? (
          <Skeleton className="h-[200px] w-[200px] rounded-xl sm:h-[400px] sm:w-[400px]" />
        ) : (
          <>
            <div className="relative ml-8 h-[200px] w-[200px] max-w-md rounded sm:h-[400px] sm:w-[400px]">
              <Image
                alt="restaurant-image"
                src={getImage(data?.username)}
                fill
                className="object-cover"
              />
            </div>
            <div className="flex flex-1 flex-col px-10">
              <div className="mb-5">
                <div className="mb-1 text-2xl font-bold">{data?.name}</div>
                <div className="text-sm">{data?.description}</div>
              </div>
              <Separator orientation="horizontal" className="mb-5" />
              <div className="mb-3 flex flex-row items-center">
                <div className="mr-4 flex h-10 w-10 items-center justify-center rounded-md bg-gray-200">
                  <MapPin className="h-5 w-5" />
                </div>
                <div>
                  <div className="text-sm text-slate-500">
                    {t('profile.address')}
                  </div>
                  <div className="text-sm">{data?.address}</div>
                </div>
              </div>
              <div className="mb-3 flex flex-row items-center">
                <div className="mr-4 flex h-10 w-10 items-center justify-center rounded-md bg-gray-200">
                  <Users className="h-5 w-5" />
                </div>
                <div>
                  <div className="text-sm text-slate-500">
                    {t('profile.employee_count')}
                  </div>
                  <div className="text-sm">{data?.employeeCount}</div>
                </div>
              </div>
              <div className="mb-4 flex flex-row items-center">
                <div className="mr-4 flex h-10 w-10 items-center justify-center rounded-md bg-gray-200">
                  <FlagTriangleRight className="h-5 w-5" />
                </div>
                <div>
                  <div className="text-sm text-slate-500">
                    {t('profile.status')}
                  </div>
                  <Badge
                    className={`w-20 justify-center ${
                      data?.status === STATUS.ACTIVE
                        ? 'h-4 bg-green-500'
                        : 'h-4 bg-red-500'
                    }`}
                  >
                    {data?.status}
                  </Badge>
                </div>
              </div>
              <div className="mb-3 flex flex-row items-center">
                <div className="mr-4 flex h-10 w-10 items-center justify-center rounded-md bg-gray-200">
                  <ReceiptText className="h-5 w-5" />
                </div>
                <div>
                  <div className="text-sm text-slate-500">
                    {t('profile.contracted')}
                  </div>
                  <div className="text-sm">{data?.contracted}</div>
                </div>
              </div>
            </div>
          </>
        )}
      </div>
    </div>
  );
};

export default OrganizationDetailContainer;

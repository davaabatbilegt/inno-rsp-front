'use client';

import CardList from '@/components/card-list';
import OrganizationCard from '@/components/organization-card';
import { GetOrganizationListResponse } from '@/services/organization.service';

type Props = {
  data: GetOrganizationListResponse;
  loading: boolean;
  handleOpenCreateContract: () => void;
  handleSelectOrganizationId: (v: number) => void;
};

const OrganizationsCardListContainer: React.FC<Props> = (props: Props) => {
  const {
    data,
    loading,
    handleOpenCreateContract,
    handleSelectOrganizationId,
  } = props;

  return (
    <div className="flex-col">
      <CardList
        meta={data?.meta}
        loading={loading}
        pageSize={data?.meta?.itemsPerPage}
      >
        <div className="mt-5 grid grid-cols-1 gap-4 p-4 sm:grid-cols-2 sm:gap-6 md:grid-cols-3 md:gap-8 lg:grid-cols-5 lg:gap-8">
          {data?.items?.map((item) => (
            <OrganizationCard
              key={item.id}
              data={item}
              handleOpenCreateContract={handleOpenCreateContract}
              handleSelectOrganizationId={handleSelectOrganizationId}
            />
          ))}
        </div>
      </CardList>
    </div>
  );
};

export default OrganizationsCardListContainer;

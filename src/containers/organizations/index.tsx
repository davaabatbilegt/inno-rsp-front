'use client';

import ContractCreateDialog from '@/components/contract-dialogs/create-dialog';
import { ContractValuesType } from '@/components/contract-dialogs/validation';
import { Tabs, TabsList, TabsTrigger } from '@/components/ui/tabs';
import { QUERY_KEYS } from '@/constants/query-keys';
import { useAuth } from '@/contexts/auth';
import {
  CreateContractParamType,
  createContract,
} from '@/services/contract.service';
import { getOrganizationsList } from '@/services/organization.service';
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import { useTranslations } from 'next-intl';
import { useSearchParams } from 'next/navigation';
import { useState } from 'react';
import { toast } from 'sonner';

import OrganizationsCardListContainer from './card-list';

const PAGE_SIZE = 10;

const OrganizationsContainer: React.FC = () => {
  const t = useTranslations();
  const searchParams = useSearchParams();
  const queryClient = useQueryClient();

  const { userData } = useAuth();

  const [option, setOption] = useState<'all' | 'contracted' | 'non-contracted'>(
    'all'
  );
  const [selectedOrganizationId, setSelectedOrganizationId] = useState<
    number | undefined
  >(undefined);
  const [openCreateContract, setOpenCreateContract] = useState<boolean>(false);

  const currentPage = searchParams.has('page')
    ? Number(searchParams.get('page'))
    : 1;

  const { data, isLoading } = useQuery({
    queryKey: [QUERY_KEYS.ORGANIZATION_LIST, currentPage, option],
    queryFn: () =>
      getOrganizationsList({
        limit: PAGE_SIZE,
        page: currentPage,
        contracted:
          option === 'all' ? undefined : option === 'contracted' ? true : false,
      }),
  });

  const {
    mutateAsync: createContractMutate,
    isPending: createContractIsPending,
  } = useMutation({
    mutationFn: (param: CreateContractParamType) => createContract(param),
    onSuccess: () => {
      setOpenCreateContract(false);
      queryClient.invalidateQueries({
        queryKey: [QUERY_KEYS.CONTRACT_LIST_REQUESTING],
      });
      toast.success(t('common.requests.success.contract_request'));
    },
    onError: () => {
      toast.error(t('common.requests.error.system'));
    },
  });

  const handleSelectOrganizationId = (v: number) => {
    setSelectedOrganizationId(v);
  };

  const handleCreateContract = (v: ContractValuesType) => {
    if (selectedOrganizationId) {
      createContractMutate({
        organizationId: selectedOrganizationId,
        menuId: Number(v.menuId),
        restaurantStatus: 'ACCEPTED',
        menuUnitPrice: Number(v.price),
        orderLimitFoodCount: Number(v.limitCount),
        orderLimitFoodMoney: Number(v.price),
        orderLimitTime: v.limitTime,
      });
    } else {
      toast.error(t('common.requests.error.system'));
    }
  };

  return (
    <>
      <Tabs value={option} className="overflow-hidden">
        <TabsList>
          <TabsTrigger value="all" onClick={() => setOption('all')}>
            {t('organizations.all_list_tab')}
          </TabsTrigger>
          <TabsTrigger
            value="contracted"
            onClick={() => setOption('contracted')}
          >
            {t('organizations.contracted_list_tab')}
          </TabsTrigger>
          <TabsTrigger
            value="non-contracted"
            onClick={() => setOption('non-contracted')}
          >
            {t('organizations.non_contracted_list_tab')}
          </TabsTrigger>
        </TabsList>
        <div className="mt-4">
          {data && (
            <OrganizationsCardListContainer
              data={data}
              loading={isLoading}
              handleOpenCreateContract={() => setOpenCreateContract(true)}
              handleSelectOrganizationId={handleSelectOrganizationId}
            />
          )}
        </div>
      </Tabs>

      {openCreateContract && selectedOrganizationId && (
        <ContractCreateDialog
          open={openCreateContract}
          handleClose={() => setOpenCreateContract(false)}
          onSubmit={handleCreateContract}
          loading={createContractIsPending}
          restaurantId={userData?.id}
        />
      )}
    </>
  );
};

export default OrganizationsContainer;

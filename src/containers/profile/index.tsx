import { USER_TYPES, UserTypesType } from '@/constants/common.constants';
import { NextPage } from 'next';

import OrganizationProfile from './organization';
import RestaurantProfile from './restaurant';
import UserProfile from './user';

type Props = {
  type: UserTypesType;
};
const ProfileContainer: NextPage<Props> = (props: Props) => {
  const { type } = props;
  if (type === USER_TYPES.ORGANIZATION) {
    return <OrganizationProfile />;
  } else if (type === USER_TYPES.RESTAURANT) {
    return <RestaurantProfile />;
  }
  return <UserProfile />;
};
export default ProfileContainer;

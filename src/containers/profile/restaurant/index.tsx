import ChangePasswordForm from '@/components/change-password';
import CustomButton from '@/components/custom-button';
import GoogleMap from '@/components/google-map';
import { Badge } from '@/components/ui/badge';
import { Button } from '@/components/ui/button';
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from '@/components/ui/form';
import { Input } from '@/components/ui/input';
import { RadioGroup, RadioGroupItem } from '@/components/ui/radio-group';
import { Separator } from '@/components/ui/separator';
import { Colors } from '@/constants/color.constants';
import { STATUS } from '@/constants/common.constants';
import { useAuth } from '@/contexts/auth';
import { getImage } from '@/lib/utils';
import { ProfileParamType, editProfile } from '@/services/auth.service';
import { useMutation } from '@tanstack/react-query';
import { FlagTriangleRight, MapPin, Pencil, Users } from 'lucide-react';
import { NextPage } from 'next';
import { useTranslations } from 'next-intl';
import Image from 'next/image';
import { useEffect, useState } from 'react';
import { toast } from 'sonner';
import { z } from 'zod';

import './validation';
import { useRestaurantProfileValidation } from './validation';

const OrganizationProfile: NextPage = () => {
  const t = useTranslations();
  const [isEditing, setIsEditing] = useState(false);
  const [changePassword, setChangePassword] = useState(false);
  const { userData, refetchUserInfo } = useAuth();

  const { mutateAsync, isPending } = useMutation({
    mutationFn: (param: ProfileParamType) => editProfile(param),
    onSuccess: () => {
      toast.success(t('common.requests.success.edit_restaurant_profile'));
      refetchUserInfo();
      setIsEditing(false);
    },
    onError: () => {
      toast.error(t('common.requests.error.system'));
    },
  });

  const { form, schema, reset } = useRestaurantProfileValidation();

  useEffect(() => {
    reset({
      name: userData?.name,
      username: userData?.username,
      status: userData?.status,
      description: userData?.description,
      address: userData?.address,
      capacity: userData?.capacity ? userData?.capacity : 0,
      latitude: userData?.latitude ? userData?.latitude : 0,
      longitude: userData?.longitude ? userData?.longitude : 0,
    });
  }, [userData, reset]);

  const onSubmit = (values: z.infer<typeof schema>) => {
    if (userData?.id) {
      mutateAsync({
        id: userData?.id,
        type: 'restaurants',
        params: values,
      });
    } else {
      toast.error(t('common.requests.error.system'));
    }
  };

  const renderInfo = () => {
    if (!isEditing && userData) {
      return (
        <div className="flex flex-1 flex-col px-10">
          <div className="mb-5">
            <div className="mb-1 text-2xl font-bold">{userData?.name}</div>
            <div className="text-sm">{userData?.description}</div>
          </div>
          <Separator orientation="horizontal" className="mb-5" />
          <div className="mb-3 flex flex-row items-center">
            <div className="mr-4 flex h-10 w-10 items-center justify-center rounded-md bg-gray-200">
              <Pencil className="h-5 w-5" />
            </div>
            <div>
              <div className="text-sm text-slate-500">
                {t('profile.username')}
              </div>
              <div className="text-sm">{userData?.username}</div>
            </div>
          </div>
          <div className="mb-3 flex flex-row items-center">
            <div className="mr-4 flex h-10 w-10 items-center justify-center rounded-md bg-gray-200">
              <MapPin className="h-5 w-5" />
            </div>
            <div>
              <div className="text-sm text-slate-500">
                {t('profile.address')}
              </div>
              <div className="text-sm">{userData?.address}</div>
            </div>
          </div>
          <div className="mb-3 flex flex-row items-center">
            <div className="mr-4 flex h-10 w-10 items-center justify-center rounded-md bg-gray-200">
              <Users className="h-5 w-5" />
            </div>
            <div>
              <div className="text-sm text-slate-500">
                {t('profile.capacity')}
              </div>
              <div className="text-sm">
                {userData?.capacity ? userData?.capacity : 0}
              </div>
            </div>
          </div>
          <div className="mb-4 flex flex-row items-center">
            <div className="mr-4 flex h-10 w-10 items-center justify-center rounded-md bg-gray-200">
              <FlagTriangleRight className="h-5 w-5" />
            </div>
            <div>
              <div className="text-sm text-slate-500">
                {t('profile.status')}
              </div>
              <Badge
                className={`w-20 justify-center ${
                  userData?.status === STATUS.ACTIVE
                    ? 'h-4 bg-green-500'
                    : 'h-4 bg-red-500'
                }`}
              >
                {userData?.status}
              </Badge>
            </div>
          </div>
          <div className=" flex flex-1 flex-col justify-end">
            <CustomButton
              text={t('profile.edit')}
              type="button"
              variant="outline"
              className="w-full"
              onClick={() => {
                setIsEditing(true), reset();
              }}
            />
            <Button
              type="reset"
              className="mt-1 w-full"
              variant={'ghost'}
              onClick={() => setChangePassword(true)}
            >
              {t('profile.change_password')}
            </Button>
          </div>
        </div>
      );
    }
  };
  const renderForm = () => {
    if (isEditing) {
      return (
        <div className="flex flex-1 flex-col px-10">
          <Form {...form}>
            <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-2">
              <FormField
                control={form.control}
                name="name"
                render={({ field }) => (
                  <FormItem className="w-full">
                    <FormLabel>{t('profile.name')}</FormLabel>
                    <Input
                      className=" text-slate-500"
                      placeholder={t('profile.name')}
                      {...field}
                    />
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="username"
                render={({ field }) => (
                  <FormItem className="w-full">
                    <FormLabel>{t('profile.username')}</FormLabel>
                    <Input
                      className=" text-slate-500"
                      placeholder={t('profile.username')}
                      {...field}
                    />
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="description"
                render={({ field }) => (
                  <FormItem className="w-full">
                    <FormLabel>{t('profile.description')}</FormLabel>
                    <Input
                      className=" text-slate-500"
                      placeholder={t('profile.description')}
                      {...field}
                    />
                    <FormMessage />
                  </FormItem>
                )}
              />
              <FormField
                control={form.control}
                name="address"
                render={({ field }) => (
                  <FormItem className="w-full">
                    <FormLabel>{t('profile.address')}</FormLabel>
                    <Input
                      className=" text-slate-500"
                      placeholder={t('profile.address')}
                      {...field}
                    />
                    <FormMessage />
                  </FormItem>
                )}
              />
              <div className="flex flex-1 justify-between">
                <FormField
                  control={form.control}
                  name="latitude"
                  render={({ field }) => (
                    <FormItem className="w-36">
                      <FormLabel>{t('profile.latitude')}</FormLabel>
                      <Input
                        className="text-slate-500"
                        placeholder={t('profile.latitude')}
                        type="number"
                        value={field.value ?? ''}
                        onChange={(e) =>
                          field.onChange(
                            e.target.value === ''
                              ? 0
                              : parseFloat(e.target.value)
                          )
                        }
                      />
                      <FormMessage />
                    </FormItem>
                  )}
                />
                <FormField
                  control={form.control}
                  name="longitude"
                  render={({ field }) => (
                    <FormItem className="w-36">
                      <FormLabel>{t('profile.longitude')}</FormLabel>
                      <Input
                        className="text-slate-500"
                        placeholder={t('profile.longitude')}
                        type="number"
                        value={field.value ?? ''}
                        onChange={(e) =>
                          field.onChange(
                            e.target.value === ''
                              ? 0
                              : parseFloat(e.target.value)
                          )
                        }
                      />
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>
              <div className="flex flex-row">
                <FormField
                  control={form.control}
                  name="capacity"
                  render={({ field }) => (
                    <FormItem className="w-36">
                      <FormLabel>{t('profile.capacity')}</FormLabel>
                      <Input
                        className="text-slate-500"
                        placeholder={t('profile.capacity')}
                        type="number"
                        value={field.value ?? ''}
                        onChange={(e) =>
                          field.onChange(
                            e.target.value === ''
                              ? 0
                              : parseFloat(e.target.value)
                          )
                        }
                      />
                      <FormMessage />
                    </FormItem>
                  )}
                />
                <FormField
                  control={form.control}
                  name="status"
                  render={({ field }) => (
                    <FormItem className="flex w-1/2 items-end justify-center">
                      <RadioGroup
                        onValueChange={field.onChange}
                        value={field.value}
                        className=" items-start justify-evenly"
                      >
                        <FormItem className="mb-2 flex items-center space-x-3 space-y-0">
                          <FormControl>
                            <RadioGroupItem value={STATUS.ACTIVE} />
                          </FormControl>
                          <FormLabel className="font-normal">
                            {t('profile.active')}
                          </FormLabel>
                        </FormItem>
                        <FormItem className="flex items-center space-x-3 space-y-0">
                          <FormControl>
                            <RadioGroupItem value={STATUS.INACTIVE} />
                          </FormControl>
                          <FormLabel className="font-normal">
                            {t('profile.inactive')}
                          </FormLabel>
                        </FormItem>
                      </RadioGroup>
                      <FormMessage />
                    </FormItem>
                  )}
                />
              </div>

              <div className="flex w-full justify-end pt-5">
                <Button
                  type="reset"
                  className="mr-5 w-full"
                  variant={'outline'}
                  onClick={() => setIsEditing(false)}
                >
                  {t('profile.cancel')}
                </Button>
                <CustomButton
                  text={t('profile.submit')}
                  type="submit"
                  className={`w-full ${Colors.BLUE} ${Colors.HOVER_BLUE}`}
                  loading={isPending}
                />
              </div>
            </form>
          </Form>
        </div>
      );
    }
  };

  return (
    <div className="flex justify-center">
      <div className="grid w-screen grid-cols-[100%] rounded p-10 shadow-lg transition md:grid-cols-[60%_40%]">
        <div className="flex flex-col md:flex-row">
          <div className="relative ml-8 h-[200px] w-[200px] max-w-md rounded sm:h-[400px] sm:w-[400px]">
            <Image
              alt="restaurant-image"
              src={getImage(userData?.username)}
              fill
              className="object-cover"
            />
          </div>
          <div className="w-full bg-stone-100 px-2 py-5 md:w-2/3">
            <ChangePasswordForm
              open={changePassword}
              onClose={() => setChangePassword(false)}
            />
            {renderInfo()}
            {renderForm()}
          </div>
        </div>
        <div className="h-[400px] rounded md:h-full">
          <GoogleMap
            type={userData?.username}
            lat={userData?.latitude ?? 0}
            lng={userData?.longitude ?? 0}
          />
        </div>
      </div>
    </div>
  );
};
export default OrganizationProfile;

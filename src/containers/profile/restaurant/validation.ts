'use client';

import { zodResolver } from '@hookform/resolvers/zod';
import { useTranslations } from 'next-intl';
import { useForm } from 'react-hook-form';
import { z } from 'zod';

const initialValues = {
  name: '',
  username: '',
  status: '',
  description: '',
  address: '',
  capacity: 0,
  latitude: 0,
  longitude: 0,
};

export const useRestaurantProfileValidation = () => {
  const t = useTranslations();

  const schema = z.object({
    name: z.string().min(1, {
      message: t('profile.validation.name_required'),
    }),
    username: z
      .string()
      .min(1, { message: t('profile.validation.username_required') }),
    capacity: z.number().optional(),
    latitude: z.number().optional(),
    longitude: z.number().optional(),
    description: z
      .string()
      .min(1, { message: t('profile.validation.description_required') }),
    address: z
      .string()
      .min(1, { message: t('profile.validation.address_required') }),
    status: z.string().optional(),
  });

  const form = useForm<z.infer<typeof schema>>({
    resolver: zodResolver(schema),
    defaultValues: initialValues,
  });

  const { reset } = form;
  return { schema, form, reset };
};

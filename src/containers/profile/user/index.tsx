import CustomButton from '@/components/custom-button';
import { Badge } from '@/components/ui/badge';
import { Button } from '@/components/ui/button';
import { Separator } from '@/components/ui/separator';
import { STATUS, USER_TYPES } from '@/constants/common.constants';
import { useAuth } from '@/contexts/auth';
import { Code, FlagTriangleRight, Pencil } from 'lucide-react';
import { NextPage } from 'next';
import { useTranslations } from 'next-intl';
import Image from 'next/image';

const UserProfile: NextPage = () => {
  const t = useTranslations();
  const { userData } = useAuth();

  const renderInfo = () => {
    return (
      <div className="flex flex-1 flex-col px-10">
        <div className="mb-5">
          {userData?.type === USER_TYPES.EMPLOYEE ? (
            <div>
              <div className="mb-1 text-2xl font-bold">
                {userData?.user?.name}
              </div>
              <div className="text-sm">{userData?.organization?.name}</div>
            </div>
          ) : (
            <div className="mb-1 text-2xl font-bold">{userData?.name}</div>
          )}
        </div>
        <Separator orientation="horizontal" className="mb-5" />
        {userData?.type === USER_TYPES.EMPLOYEE ? (
          <div className="mb-3 flex flex-row items-center">
            <div className="mr-4 flex h-10 w-10 items-center justify-center rounded-md bg-gray-200">
              <Code className="h-5 w-5" />
            </div>
            <div>
              <div className="text-sm text-slate-500">
                {t('profile.employee_code')}
              </div>
              <div className="text-sm">{userData?.employeeCode}</div>
            </div>
          </div>
        ) : (
          <></>
        )}
        <div className="mb-3 flex flex-row items-center">
          <div className="mr-4 flex h-10 w-10 items-center justify-center rounded-md bg-gray-200">
            <Pencil className="h-5 w-5" />
          </div>
          <div>
            <div className="text-sm text-slate-500">
              {t('profile.username')}
            </div>
            <div className="text-sm">{userData?.username}</div>
          </div>
        </div>
        <div className="mb-4 flex flex-row items-center">
          <div className="mr-4 flex h-10 w-10 items-center justify-center rounded-md bg-gray-200">
            <FlagTriangleRight className="h-5 w-5" />
          </div>
          <div>
            <div className="text-sm text-slate-500">{t('profile.status')}</div>
            <Badge
              className={`w-20 justify-center ${
                userData?.status === STATUS.ACTIVE
                  ? 'h-4 bg-green-500'
                  : 'h-4 bg-red-500'
              }`}
            >
              {userData?.status}
            </Badge>
          </div>
        </div>
        <div className=" flex flex-1 flex-col justify-end">
          <CustomButton
            text={t('profile.edit')}
            type="button"
            variant="outline"
            className="w-full"
          />
          <Button type="reset" className="mt-1 w-full" variant={'ghost'}>
            {t('profile.change_password')}
          </Button>
        </div>
      </div>
    );
  };
  return (
    <>
      <div className="flex justify-center">
        <div className="w-screen-xl mt-5 grid overflow-hidden rounded p-10 shadow-lg transition xl:grid-cols-2">
          <div className="relative ml-8 h-[200px] w-[200px] max-w-md rounded sm:h-[400px] sm:w-[400px]">
            <Image
              alt="employee-image"
              src="/images/unimedia_logo.png"
              fill
              className="object-cover"
            />
          </div>
          {renderInfo()}
        </div>
      </div>
    </>
  );
};
export default UserProfile;

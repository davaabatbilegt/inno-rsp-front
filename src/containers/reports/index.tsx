'use client';

import { USER_TYPES, UserTypesType } from '@/constants/common.constants';
import { NextPage } from 'next';

import ReportOrganizationContainer from './organization';
import ReportRestaurantContainer from './restaurant';

type Props = {
  type: UserTypesType;
};
const ReportContainer: NextPage<Props> = (props: Props) => {
  const { type } = props;
  if (type === USER_TYPES.ORGANIZATION) {
    return <ReportOrganizationContainer />;
  } else {
    return <ReportRestaurantContainer />;
  }
};
export default ReportContainer;

'use client';

import EmptyData from '@/components/empty-data';
import { ReportType } from '@/services/report.service';
import dayjs from 'dayjs';
import _ from 'lodash';
import React from 'react';
import {
  CartesianGrid,
  Legend,
  Line,
  LineChart,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from 'recharts';

type Props = {
  data?: ReportType[];
};

const ReportOrganizationAnalyticContainer: React.FC<Props> = (props: Props) => {
  const { data = [] } = props;

  const calculatedData = Object.values(
    data.reduce((acc: { [key: string]: ReportType }, item) => {
      const orderDate = dayjs(item.order_date).format('YYYY-MM-DD');
      acc[orderDate] = acc[orderDate] || {
        order_date: orderDate,
        ordered: 0,
        taken: 0,
      };
      acc[orderDate].ordered += parseInt(item.ordered);
      acc[orderDate].taken += parseInt(item.taken);
      return acc;
    }, {})
  );

  return (
    <div className="mt-8">
      {_.isEmpty(data) ? (
        <EmptyData size="large" />
      ) : (
        <ResponsiveContainer width="100%" height={500}>
          <LineChart width={500} height={300} data={calculatedData}>
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis
              dataKey="order_date"
              tickFormatter={(v) => dayjs(v).format('MM-DD')}
              tickMargin={10}
            />
            <YAxis type="number" allowDecimals={false} tickMargin={10} />
            <Tooltip />
            <Legend />
            <Line
              type="monotone"
              dataKey="ordered"
              stroke="#8884d8"
              activeDot={{ r: 8 }}
            />
            <Line type="monotone" dataKey="taken" stroke="#82ca9d" />
          </LineChart>
        </ResponsiveContainer>
      )}
    </div>
  );
};
export default ReportOrganizationAnalyticContainer;

'use client';

import { Input } from '@/components/ui/input';
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from '@/components/ui/select';
import { Tabs, TabsList, TabsTrigger } from '@/components/ui/tabs';
import { RestaurantItem } from '@/constants/common.constants';
import { QUERY_KEYS } from '@/constants/query-keys';
import { getReportsList } from '@/services/report.service';
import {
  RestaurantType,
  getRestaurantsList,
} from '@/services/restaurant.service';
import { useQuery } from '@tanstack/react-query';
import dayjs from 'dayjs';
import _ from 'lodash';
import { useTranslations } from 'next-intl';
import { usePathname, useRouter, useSearchParams } from 'next/navigation';
import React, { useEffect, useState } from 'react';
import { toast } from 'sonner';

import ReportOrganizationAnalyticContainer from './analytic';
import ReportOrganizationTableContainer from './table';

const ReportOrganizationContainer: React.FC = () => {
  const t = useTranslations();
  const router = useRouter();
  const pathname = usePathname();
  const [restaurantSelectList, setRestaurantSelectList] = useState<
    RestaurantItem[]
  >([{ id: 0, name: t('reports.all') }]);
  const [selectedRestaurant, setSelectedRestaurant] = useState<string>('0');
  const [isTable, setIsTable] = useState<boolean>(true);

  const searchParams = useSearchParams();

  useEffect(() => {
    const fetchRestaurants = async () => {
      try {
        const response = await getRestaurantsList({ contracted: true });
        response.items.forEach((item: RestaurantType) => {
          setRestaurantSelectList((prevList) => {
            if (!prevList.find((restaurant) => restaurant.id === item.id)) {
              return [...prevList, { name: item.name, id: item.id }];
            }
            return prevList;
          });
        });
      } catch (error) {
        toast.error(t('common.requests.error.system'));
      }
    };
    fetchRestaurants();
  }, []);

  const selected = searchParams.has('restaurant')
    ? searchParams.get('restaurant')
    : undefined;

  const targetMonth = searchParams.has('targetMonth')
    ? dayjs(searchParams.get('targetMonth'))
    : undefined;

  const { data, isLoading } = useQuery({
    queryKey: [QUERY_KEYS.REPORT_LIST, targetMonth, selected],
    queryFn: () => {
      return getReportsList({
        restaurantId:
          selectedRestaurant !== '0' ? parseInt(selectedRestaurant) : undefined,
        targetMonth: dayjs(targetMonth).format('YYYY-MM-DD'),
      });
    },
  });

  const groupedData = Object.values(
    _.groupBy(data || [], (obj) => dayjs(obj.order_date).format('YYYY-MM-DD'))
  );

  return (
    <div>
      <section className="flex justify-between p-2 pb-4">
        <Tabs defaultValue="table">
          <TabsList>
            <TabsTrigger value="table" onClick={() => setIsTable(true)}>
              {t('reports.table')}
            </TabsTrigger>
            <TabsTrigger value="analytic" onClick={() => setIsTable(false)}>
              {t('reports.analytic')}
            </TabsTrigger>
          </TabsList>
        </Tabs>
        <div className="grid grid-cols-1 flex-row items-center space-x-3 sm:grid-cols-2">
          <Select
            onValueChange={(value) => {
              setSelectedRestaurant(value);
              const params = new URLSearchParams(searchParams);
              params.set('restaurant', value);
              router.replace(`${pathname}?${params.toString()}`);
            }}
            value={selectedRestaurant}
          >
            <SelectTrigger className="mb-4 ml-4 h-8 w-full sm:mb-0 sm:w-48">
              <SelectValue />
            </SelectTrigger>
            <SelectContent>
              {restaurantSelectList.map((restaurant: RestaurantItem) => (
                <SelectItem
                  key={restaurant.id}
                  value={restaurant.id.toString()}
                >
                  {restaurant.name}
                </SelectItem>
              ))}
            </SelectContent>
          </Select>
          <Input
            type="month"
            defaultValue={dayjs().format('YYYY-MM')}
            className="w-42 h-8"
            onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
              if (event.target.value) {
                const params = new URLSearchParams(searchParams);
                params.set('targetMonth', event.target.value);
                router.replace(`${pathname}?${params.toString()}`);
              } else {
                const params = new URLSearchParams(searchParams);
                params.delete('targetMonth');
                router.replace(`${pathname}?${params.toString()}`);
              }
            }}
          />
        </div>
      </section>
      <section className="w-full">
        {isTable ? (
          <ReportOrganizationTableContainer
            data={groupedData}
            isLoading={isLoading}
          />
        ) : (
          <ReportOrganizationAnalyticContainer data={data} />
        )}
      </section>
      <div />
    </div>
  );
};

export default ReportOrganizationContainer;

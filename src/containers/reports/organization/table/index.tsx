'use client';

import EmptyData from '@/components/empty-data';
import { Separator } from '@/components/ui/separator';
import { Skeleton } from '@/components/ui/skeleton';
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from '@/components/ui/table';
import { ReportType } from '@/services/report.service';
import {
  ColumnDef,
  flexRender,
  getCoreRowModel,
  useReactTable,
} from '@tanstack/react-table';
import dayjs from 'dayjs';
import _ from 'lodash';
import { useTranslations } from 'next-intl';
import React from 'react';

type Props = {
  data?: ReportType[][];
  isLoading: boolean;
};

const ReportOrganizationTableContainer: React.FC<Props> = (props: Props) => {
  const { data = [], isLoading } = props;
  const t = useTranslations();

  const flattenData: ReportType[] = _.flatten(data);

  const orderedSum = flattenData?.reduce(
    (acc, curr) => acc + parseInt(curr.ordered),
    0
  );
  const takenSum = flattenData?.reduce(
    (acc, curr) => acc + parseInt(curr.taken),
    0
  );
  const uniqueRestaurants = new Set(flattenData?.map((item) => item.r_name));
  const totalRestaurants = uniqueRestaurants.size;
  const uniqueMenus = new Set(flattenData?.map((item) => item.rm_name));
  const totalMenus = uniqueMenus.size;
  const uniqueDays = new Set(flattenData?.map((item) => item.order_date));
  const totalDays = uniqueDays.size;

  const multipleCellRender = (
    value: ReportType[],
    key: string,
    renderValue?: (v: string) => React.ReactElement | string
  ) => {
    const data = _.map(value, key);

    return (
      <div className="flex flex-col">
        {data.map((item, index) => (
          <div key={key + item + index} className="flex flex-col items-center">
            <div className="flex h-[40px] flex-row items-center justify-center text-center">
              {renderValue ? renderValue(item) : item}
            </div>
            {data.length - 1 !== index && <Separator className="my-1" />}
          </div>
        ))}
      </div>
    );
  };

  const columns: ColumnDef<ReportType[]>[] = [
    {
      id: 'id',
      header: t('reports.id'),
      cell: ({ row }) => <div>{row.index + 1}</div>,
    },
    {
      id: 'order_date',
      header: t('reports.order_date'),
      cell: ({ row }) =>
        multipleCellRender(row.original, 'order_date', (v) =>
          dayjs(v).format('YYYY-MM-DD')
        ),
    },
    {
      id: 'r_name',
      header: t('reports.r_name'),
      cell: ({ row }) => multipleCellRender(row.original, 'r_name'),
    },
    {
      id: 'rm_name',
      header: t('reports.rm_name'),
      cell: ({ row }) => multipleCellRender(row.original, 'rm_name'),
    },
    {
      id: 'ordered',
      header: t('reports.ordered'),
      cell: ({ row }) => multipleCellRender(row.original, 'ordered'),
    },
    {
      id: 'taken',
      header: t('reports.taken'),
      cell: ({ row }) => multipleCellRender(row.original, 'taken'),
    },
  ];

  const table = useReactTable({
    data: data || [],
    columns,
    getCoreRowModel: getCoreRowModel(),
  });

  return (
    <Table>
      <TableHeader>
        {table.getHeaderGroups().map((headerGroup) => (
          <TableRow key={headerGroup.id} className="bg-slate-100">
            {headerGroup.headers.map((header) => {
              return (
                <TableHead key={header.id} className="border text-center">
                  {header.isPlaceholder
                    ? null
                    : flexRender(
                        header.column.columnDef.header,
                        header.getContext()
                      )}
                </TableHead>
              );
            })}
          </TableRow>
        ))}
      </TableHeader>
      <TableBody>
        {table.getRowModel().rows?.length ? (
          <>
            {table.getRowModel().rows.map((row) => (
              <TableRow key={row.id} className="bg-white">
                {row.getVisibleCells().map((cell) => (
                  <TableCell key={cell.id} className="border text-center">
                    {isLoading ? (
                      <Skeleton
                        key={row.id}
                        className="h-[30px] w-full rounded-xl"
                      />
                    ) : (
                      flexRender(cell.column.columnDef.cell, cell.getContext())
                    )}
                  </TableCell>
                ))}
              </TableRow>
            ))}
            <TableRow className="bg-slate-300">
              <TableCell className="border text-center">
                {t('reports.total')}
              </TableCell>
              <TableCell className="border text-center">{totalDays}</TableCell>
              <TableCell className="border text-center">
                {totalRestaurants}
              </TableCell>
              <TableCell className="border text-center">{totalMenus}</TableCell>
              <TableCell className="border text-center">{orderedSum}</TableCell>
              <TableCell className="border text-center">{takenSum}</TableCell>
            </TableRow>
          </>
        ) : (
          <TableRow>
            <TableCell
              colSpan={columns.length}
              className="h-24 border bg-white text-center"
            >
              <EmptyData size="large" />
            </TableCell>
          </TableRow>
        )}
      </TableBody>
    </Table>
  );
};

export default ReportOrganizationTableContainer;

import { Tabs, TabsContent, TabsList, TabsTrigger } from '@/components/ui/tabs';
import { USER_TYPES, UserTypesType } from '@/constants/common.constants';
import { useTranslations } from 'next-intl';

import ContractsMenuListContainer from '../contracts-menu';
import MenusContainer from '../menus';
import RestaurantProfileContainer from '../restaurant-profile';

type Props = {
  id: string;
  type: UserTypesType;
};

const RestaurantDetailContainer: React.FC<Props> = (props: Props) => {
  const t = useTranslations();

  return (
    <Tabs defaultValue="profile">
      <TabsList>
        <TabsTrigger value="profile">
          {t('restaurant_detail.profile_tab')}
        </TabsTrigger>
        <TabsTrigger value="menu-list">
          {t('restaurant_detail.menu_list_tab')}
        </TabsTrigger>
      </TabsList>
      <TabsContent value="profile">
        <RestaurantProfileContainer restaurant_id={props.id} />
      </TabsContent>
      <TabsContent value="menu-list">
        {props.type === USER_TYPES.EMPLOYEE ? (
          <ContractsMenuListContainer restaurant_id={props.id} />
        ) : (
          <MenusContainer restaurant_id={props.id} />
        )}
      </TabsContent>
    </Tabs>
  );
};

export default RestaurantDetailContainer;

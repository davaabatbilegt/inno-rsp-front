import GoogleMap from '@/components/google-map';
import Rating from '@/components/rating';
import { Badge } from '@/components/ui/badge';
import { Separator } from '@/components/ui/separator';
import { STATUS } from '@/constants/common.constants';
import { QUERY_KEYS } from '@/constants/query-keys';
import { getImage } from '@/lib/utils';
import { getRestaurantDetail } from '@/services/restaurant.service';
import { useQuery } from '@tanstack/react-query';
import {
  FlagTriangleRight,
  MapPin,
  ReceiptText,
  TrendingUp,
  Users,
} from 'lucide-react';
import { useTranslations } from 'next-intl';
import Image from 'next/image';

type Props = {
  restaurant_id: string;
};

const RestaurantProfileContainer: React.FC<Props> = (props: Props) => {
  const t = useTranslations();
  const { restaurant_id } = props;

  const { data } = useQuery({
    queryKey: [QUERY_KEYS.RESTAURANT_DETAIL, restaurant_id],
    queryFn: () => getRestaurantDetail(restaurant_id),
  });

  return (
    <div className="flex justify-center">
      <div className="grid w-screen grid-cols-[100%] rounded p-10 shadow-lg transition md:grid-cols-[60%_40%]">
        <div className="flex flex-col md:flex-row">
          <div className="relative ml-8 h-[200px] w-[200px] max-w-md rounded sm:h-[400px] sm:w-[400px]">
            <Image
              alt="restaurant-image"
              src={getImage(data?.username)}
              fill
              className="object-cover"
            />
          </div>
          <div className="w-full bg-stone-100 px-2 py-5 md:w-1/2">
            <div className="w-full bg-stone-100 px-8 py-5 md:w-2/3">
              <div className="mb-5">
                <div className="mb-1 text-2xl font-bold">{data?.name}</div>
                <div className="text-sm">{data?.description}</div>
              </div>
              <Separator orientation="horizontal" className="mb-5" />
              <div className="mb-3 flex flex-row items-center">
                <div className="mr-4 flex h-10 w-10 items-center justify-center rounded-md bg-gray-200">
                  <MapPin className="h-5 w-5" />
                </div>
                <div>
                  <div className="text-sm text-slate-500">
                    {t('profile.address')}
                  </div>
                  <div className="text-sm">{data?.address}</div>
                </div>
              </div>
              <div className="mb-4 flex flex-row items-center">
                <div className="mr-4 flex h-10 w-10 items-center justify-center rounded-md bg-gray-200">
                  <TrendingUp className="h-5 w-5" />
                </div>
                <div>
                  <div className="text-sm text-slate-500">
                    {t('profile.rating')}
                  </div>
                  <div>
                    {typeof data?.rating === 'number' ? (
                      <Rating
                        rating={data?.rating}
                        restaurantId={restaurant_id}
                      />
                    ) : (
                      t('restaurants.unrated')
                    )}
                  </div>
                </div>
              </div>
              <div className="mb-4 flex flex-row items-center">
                <div className="mr-4 flex h-10 w-10 items-center justify-center rounded-md bg-gray-200">
                  <FlagTriangleRight className="h-5 w-5" />
                </div>
                <div>
                  <div className="text-sm text-slate-500">
                    {t('profile.status')}
                  </div>
                  <Badge
                    className={`w-20 justify-center ${
                      data?.status === STATUS.ACTIVE
                        ? 'h-4 bg-green-500'
                        : 'h-4 bg-red-500'
                    }`}
                  >
                    {data?.status}
                  </Badge>
                </div>
              </div>

              <div className="mb-4 flex flex-row items-center">
                <div className="mr-4 flex h-10 w-10 items-center justify-center rounded-md bg-gray-200">
                  <Users className="h-5 w-5" />
                </div>
                <div>
                  <div className="text-sm text-slate-500">
                    {t('profile.capacity')}
                  </div>
                  <div className="text-sm">
                    {data?.capacity ? data?.capacity : 0}
                  </div>
                </div>
              </div>
              <div className="mb-4 flex flex-row items-center">
                <div className="mr-4 flex h-10 w-10 items-center justify-center rounded-md bg-gray-200">
                  <ReceiptText className="h-5 w-5" />
                </div>
                <div>
                  <div className="text-sm text-slate-500">
                    {t('profile.contracted')}
                  </div>
                  <div className="text-sm">{data?.contracted}</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="h-[400px] rounded md:h-full">
          <GoogleMap
            type={data?.username}
            lat={data?.latitude ?? 0}
            lng={data?.longitude ?? 0}
          />
        </div>
      </div>
    </div>
  );
};

export default RestaurantProfileContainer;

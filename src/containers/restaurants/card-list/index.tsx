'use client';

import CardList from '@/components/card-list';
import RestaurantCard from '@/components/restaurant-card';
import { GetRestaurantListResponse } from '@/services/restaurant.service';

type Props = {
  data: GetRestaurantListResponse;
  loading: boolean;
  handleOpenCreateContract: () => void;
  handleSelectRestaurantId: (v: number) => void;
};

const RestaurantsCardListContainer: React.FC<Props> = (props: Props) => {
  const { data, loading, handleOpenCreateContract, handleSelectRestaurantId } =
    props;

  return (
    <div className="flex-col">
      <CardList
        meta={data?.meta}
        loading={loading}
        pageSize={data?.meta?.itemsPerPage}
      >
        <div className="mt-5 grid grid-cols-1 gap-4 p-4 sm:grid-cols-2 sm:gap-6 md:grid-cols-3 md:gap-8 lg:grid-cols-5 lg:gap-8">
          {data?.items?.map((item) => (
            <RestaurantCard
              key={item.id}
              data={item}
              handleOpenCreateContract={handleOpenCreateContract}
              handleSelectRestaurantId={handleSelectRestaurantId}
            />
          ))}
        </div>
      </CardList>
    </div>
  );
};

export default RestaurantsCardListContainer;

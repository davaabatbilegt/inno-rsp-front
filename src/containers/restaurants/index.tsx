'use client';

import ContractCreateDialog from '@/components/contract-dialogs/create-dialog';
import { ContractValuesType } from '@/components/contract-dialogs/validation';
import { Tabs, TabsList, TabsTrigger } from '@/components/ui/tabs';
import { USER_TYPES, UserTypesType } from '@/constants/common.constants';
import { QUERY_KEYS } from '@/constants/query-keys';
import {
  CreateContractParamType,
  createContract,
} from '@/services/contract.service';
import { getRestaurantsList } from '@/services/restaurant.service';
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import { useTranslations } from 'next-intl';
import { useSearchParams } from 'next/navigation';
import { useState } from 'react';
import { toast } from 'sonner';

import RestaurantsCardListContainer from './card-list';

const PAGE_SIZE = 10;

type Props = { type: UserTypesType };

const RestaurantsContainer: React.FC<Props> = (props: Props) => {
  const t = useTranslations();
  const searchParams = useSearchParams();
  const queryClient = useQueryClient();

  const [option, setOption] = useState<'all' | 'contracted' | 'non-contracted'>(
    props.type === USER_TYPES.EMPLOYEE ? 'contracted' : 'all'
  );
  const [selectedRestaurantId, setSelectedRestaurantId] = useState<
    number | undefined
  >(undefined);
  const [openCreateContract, setOpenCreateContract] = useState<boolean>(false);

  const currentPage = searchParams.has('page')
    ? Number(searchParams.get('page'))
    : 1;

  const { data, isLoading } = useQuery({
    queryKey: [QUERY_KEYS.RESTAURANT_LIST, currentPage, option],
    queryFn: () =>
      getRestaurantsList({
        limit: PAGE_SIZE,
        page: currentPage,
        contracted:
          option === 'all' ? undefined : option === 'contracted' ? true : false,
      }),
  });

  const {
    mutateAsync: createContractMutate,
    isPending: createContractIsPending,
  } = useMutation({
    mutationFn: (param: CreateContractParamType) => createContract(param),
    onSuccess: () => {
      setOpenCreateContract(false);
      queryClient.invalidateQueries({
        queryKey: [QUERY_KEYS.CONTRACT_LIST_REQUESTING],
      });
      toast.success(t('common.requests.success.contract_request'));
    },
    onError: () => {
      toast.error(t('common.requests.error.system'));
    },
  });

  const handleSelectRestaurantId = (v: number) => {
    setSelectedRestaurantId(v);
  };

  const handleCreateContract = (v: ContractValuesType) => {
    if (selectedRestaurantId) {
      createContractMutate({
        restaurantId: selectedRestaurantId,
        menuId: Number(v.menuId),
        organizationStatus: 'ACCEPTED',
        menuUnitPrice: Number(v.price),
        orderLimitFoodCount: Number(v.limitCount),
        orderLimitFoodMoney: Number(v.price),
        orderLimitTime: v.limitTime,
      });
    } else {
      toast.error(t('common.requests.error.system'));
    }
  };

  if (props.type === USER_TYPES.EMPLOYEE || props.type === USER_TYPES.USER) {
    return (
      data && (
        <RestaurantsCardListContainer
          data={data}
          loading={isLoading}
          handleOpenCreateContract={() => setOpenCreateContract(true)}
          handleSelectRestaurantId={handleSelectRestaurantId}
        />
      )
    );
  }

  return (
    <>
      <Tabs value={option} className="overflow-hidden">
        <TabsList>
          <TabsTrigger value="all" onClick={() => setOption('all')}>
            {t('restaurants.all_list_tab')}
          </TabsTrigger>
          <TabsTrigger
            value="contracted"
            onClick={() => setOption('contracted')}
          >
            {t('restaurants.contracted_list_tab')}
          </TabsTrigger>
          <TabsTrigger
            value="non-contracted"
            onClick={() => setOption('non-contracted')}
          >
            {t('restaurants.non_contracted_list_tab')}
          </TabsTrigger>
        </TabsList>
        <div className="mt-4">
          {data && (
            <RestaurantsCardListContainer
              data={data}
              loading={isLoading}
              handleOpenCreateContract={() => setOpenCreateContract(true)}
              handleSelectRestaurantId={handleSelectRestaurantId}
            />
          )}
        </div>
      </Tabs>
      {openCreateContract && selectedRestaurantId && (
        <ContractCreateDialog
          open={openCreateContract}
          handleClose={() => setOpenCreateContract(false)}
          onSubmit={handleCreateContract}
          loading={createContractIsPending}
          restaurantId={selectedRestaurantId}
        />
      )}
    </>
  );
};

export default RestaurantsContainer;

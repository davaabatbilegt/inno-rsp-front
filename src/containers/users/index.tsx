'use client';

import { useTranslations } from 'next-intl';

const UsersContainer: React.FC = () => {
  const t = useTranslations();

  return <>{t('users.index')}</>;
};

export default UsersContainer;

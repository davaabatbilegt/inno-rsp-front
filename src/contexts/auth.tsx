'use client';

import {
  CONTRACT_STATUS,
  COOKIE_KEYS,
  STATUS,
  USER_TYPES,
  UserTypesType,
} from '@/constants/common.constants';
import { QUERY_KEYS } from '@/constants/query-keys';
import { getLoginRoute, useRouter } from '@/lib/navigation';
import { GetProfileResponseType, getProfile } from '@/services/auth.service';
import {
  GetContractsResponseType,
  getContractsList,
} from '@/services/contract.service';
import {
  GetOrderDetailsResponseType,
  getOrderDetailsList,
} from '@/services/order.service';
import { UseQueryResult, useQuery } from '@tanstack/react-query';
import { deleteCookie } from 'cookies-next';
import dayjs from 'dayjs';
import {
  Dispatch,
  SetStateAction,
  createContext,
  useContext,
  useEffect,
  useState,
} from 'react';

export type AuthContextType = {
  userData: GetProfileResponseType;
  orderedData: GetOrderDetailsResponseType;
  contractedData: GetContractsResponseType;
  isAuthenticated: boolean;
  isLoading: boolean;
  triggerFullLoading: boolean;
  clearUserInfo: () => void;
  logout: (type: UserTypesType) => Promise<void>;
  setIsAuthenticated: Dispatch<SetStateAction<boolean>>;
  setIsLoading: Dispatch<SetStateAction<boolean>>;
  refetchUserInfo: () => Promise<UseQueryResult>;
  refetchCorporatePoint: () => Promise<UseQueryResult>;
};

const AuthContext = createContext<AuthContextType | { isLoading: boolean }>({
  isLoading: true,
});

export const AuthContextProvider = ({
  children,
}: {
  children: React.ReactNode;
}): JSX.Element => {
  const router = useRouter();

  const [isAuthenticated, setIsAuthenticated] = useState<boolean>(false);
  const [isLoading, setIsLoading] = useState<boolean>(true);

  const clearUserInfo = (): void => {
    setIsAuthenticated(false);
  };

  const logout = async (type: UserTypesType): Promise<void> => {
    setIsLoading(true);
    await deleteCookie(COOKIE_KEYS.ACCESS_TOKEN);
    await deleteCookie(COOKIE_KEYS.TYPE);
    await router.push(getLoginRoute(type));
    setIsAuthenticated(false);
    setIsLoading(false);
  };

  const {
    data: userData,
    refetch: refetchUserInfo,
    isLoading: userDataIsLoading,
    status: userDataStatus,
  } = useQuery({
    queryKey: [QUERY_KEYS.USER_DATA],
    queryFn: () => getProfile(),
    enabled: isAuthenticated,
  });

  const { data: orderedData } = useQuery({
    queryKey: [QUERY_KEYS.EMPLOYEE_ORDERED_DETAILS_LIST],
    queryFn: () =>
      getOrderDetailsList({
        employeeId: userData?.id,
        orderDate: dayjs().format('YYYY-MM-DD'),
        status: STATUS.ACTIVE,
        page: 1,
        limit: 200,
      }),
    enabled: isAuthenticated && userData?.type === USER_TYPES.EMPLOYEE,
  });

  const { data: contractedData } = useQuery({
    queryKey: [QUERY_KEYS.CONTRACTED_LIST],
    queryFn: () =>
      getContractsList({
        restaurantId:
          userData?.type === USER_TYPES.RESTAURANT ? userData?.id : undefined,
        organizationId:
          userData?.type === USER_TYPES.ORGANIZATION ? userData?.id : undefined,
        organizationContractStatus: CONTRACT_STATUS.ACCEPTED,
        restaurantContractStatus: CONTRACT_STATUS.ACCEPTED,
        status: STATUS.ACTIVE,
        page: 1,
        limit: 200,
      }),
    enabled:
      isAuthenticated &&
      (userData?.type === USER_TYPES.RESTAURANT ||
        userData?.type === USER_TYPES.ORGANIZATION),
  });

  useEffect(() => {
    if (userDataStatus === 'success') {
      setIsAuthenticated(true);
    }
    if (userDataStatus === 'error' || userDataStatus === 'success')
      setIsLoading(false);
  }, [userDataStatus]);

  const triggerFullLoading = isLoading || userDataIsLoading;

  return (
    <AuthContext.Provider
      value={{
        userData,
        orderedData,
        contractedData,
        isAuthenticated,
        isLoading,
        triggerFullLoading,
        clearUserInfo,
        logout,
        setIsAuthenticated,
        setIsLoading,
        refetchUserInfo,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = (): AuthContextType => {
  const context = useContext(AuthContext);
  if (context === undefined) {
    throw new Error('useAuth must be used within AuthContextProvider');
  }
  return context as AuthContextType;
};

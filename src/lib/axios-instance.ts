import { COOKIE_KEYS } from '@/constants/common.constants';
import { ROUTES } from '@/constants/routes.constants';
import { BASE_URI } from '@/constants/uri.constants';
import axios from 'axios';
import { getCookie } from 'cookies-next';

const api = axios.create({
  baseURL: BASE_URI,
  withCredentials: true,
});

const getToken = () => {
  if (typeof window === 'undefined') return undefined;
  return getCookie(COOKIE_KEYS.ACCESS_TOKEN);
};

api.interceptors.request.use(
  (config) => {
    const token = getToken();
    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

api.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    if (
      error?.response?.data?.statusCode === 401 &&
      !window.location.pathname.includes('/login')
    ) {
      window.location.href = `${ROUTES.ORGANIZATION_LOGIN}`;
    }
    return Promise.reject(error);
  }
);

export default api;

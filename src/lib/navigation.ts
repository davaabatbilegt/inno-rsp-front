import {
  LOCALES_LIST,
  USER_TYPES,
  UserTypesType,
} from '@/constants/common.constants';
import { NAV_GUARD } from '@/constants/navigation.constants';
import { PUBLIC_ROUTES } from '@/constants/routes.constants';
import { createSharedPathnamesNavigation } from 'next-intl/navigation';

export const locales = LOCALES_LIST;
export const localePrefix = 'never';

export const { Link, redirect, usePathname, useRouter } =
  createSharedPathnamesNavigation({ locales, localePrefix });

export const getHomeRoute = (type: UserTypesType): string =>
  `/${type}${NAV_GUARD[type][0]}`;

export const getLoginRoute = (type: UserTypesType): string => {
  let loginRoute = PUBLIC_ROUTES.EMPLOYEE_LOGIN;
  if (type === USER_TYPES.ORGANIZATION)
    loginRoute = PUBLIC_ROUTES.ORGANIZATION_LOGIN;
  if (type === USER_TYPES.RESTAURANT)
    loginRoute = PUBLIC_ROUTES.RESTAURANT_LOGIN;

  return loginRoute;
};

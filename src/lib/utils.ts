import IMAGES from '@/constants/images.constants';
import { type ClassValue, clsx } from 'clsx';
import dayjs from 'dayjs';
import { twMerge } from 'tailwind-merge';

export function cn(...inputs: ClassValue[]) {
  return twMerge(clsx(inputs));
}

export function extractGeneralPath(inputPath: string) {
  const regex = /^\/[^/]+(\/.*)?/;
  const match = inputPath.match(regex);
  return match ? match[1] : inputPath;
}

export function calculateDistance(
  lat1: number,
  lon1: number,
  lat2: number,
  lon2: number
) {
  const R = 6371;
  const dLat = (lat2 - lat1) * (Math.PI / 180);
  const dLon = (lon2 - lon1) * (Math.PI / 180);
  const a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(lat1 * (Math.PI / 180)) *
      Math.cos(lat2 * (Math.PI / 180)) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2);
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  const distance = R * c;
  return distance;
}

export function getImage(name: string | undefined) {
  let imageName;
  switch (name) {
    case 'cosy':
      imageName = IMAGES.COSY;
      break;
    case 'ikhmongol':
      imageName = IMAGES.IKH_MONGOL;
      break;
    case 'um':
    case 'ums':
      imageName = IMAGES.UNIMEDIA;
      break;
    default:
      imageName = '';
      break;
  }
  return imageName;
}

export function formatMoney(value: string | number): string {
  return `${value.toLocaleString()}₮`;
}

export function calculateInTime(value: string | null): boolean {
  if (!value) return false;
  const currentTime = new Date().toLocaleTimeString('en-US', { hour12: false });
  const currentTimeObj = new Date(`2000-01-01 ${currentTime}`);
  const limitTimeObj = new Date(`2000-01-01 ${value}`);
  return limitTimeObj > currentTimeObj;
}

export function orderableByDate(value: string): boolean {
  const DATA = {
    ALL: 0,
    MONDAY: 1,
    TUESDAY: 2,
    WEDNESDAY: 3,
    THURSDAY: 4,
    FRIDAY: 5,
    SATURDAY: 6,
    SUNDAY: 7,
  };

  if (!Object.keys(DATA).includes(value)) return false;
  if (value === 'ALL') return true;
  if (DATA[value as keyof typeof DATA] === dayjs().day()) return true;

  return false;
}

import createIntlMiddleware from 'next-intl/middleware';
import { NextResponse } from 'next/server';
import type { NextRequest } from 'next/server';

import {
  COOKIE_KEYS,
  LOCALES,
  LOCALES_LIST,
  UserTypesType,
} from './constants/common.constants';
import { PRIVATE_ROUTES, PUBLIC_ROUTES } from './constants/routes.constants';
import { getHomeRoute, getLoginRoute } from './lib/navigation';

export default async function middleware(
  request: NextRequest
): Promise<NextResponse<unknown>> {
  const handleI18nRouting = createIntlMiddleware({
    locales: LOCALES_LIST,
    defaultLocale: LOCALES.EN,
    localeDetection: false,
  });
  const response = handleI18nRouting(request);
  const pathname = request.nextUrl.pathname.replace(/^\/[^/]+/, '');
  const typeFromPathname = pathname.split('/')[1] as UserTypesType;
  const type = request.cookies.get(COOKIE_KEYS.TYPE) as {
    name: 'type';
    value: UserTypesType;
  };

  response.headers.set(
    'x-default-locale',
    request.headers.get('x-default-locale') || LOCALES.EN
  );

  if (
    request.cookies.has(COOKIE_KEYS.ACCESS_TOKEN) &&
    Object.values(PUBLIC_ROUTES).includes(pathname)
  ) {
    return NextResponse.redirect(
      new URL(getHomeRoute(type?.value ?? typeFromPathname), request.url)
    );
  }

  if (
    !request.cookies.has(COOKIE_KEYS.ACCESS_TOKEN) &&
    Object.values(PRIVATE_ROUTES).includes(pathname)
  ) {
    return NextResponse.redirect(
      new URL(getLoginRoute(type?.value ?? typeFromPathname), request.url)
    );
  }

  return response;
}

export const config = {
  matcher: ['/((?!api|_next|.*\\..*).*)'],
};

import { StatusType } from '@/constants/types.constants';
import { URI } from '@/constants/uri.constants';
import api from '@/lib/axios-instance';

import { OrganizationType } from './organization.service';

export type LoginParamType = {
  username: string;
  password: string;
  type: 'admin' | 'restaurant' | 'organization' | 'employee' | 'user';
};

type LoginResponseType = {
  access_token: string;
};

type UserType = {
  id: number;
  name: string;
  status: StatusType;
  username: string;
  password: string;
  createdAt: string;
  updatedAt: string;
};

export type GetProfileResponseType = {
  id: number;
  name: string;
  username: string;
  type: string;
  employeeCode: string | null;
  organization: OrganizationType | null;
  user: UserType | null;
  password: string;
  description: string;
  address: string;
  status: StatusType;
  employeeCount?: number;
  salaryMonthStartDay?: number;
  latitude?: number;
  longitude?: number;
  capacity?: number;
};

export type ProfileParamType = {
  id: number;
  type: string;
  params: {
    name?: string;
    username?: string;
    description?: string;
    address?: string;
    employeeCount?: number;
    status?: string;
    password?: string;
    latitude?: number;
    longitude?: number;
    capacity?: number;
  };
};

export const login = async (
  params: LoginParamType
): Promise<LoginResponseType> => {
  const response = await api.post(URI.LOGIN, params);
  return response.data;
};

export const getProfile = async (): Promise<GetProfileResponseType> => {
  const response = await api.get(URI.GET_PROFILE);
  return response.data;
};

export const editProfile = async (
  params: ProfileParamType
): Promise<GetProfileResponseType> => {
  const response = await api.patch(
    `/${params.type}/${params.id}`,
    params.params
  );
  return response.data;
};

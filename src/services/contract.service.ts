import {
  ContractStatusType,
  LinksType,
  MetaType,
  SortOrderType,
  StatusType,
} from '@/constants/types.constants';
import { URI } from '@/constants/uri.constants';
import api from '@/lib/axios-instance';

import { MenuType } from './menu.service';
import { OrganizationType } from './organization.service';
import { RestaurantType } from './restaurant.service';

export type ContractType = {
  id: number;
  restaurantId: number;
  organizationId: number;
  restaurantStatus: ContractStatusType;
  organizationStatus: ContractStatusType;
  menuId: number;
  menuUnitPrice: number;
  orderLimitTime: string | null;
  orderLimitFoodCount: number;
  orderLimitFoodMoney: number;
  menu: MenuType;
  organization: OrganizationType;
  restaurant: RestaurantType;
  createdAt: string;
  updatedAt: string;
};

type GetContractsParamType = {
  restaurantId?: number;
  organizationId?: number;
  sortOrder?: SortOrderType;
  limit?: number;
  page?: number;
  restaurantContractStatus?: ContractStatusType;
  organizationContractStatus?: ContractStatusType;
  status?: StatusType;
  menuDays?: number;
};

export type GetContractsResponseType = {
  items: ContractType[];
  links: LinksType;
  meta: MetaType;
};

export type CreateContractParamType = {
  restaurantId?: number;
  menuId: number;
  organizationId?: number;
  restaurantStatus?: ContractStatusType;
  organizationStatus?: ContractStatusType;
  menuUnitPrice: number;
  orderLimitTime: string;
  orderLimitFoodCount: number;
  orderLimitFoodMoney: number;
};

type CreateContractResponseType = ContractType;

export type EditContractParamType = {
  restaurantId?: number;
  menuId?: number;
  restaurantStatus?: ContractStatusType;
  organizationStatus?: ContractStatusType;
  organizationId?: number;
  menuUnitPrice?: number;
  orderLimitTime?: string;
  orderLimitFoodCount?: number;
  orderLimitFoodMoney?: number;
  status?: StatusType;
};

type EditContractResponseType = ContractType;

export const getContractsList = async (
  params: GetContractsParamType
): Promise<GetContractsResponseType> => {
  const response = await api.get(URI.GET_CONTRACTS_LIST, { params });
  return response.data;
};

export const createContract = async (
  params: CreateContractParamType
): Promise<CreateContractResponseType> => {
  const response = await api.post(URI.CREATE_CONTRACT, params);
  return response.data;
};

export const editContract = async (
  id: string,
  params: EditContractParamType
): Promise<EditContractResponseType> => {
  const response = await api.patch(
    URI.EDIT_CONTRACT.replace(':id', id),
    params
  );
  return response.data;
};

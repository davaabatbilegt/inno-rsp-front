import { SortOrderType } from '@/constants/types.constants';
import { URI } from '@/constants/uri.constants';
import api from '@/lib/axios-instance';

export type GetEmployeeReportsParamType = {
  targetMonth: string;
  sortOrder?: SortOrderType;
};

export type EmployeeReportType = {
  employee_code: string;
  employee_id: number;
  ordered: string;
  taken: string;
};

export type GetEmployeeReportsResponseType = EmployeeReportType[];

export const getEmployeeReportsList = async (
  params: GetEmployeeReportsParamType
): Promise<GetEmployeeReportsResponseType> => {
  const response = await api.get(`${URI.GET_REPORTS_LIST}/employee`, {
    params,
  });
  return response.data;
};

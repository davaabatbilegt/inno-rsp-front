import {
  BillStatusType,
  LinksType,
  MetaType,
  SortOrderType,
  StatusType,
} from '@/constants/types.constants';
import { URI } from '@/constants/uri.constants';
import api from '@/lib/axios-instance';

import { OrganizationType } from './organization.service';
import { RestaurantType } from './restaurant.service';

export type CreateInvoiceParamType = {
  organizationId: string;
  yearMonth: string;
};

export type GetInvoicesParamType = {
  restaurantId?: number;
  organizationId?: number;
  sortOrder?: SortOrderType;
  limit: number;
  page: number;
  status?: StatusType;
  billStatus?: BillStatusType;
  yearMonth?: string;
};

export type InvoiceEditParamType = {
  id: number;
  params: {
    billStatus?: BillStatusType;
  };
};

export type InvoiceType = {
  id: number;
  restaurantId: number;
  restaurant: RestaurantType;
  organizationId: number;
  organization: OrganizationType;
  yearMonth: Date;
  status: StatusType;
  billStatus: BillStatusType;
  totalMoney: number;
  createdAt: string;
  updatedAt: string;
};

export type GetInvoicesResponseType = {
  items: InvoiceType[];
  links: LinksType;
  meta: MetaType;
};

export const getInvoicesList = async (
  params: GetInvoicesParamType
): Promise<GetInvoicesResponseType> => {
  const response = await api.get(URI.GET_INVOICES_LIST, { params });
  return response.data;
};

export const createInvoice = async (
  params: CreateInvoiceParamType
): Promise<InvoiceType> => {
  const response = await api.post(URI.GET_INVOICES_LIST, params);
  return response.data;
};

export const editInvoice = async (
  params: InvoiceEditParamType
): Promise<InvoiceType> => {
  const response = await api.patch(
    `${URI.GET_INVOICES_LIST}/${params.id}`,
    params.params
  );
  return response.data;
};

import {
  GetListParam,
  IsContractedType,
  LinksType,
  MetaType,
  SortOrderType,
  StatusType,
} from '@/constants/types.constants';
import { URI } from '@/constants/uri.constants';
import api from '@/lib/axios-instance';

import { RestaurantType } from './restaurant.service';

export type MenuTypeType = {
  id: number;
  name: string;
  description: string;
  status: StatusType;
  createdAt: string;
  updatedAt: string;
};

export type MenuType = {
  id: number;
  restaurantId: number;
  restaurantMenuTypeId: number;
  name: string;
  externalMenuId: 0;
  status: StatusType;
  description: string;
  price: number;
  days: string;
  contracted: IsContractedType;
  menuType: MenuTypeType;
  restaurant: RestaurantType;
  createdAt: string;
  updatedAt: string;
};

export type CreateMenuParamType = {
  restaurantMenuTypeId?: number;
  name: string;
  externalMenuId: number;
  description?: string;
  price: number;
  days?: string;
};

type CreateMenuResponseType = MenuType;

export type EditMenuParamType = {
  restaurantMenuTypeId?: number;
  name?: string;
  externalMenuId?: number;
  description?: string;
  price?: number;
  days?: string;
  status?: StatusType;
};

type EditMenuResponseType = MenuType;

export type GetMenusParamType = {
  restaurantId?: string;
  contracted?: boolean;
  sortOrder?: SortOrderType;
  menuDays?: number;
  status?: StatusType;
  limit: number;
  page: number;
};

export type GetMenuResponseType = {
  items: MenuType[];
  links: LinksType;
  meta: MetaType;
};

export const getMenus = async (
  params: GetMenusParamType
): Promise<GetMenuResponseType> => {
  const response = await api.get(URI.GET_MENUS_LIST, { params });
  return response.data;
};

export const getMenusList = async (
  params: GetListParam
): Promise<GetMenuResponseType> => {
  const response = await api.get(URI.GET_MENUS_LIST, { params });
  return response.data;
};

export const getMenuDetail = async (
  params: GetMenusParamType
): Promise<MenuType> => {
  const response = await api.get(URI.GET_MENU, { params });
  return response.data;
};

export const createMenu = async (
  params: CreateMenuParamType
): Promise<CreateMenuResponseType> => {
  const response = await api.post(URI.CREATE_MENU, params);
  return response.data;
};

export const editMenu = async (
  id: string,
  params: EditMenuParamType
): Promise<EditMenuResponseType> => {
  const response = await api.patch(URI.EDIT_MENU.replace(':id', id), params);
  return response.data;
};

import {
  IsInsideContractRangeType,
  LinksType,
  MetaType,
  OrderStatusType,
  SortOrderType,
  StatusType,
} from '@/constants/types.constants';
import { URI } from '@/constants/uri.constants';
import api from '@/lib/axios-instance';

import { MenuType } from './menu.service';
import { EmployeeType } from './organization-employee.service';
import { OrganizationType } from './organization.service';
import { RestaurantType } from './restaurant.service';

export type OrderDetailType = {
  id: number;
  restaurantId: number;
  restaurantOrderId: number;
  restaurantMenuId: number;
  organizationId: number;
  employeeId: number | null;
  userId: number | null;
  isInsideContractRange: IsInsideContractRangeType;
  orderDate: string;
  orderStatus: OrderStatusType;
  status: StatusType;
  unitCount: number;
  unitPrice: number;
  totalPrice: number;
  createdAt: string;
  updatedAt: string;
  organization: OrganizationType;
  restaurant: RestaurantType;
  employee: EmployeeType;
  menu: MenuType;
};

export type OrderType = {
  id: number;
  restaurantId: number;
  organizationId: number;
  tableId: number;
  employeeId?: number;
  userId?: number;
  menu: MenuType;
  orderStatus: OrderStatusType;
  organization: OrganizationType;
  orderDetails: OrderDetailType[];
  restaurant: RestaurantType;
  createdAt: string;
  updatedAt: string;
};

export type CreateOrderDetailParamType = {
  restaurantId: number;
  restaurantMenuId: number;
  organizationId?: number;
  employeeId?: number;
  userId?: number;
  isInsideContractRange?: IsInsideContractRangeType;
  orderDate: string;
  unitCount: number;
  unitPrice: number;
  totalPrice: number;
};

export type CreateOrderDetailResponseType = {
  restaurantId: number;
  restaurantMenuId: number;
  organizationId: number;
  employeeId: number;
  orderDate: string;
  limit: number;
  page: number;
  orderStatus: OrderStatusType;
  isInsideContractRange: IsInsideContractRangeType;
  sortOrder?: SortOrderType;
  status?: StatusType;
};

export type EditOrderDetailParamType = {
  restaurantId?: number;
  restaurantMenuId?: number;
  unitCount?: number;
  unitPrice?: number;
  totalPrice?: number;
  orderStatus?: OrderStatusType;
  status?: StatusType;
};

export type EditOrderDetailResponseType = {
  restaurantId: number;
  restaurantMenuId: number;
  organizationId: number;
  employeeId: number;
  orderDate: string;
  limit: number;
  page: number;
  orderStatus: OrderStatusType;
  isInsideContractRange: IsInsideContractRangeType;
  sortOrder?: SortOrderType;
  status?: StatusType;
};

export type EditOrderParamType = {
  tableId?: number;
  totalBill?: number;
  orderStatus?: OrderStatusType;
  status?: StatusType;
};

export type GetOrdersParamType = {
  restaurantId?: number;
  restaurantMenuId?: number;
  organizationId?: number;
  employeeId?: number;
  orderDate: string;
  orderStatus?: OrderStatusType;
  isInsideContractRange?: IsInsideContractRangeType;
  sortOrder?: SortOrderType;
  status?: StatusType;
  limit: number;
  page: number;
};

export type GetOrdersResponseType = {
  items: OrderType[];
  links: LinksType;
  meta: MetaType;
};

export type GetOrderDetailsParamType = {
  restaurantId?: number;
  restaurantMenuId?: number;
  organizationId?: number;
  employeeId?: number;
  orderDate: string;
  orderStatus?: OrderStatusType;
  isInsideContractRange?: IsInsideContractRangeType;
  sortOrder?: SortOrderType;
  status?: StatusType;
  limit: number;
  page: number;
};

export type GetOrderDetailsResponseType = {
  items: OrderDetailType[];
  links: LinksType;
  meta: MetaType;
};

export type DecryptParamType = { encryptedText: string };

export const createOrderDetail = async (
  params: CreateOrderDetailParamType
): Promise<CreateOrderDetailResponseType> => {
  const response = await api.post(URI.CREATE_ORDER_DETAIL, params);
  return response.data;
};

export const editOrderDetail = async (
  id: string,
  params: EditOrderDetailParamType
): Promise<EditOrderDetailResponseType> => {
  const response = await api.patch(
    URI.EDIT_ORDER_DETAIL.replace(':id', id),
    params
  );
  return response.data;
};

export const getOrdersList = async (
  params: GetOrdersParamType
): Promise<GetOrdersResponseType> => {
  const response = await api.get(URI.GET_ORDERS_LIST, { params });
  return response.data;
};

export const getOrderDetailsList = async (
  params: GetOrderDetailsParamType
): Promise<GetOrderDetailsResponseType> => {
  const response = await api.get(URI.GET_ORDER_DETAILS_LIST, { params });
  return response.data;
};

export const editOrder = async (
  id: string,
  params: EditOrderParamType
): Promise<OrderType> => {
  const response = await api.patch(URI.EDIT_ORDER.replace(':id', id), params);
  return response.data;
};

export const encrypt = async (params: object): Promise<string> => {
  const response = await api.post(URI.ENCRYPT, params);
  return response.data;
};

export const decrypt = async (params: DecryptParamType): Promise<object> => {
  const response = await api.get(URI.DECRYPT, { params });
  return response.data;
};

import { STATUS } from '@/constants/common.constants';
import { GetListParam, LinksType, MetaType } from '@/constants/types.constants';
import { URI } from '@/constants/uri.constants';
import api from '@/lib/axios-instance';

export type EmployeeItem = {
  id: number;
  name: string;
  username: string;
  status: STATUS;
};

export type EmployeeType = {
  id: number;
  userId: number;
  employeeCode: string;
  status: STATUS;
  username: string;
  password: string;
  user?: EmployeeItem;
};

type GetEmployeeListResponse = {
  items: EmployeeType[];
  links: LinksType;
  meta: MetaType;
};

export type EmployeeParamType = {
  organizationId: number | null;
  employeeCode: string;
  username: string;
  password: string;
  status: string;
};

export type EmployeeEditParamType = {
  id: number | null;
  params: {
    employeeCode?: string;
    username?: string;
    password?: string;
    status?: string;
  };
};

export const getEmployeesList = async (
  params: GetListParam
): Promise<GetEmployeeListResponse> => {
  const response = await api.get(URI.GET_EMPLOYEES_LIST, { params });
  return response.data;
};

export const addEmployee = async (
  params: EmployeeParamType
): Promise<EmployeeType> => {
  const response = await api.post(URI.GET_EMPLOYEES_LIST, params);
  return response.data;
};

export const editEmployee = async (
  params: EmployeeEditParamType
): Promise<EmployeeType> => {
  const response = await api.patch(
    `${URI.GET_EMPLOYEES_LIST}/${params.id}`,
    params.params
  );
  return response.data;
};

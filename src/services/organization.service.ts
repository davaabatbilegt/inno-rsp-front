import {
  GetListParam,
  LinksType,
  MetaType,
  StatusType,
} from '@/constants/types.constants';
import { URI } from '@/constants/uri.constants';
import api from '@/lib/axios-instance';

export type OrganizationType = {
  id: number;
  name: string;
  username: string;
  address: string;
  description: string;
  employeeCount: number;
  status: StatusType;
  contracted: number;
  createdAt: string;
  updatedAt: string;
};

type GetOrganizationListParam = GetListParam & {
  contracted?: boolean;
};

export type GetOrganizationListResponse = {
  items: OrganizationType[];
  links: LinksType;
  meta: MetaType;
};

type OrganizationDetailParam = string;

export const getOrganizationsList = async (
  params: GetOrganizationListParam
): Promise<GetOrganizationListResponse> => {
  const response = await api.get(URI.GET_ORGANIZATIONS_LIST, { params });
  return response.data;
};

export const getOrganizationDetail = async (
  id: OrganizationDetailParam
): Promise<OrganizationType> => {
  const response = await api.get(`${URI.GET_ORGANIZATIONS_LIST}/${id}`);
  return response.data;
};

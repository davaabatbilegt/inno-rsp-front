import { SortOrderType } from '@/constants/types.constants';
import { URI } from '@/constants/uri.constants';
import api from '@/lib/axios-instance';

export type GetReportsParamType = {
  targetMonth: string;
  restaurantId?: number;
  restaurantMenuId?: number;
  organizationId?: number;
  sortOrder?: SortOrderType;
};

export type ReportType = {
  r_name: string;
  o_name: string;
  rm_name: string;
  order_date: string;
  ordered: string;
  taken: string;
};

export type GetReportsResponseType = ReportType[];

export const getReportsList = async (
  params: GetReportsParamType
): Promise<GetReportsResponseType> => {
  const response = await api.get(URI.GET_REPORTS_LIST, { params });
  return response.data;
};

import {
  GetListParam,
  LinksType,
  MetaType,
  StatusType,
} from '@/constants/types.constants';
import { URI } from '@/constants/uri.constants';
import api from '@/lib/axios-instance';

import { RestaurantType } from './restaurant.service';

export type RestaurantTableType = {
  id: number;
  restaurantId: number;
  name: string;
  status: StatusType;
  capacity: number;
  restaurant: RestaurantType;
  createdAt: string;
  updatedAt: string;
};

type GetRestaurantTableListParam = GetListParam & {
  restaurantId?: number;
};

export type GetRestaurantTableListResponse = {
  items: RestaurantTableType[];
  links: LinksType;
  meta: MetaType;
};

export const getRestaurantTablesList = async (
  params: GetRestaurantTableListParam
): Promise<GetRestaurantTableListResponse> => {
  const response = await api.get(URI.GET_RESTAURANT_TABLES, { params });
  return response.data;
};

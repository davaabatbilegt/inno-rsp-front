import {
  GetListParam,
  LinksType,
  MetaType,
  StatusType,
} from '@/constants/types.constants';
import { URI } from '@/constants/uri.constants';
import api from '@/lib/axios-instance';

export type RestaurantType = {
  id: number;
  name: string;
  address: string;
  description: string;
  rating: number;
  latitude: number | null;
  longitude: number | null;
  capacity: number | null;
  contracted: string;
  status: StatusType;
  username: string;
  createdAt: string;
  updatedAt: string;
};

type GetRestaurantListParam = GetListParam & {
  contracted?: boolean;
};

export type GetRestaurantListResponse = {
  items: RestaurantType[];
  links: LinksType;
  meta: MetaType;
};

type RestaurantDetailParam = string;

export const getRestaurantsList = async (
  params: GetRestaurantListParam
): Promise<GetRestaurantListResponse> => {
  const response = await api.get(URI.GET_RESTAURANTS_LIST, { params });
  return response.data;
};

export const getRestaurantDetail = async (
  id: RestaurantDetailParam
): Promise<RestaurantType> => {
  const response = await api.get(`${URI.GET_RESTAURANTS_LIST}/${id}`);
  return response.data;
};
